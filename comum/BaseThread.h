#ifndef BaseThreadH
#define BaseThreadH
#include <windows.h>
#include <stdio.h>

class BaseThread
{
private:
	HANDLE  hTread;
	DWORD id;
	BaseThread(const BaseThread &a);
	BaseThread & operator = (const BaseThread &a);
	DWORD internalExecute();
	bool terminated;
protected:
	virtual void execute();
	static DWORD WINAPI ThreadCall(LPVOID param);
public:
	BaseThread();
	virtual void start();
	virtual void stop();
	bool getTerminated() const;
	void setTerminated();
	void wait(DWORD miliseconds);
	virtual ~BaseThread(void);

};

#endif