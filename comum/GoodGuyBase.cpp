#include <string>
#include "..\comum\GoodGuyBase.h"
#include "..\comum\BadGuyBase.h"

using namespace std;

GoodGuyBase::GoodGuyBase( GameBase *game, string nome,int vida, bool inicioAuto, int x, int y):Guy(game, x, y)
{
	this->nome=nome;
	this->vida=vida;
	this->inicioAuto=inicioAuto;
	setAuto(inicioAuto);

	pontos=0;
	come();
}

void GoodGuyBase::come() {
	
	if (getGame()->getTabuleiro()->come(getX(),getY())) {
		pontos++;
	}
	Guy::come();
}


GoodGuyBase::~GoodGuyBase(void) {
}

int GoodGuyBase::getPontos() const
{
	return pontos;
}

string GoodGuyBase::getNome()
{
	return nome;
}

int GoodGuyBase::getVida()const
{
	return vida;
}

void GoodGuyBase::foiAtacadoPor( Guy *guy )
{
	Guy::foiAtacadoPor(guy);
	if (guy->isGoodGuy()) return;
	if(vida>0) {
		vida--;
		if (vida==0) {
			morre();
			guy->morre();
		}
	}
}

void GoodGuyBase::setPontos(int pontos)
{
	this->pontos=pontos;
}

void GoodGuyBase::setVida( int vida )
{
	this->vida=vida;
}

bool GoodGuyBase::getInicioAuto()
{
	return inicioAuto;
}

void GoodGuyBase::avanca()
{
	Guy::avanca();
	if (inicioAuto && getAuto() && getGame()->getGameTime()>(TIMEAUTOMATIC*1000)) {
		setAuto(false);
	}
}
