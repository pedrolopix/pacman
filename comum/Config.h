#ifndef ConfigH
#define ConfigH

#include <string>
#include <windows.h>
#include <winreg.h>
#include <wtypes.h>

class Config
{
private:
	std::string host;
	std::string username;
	std::string password;
	std::string root;
	bool saveLogin;

	LONG getValue(HKEY hKey, const std::string &strValueName, std::string &strValue, const std::string &strDefaultValue);
	LONG getValue(HKEY hKey, const std::string &strValueName, DWORD &Value, const DWORD &DefaultValue);
	LONG setValue(HKEY hKey, const std::string &strValueName, const std::string &strValue);
	LONG setValue(HKEY hKey, const std::string &strValueName, const DWORD &value);
	Config(const Config &c);
	Config & operator = (const Config &c);
public:
	Config();
	void load();
	void save();
	std::string getHost() const;
	void setHost(std::string val);
	bool getSaveLogin() const;
	void setSaveLogin(bool val);
	std::string getUsername() const;
	void setUsername(std::string val);
	std::string getPassword() const;
	void setPassword(std::string val);
	std::string getRoot();
	void setRoot(std::string root);
	std::string getServerName();
	~Config();
	

};


#endif;

