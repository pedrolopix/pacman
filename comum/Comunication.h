#pragma once
#define MAX_CHAR 40
#define MAXUSERS 20
#define MAXBUFFER 2048
#define TIMEOUT 10000
#define TAB_MAX_X 33
#define TAB_MAX_Y 23
#define WAITTIME 20
#define TIMEAUTOMATIC 20

#define PIPE_NAME "\\pipe\\plpacman"
#define PIPE_NAME_GAME "\\pipe\\plpacman_game"

#define MAXJOGADORES 3
#define MAXGUYS 15

enum Commands {cmd_c_Login, cmd_s_Loggedin, cmd_s_LoginError, cmd_s_UserAlreadyLoggedin, 
	           cmd_c_UserAdd, cmd_s_UserAdded, cmd_s_UserDuplicated, cmd_s_UserUpdated,
			   cmd_c_UserList, cmd_s_UserList,
               cmd_c_UserDelete, cmd_s_UserNotExist, cmd_s_UserDeleted,
			   cmd_c_UserGet, cmd_s_GetUser, //cmd_s_UserNotExist
			   cmd_c_NovoJogo, cmd_s_novoJogoOk, cmd_s_JaExisteJogo,
			   cmd_c_LigarJogo, cmd_s_LigarJogoOk, cmd_s_NaoExisteJogo, cmd_s_JogoADecorrer, cmd_s_MaxJogadores,
			   cmd_c_UserSetKeys, 
			   cmd_c_Sendkeys,
			   cmd_c_DesisteJogo,
			   cmd_c_top10, cmd_s_top10,
			   cmd_s_cmdUnknown
};

enum gameCmd {gcmd_esperaJogadores, gcmd_update, gcmd_end, gcmd_serverend};
enum GuyCor {vermelho, azul};
enum Direccao {d_cima, d_esq, d_dto, d_baixo};

struct CommandBuf{
	Commands command;
	int len;
};

struct CBufLogin {
   char username[MAX_CHAR];
   char password[MAX_CHAR];
};

struct CBufUserAdd{
	CBufLogin user;
	bool isAdmin;
};

struct CBufUserName{
	char username[MAX_CHAR];
};

struct CBufUserProp {
	int NJogos;
	int NPontos;
	int NDesistencias;
	int NQuebras;
	int keyDown;
	int keyLeft;
	int keyRight;
	int keyUp;
	bool isAdmin;
}; 


struct CBufUser {
	CBufLogin user;
	CBufUserProp Prop;
};

struct CBufUserList{
	int nUsers;
	char users[MAXUSERS][MAX_CHAR];
	bool eof;
};


struct CBufSetKeys {
	int left;
	int right;
	int up;
	int down;
};

struct CBufSendKeys {
	int key;
};


struct CBufInicarJogo {
	int nObjectos;
	int nBadGuys; 
	int nAtaques;
	bool inicioAuto;
	int xini;
    int yini;
};

struct CBufLigarJogo {
	bool inicioAuto;
	int xini;
    int yini;
};


struct CbufGame {
	gameCmd cmd;
	int nguys;
	int time;
	int pontosTotal;
	int pontosMax;
	int pontosFalta;
	char tabuleiro[TAB_MAX_X][TAB_MAX_Y];
	struct {
		char nome[MAX_CHAR];
		int pontos;
		int vida;
		int x;
		int y;
		Direccao direccao;
		bool automatic;		
		bool morto;
		bool isGoodGuy;
		GuyCor cor;
	} guys[MAXGUYS];
};


struct CbufTop10 {
   int count;
   struct {
     char nome[MAX_CHAR];
     int NPontos;
	 int NVitorias;
	 int NDerroras;
	 int NDesistencias;
   } jogadores[10];
};