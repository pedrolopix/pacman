#ifndef PLMutexH
#define PLMutexH
#include <Windows.h>

class PLMutex {
private:
	HANDLE hMutex;
	PLMutex(const PLMutex &m);
	PLMutex & operator = (const PLMutex & m);
public:
	PLMutex(bool InitialOwner=false);
	void waitFor(DWORD timeout);
	void lock();
	void unlock();
	~PLMutex();
};


#endif