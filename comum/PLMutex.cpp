
#include "PLMutex.h"
#include "..\comum\utils.h"

PLMutex::PLMutex( bool InitialOwner )
{
   hMutex=CreateMutex(NULL,InitialOwner,NULL);
   if (hMutex==NULL) {
	ErrorExit("Erro a criar Mutex");
   }
}


void PLMutex::waitFor( DWORD timeout )
{
	WaitForSingleObject(hMutex,timeout);
}

void PLMutex::lock()
{
	waitFor(INFINITE);
}

void PLMutex::unlock()
{
	ReleaseMutex(hMutex);
}

PLMutex::~PLMutex()
{
	CloseHandle(hMutex);
}
