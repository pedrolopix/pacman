#ifndef TabuleiroBaseH
#define TabuleiroBaseH
#include <string>
#include "..\comum\GraphicObject.h"
#include "..\comum\Comunication.h"

class TabuleiroBase:public GraphicObject
{
private:
	char tab[TAB_MAX_X][TAB_MAX_Y];
	int npontos;
	int nPontosMax;
	int nPontosTotal;
	std::string fileName;


	TabuleiroBase(const TabuleiroBase &a);
	TabuleiroBase & operator = (const TabuleiroBase &a);
protected:
	bool isTopoEsquerdo(int x,int y);
	bool isTopoDireito( int x,int y );
	bool isBbottomEsquerdo( int x,int y );
	bool isBottomDireito( int x,int y );
	bool isCruz( int x,int y ); 
	bool isTCima(int x,int y );
	bool isTBaixo( int x,int y );
	bool isTEsquerdo(int x,int y );
	bool isTDireito( int x,int y ); 

public:
	TabuleiroBase(GameBase *game);
	virtual void reload();
	virtual bool loadTabuleiro(std::string filename);
	virtual void loadTabuleiro(char tab[TAB_MAX_X][TAB_MAX_Y]);
	virtual bool canGoLeft(int x, int y);
	virtual bool canGoRight( int x, int y );
	virtual bool canGoUp( int x, int y );
	virtual bool canGoDown( int x, int y );
	virtual int getPontosMax();
	virtual char getTab(int x, int y)const;
	virtual bool come( int x, int y );
	virtual int getPontosFalta();
	virtual void SetPontos( int pontos );
	virtual bool canGoXY( int x, int y );
	virtual int getPontosTotal();
	virtual void LoadInternal();
	virtual ~TabuleiroBase(void);
};

#endif