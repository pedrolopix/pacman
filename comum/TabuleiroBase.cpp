#include <stdio.h>
#include <istream>
#include <fstream>
#include <string>
#include "..\comum\TabuleiroBase.h"
#include "..\comum\GraphicObject.h"
#include "..\comum\GameBase.h"
#include "..\comum\Application.h"
#include "..\comum\utils.h"




using namespace std;

TabuleiroBase::TabuleiroBase(GameBase *game):GraphicObject(game){
	npontos=0;
	nPontosMax=0;
	fileName="";

}



TabuleiroBase::~TabuleiroBase(void)
{

}


void TabuleiroBase::LoadInternal() {
   loadTabuleiro(getAppPath()+"\\tabuleiro1.txt");	
}


bool TabuleiroBase::loadTabuleiro( std::string filename ) 
{
	this->fileName=filename;
	for(int x=0;x<TAB_MAX_X;x++) {
		for(int y=0; y<TAB_MAX_Y;y++) {
		tab[x][y]=' ';
		}
	}
	ifstream file;
	string line;
	int y=0;
	file.open (filename, ios::in);
	if (!file.is_open()) {
		return false;
	}
	nPontosTotal=0;

	while ( file.good() )
	{
		getline (file,line);
		for(unsigned int x=0; (x<line.length()) && (x<TAB_MAX_X); x++) {
			tab[x][y]=line.at(x);
			if (line.at(x)=='.') nPontosTotal++;
		}
		y++;
	}
	file.close();

	nPontosMax=nPontosTotal;
	return true;
}

void TabuleiroBase::loadTabuleiro(char tab[TAB_MAX_X][TAB_MAX_Y])
{
	for(int x=0; x<TAB_MAX_X;x++) {
		for(int y=0; y<TAB_MAX_Y; y++) {
			this->tab[x][y]=tab[x][y];
		}
	}
}


bool TabuleiroBase::isTopoEsquerdo( int x,int y )
{
	if ((x==0) && (y==0)) return true;
	if ((x==TAB_MAX_X) || (y==TAB_MAX_Y)) return false;
    if (tab[x+1][y]=='-' && tab[x][y+1]=='|' && tab[x-1][y]!='-' && tab[x][y-1]!='|') return true;
	return false;
}

bool TabuleiroBase::isTopoDireito( int x,int y )
{
	if ((x==TAB_MAX_X) && (y==TAB_MAX_Y)) return true;
	if ((x==0) && (y==0)) return false;
	if (tab[x-1][y]=='-' && tab[x][y+1]=='|' && tab[x+1][y]!='-' && tab[x][y-1]!='|') return true;
	return false;
}

bool TabuleiroBase::isBbottomEsquerdo( int x,int y ) {
	if ((x==0) && (y==TAB_MAX_Y)) return true;
	if (y==0) return false;
	if (tab[x-1][y]!='-' && tab[x][y+1]!='|' && tab[x+1][y]=='-' && tab[x][y-1]=='|') return true;
	return false;
}

bool TabuleiroBase::isBottomDireito( int x,int y ) {
	if ((x==TAB_MAX_X) && (y==TAB_MAX_Y)) return true;
	if (x==0) return false;
	if (tab[x-1][y]=='-' && tab[x][y+1]!='|' && tab[x+1][y]!='-' && tab[x][y-1]=='|') return true;
	return false;
}

bool TabuleiroBase::isTCima( int x,int y ) {
	if ((x==TAB_MAX_X) && (y==TAB_MAX_Y)) return false;
	if (tab[x-1][y]=='-' && tab[x][y+1]=='|' && tab[x+1][y]=='-' && tab[x][y-1]!='|') return true;
	return false;
}

bool TabuleiroBase::isTEsquerdo( int x,int y ) {
	//if ((x==TAB_MAX_X) && (y==TAB_MAX_Y)) return false;
	if (tab[x][y]!='+') return false;
	if (y==0 && y==TAB_MAX_Y) return false;
	if (tab[x][y-1]=='|' && tab[x+1][y]=='-' && tab[x][y+1]=='|') return true;
	return false;
}

bool TabuleiroBase::isTDireito( int x,int y ) {
	//if ((x==TAB_MAX_X) && (y==TAB_MAX_Y)) return false;
	if (tab[x][y]!='+') return false;
	if (x==0 && y==0) return false;
	if (tab[x-1][y]=='-' && tab[x][y-1]=='|' && tab[x][y+1]=='|') return true;
	return false;
}

bool TabuleiroBase::isTBaixo( int x,int y ) {
	if (y==0) false;
	if (tab[x][y]!='+') return false;
	if (tab[x-1][y]=='-' && tab[x][y+1]!='|' && tab[x+1][y]=='-' && tab[x][y-1]=='|') return true;
	return false;
}

bool TabuleiroBase::isCruz( int x,int y )
{
	if ((x==TAB_MAX_X) || (y==TAB_MAX_Y)) return false;
	if ((x==0) || (y==0)) return false;
	if ((tab[x-1][y]=='-') && (tab[x][y-1]=='|') && (tab[x+1][y]=='-') && (tab[x][y+1]=='|')) return true;
	return false;
}


bool TabuleiroBase::canGoLeft( int x,int y )
{	
	return (x-1>0 && (tab[x-1][y]!='+' && tab[x-1][y]!='|' && tab[x-1][y]!='-'));
}

bool TabuleiroBase::canGoRight( int x, int y )
{
	return (x+1<TAB_MAX_X && ((tab[x+1][y]!='+') && (tab[x+1][y]!='|') && (tab[x+1][y]!='-')));
}

bool TabuleiroBase::canGoUp( int x, int y )
{
	return (y-1>0 && (tab[x][y-1]!='+' && tab[x][y-1]!='|' && tab[x][y-1]!='-'));
}

bool TabuleiroBase::canGoDown( int x, int y )
{
	return (y+1<TAB_MAX_Y && (tab[x][y+1]!='+' && tab[x][y+1]!='|' && tab[x][y+1]!='-'));
}

bool TabuleiroBase::canGoXY( int x, int y )
{
	return (x>0 && y>0 && x<TAB_MAX_X && y<TAB_MAX_Y) && (tab[x][y]=='.' || tab[x][y]==' ');
}

bool TabuleiroBase::come( int x, int y )
{
	if (tab[x][y]=='.') {
		npontos++;
		tab[x][y]=' ';
	  return true;
	}
	return false;
}

int TabuleiroBase::getPontosMax()
{
	return nPontosMax;
}

int TabuleiroBase::getPontosFalta()
{
	return nPontosMax-npontos;
}

void TabuleiroBase::SetPontos( int pontos )
{
	if (pontos>nPontosMax) return;
	while (nPontosMax!=pontos) {
		int x= random_l_h(0,TAB_MAX_X-1);
		int y= random_l_h(0,TAB_MAX_Y-1);

		if (tab[x][y]=='.') {
			tab[x][y]=' ';
			nPontosMax--;
		}

	}

}

void TabuleiroBase::reload()
{
	if (fileName.length()!=0) {
		loadTabuleiro(fileName);
	}
}

int TabuleiroBase::getPontosTotal()
{
	return nPontosTotal;
}

char TabuleiroBase::getTab( int x, int y )const
{
	return tab[x][y];
}

