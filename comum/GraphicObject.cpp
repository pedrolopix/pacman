#include "..\comum\GraphicObject.h"
#include "..\comum\GameBase.h"

GraphicObject::GraphicObject(GameBase *game)
{
	this->game=game;
}


GraphicObject::~GraphicObject(void)
{
}


void GraphicObject::draw(HDC &hdc,PAINTSTRUCT &PtStc) {

}

GameBase * GraphicObject::getGame()
{
	return game;
}

int GraphicObject::translateCoordX( int x )
{
	return game->getCoordX(x);
}

int GraphicObject::translateCoordY( int y )
{
	return game->getCoordY(y);
}

int GraphicObject::getHeight()
{
	return game->getHeight();
}

int GraphicObject::getWidth()
{
	return game->getWidth();
}

void GraphicObject::doKeyDown( WPARAM wParam, LPARAM lParam )
{

}

