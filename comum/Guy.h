#pragma once
#include "..\comum\graphicobject.h"
#include "..\comum\GameBase.h"
#include <windef.h>
#include <stdio.h>

#define ngbt 4



class Guy :public GraphicObject
{
	int x;
	int y;
	int lastX;
	int lastY;	
	int keyDown;
	int keyUp;
	int keyLeft;
	int keyRight;
	bool morreu;
	HBITMAP hbit[ngbt];
	Direccao direccao;
	bool automatico;
	Guy(const Guy &a);
	Guy & operator = (const Guy &a);
protected:

	void loadBitmap(Direccao tipo, int r);
public:
	Guy(GameBase *game, int x=1, int y=1);
	virtual void draw(HDC &hdc,PAINTSTRUCT &PtStc);
	void morre();
	void draw( HDC & hdc );
	virtual void foiAtacadoPor(Guy *guy);
	virtual void doKeyDown( int key );
	virtual void come();
	virtual void setAuto(bool a);
	virtual bool getAuto()const;
	virtual bool isIn(int x, int y);
	virtual bool isLastIn(int lastx, int lasty);
	virtual int getX()const;
	virtual int getY()const;;
	virtual int getKeyDown() const;
	virtual void setKeyDown(int val);
	virtual int getKeyUp() const;
	virtual void setKeyUp(int val);
	virtual int getKeyLeft() const;
	virtual void setKeyLeft(int val);
	virtual int getKeyRight() const;
	virtual void setKeyRight(int val);
	virtual void avanca();
	virtual void setXY(int x, int y);
	virtual void AutoMudaDireccao();
	virtual bool estaMorto()const;
	virtual void setDireccao(Direccao direccao);
	virtual Direccao getDireccao()const;
	virtual int getLastX() const;
	virtual int getlastY() const;
	virtual bool isGoodGuy()const;
	~Guy(void);

};

