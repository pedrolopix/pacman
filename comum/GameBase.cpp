#include "..\comum\GameBase.h"
#include "..\comum\Application.h"
#include "..\comum\Guy.h"
#include "..\comum\GoodGuyBase.h"

GameBase::GameBase(Application *application) {
	this->application = application;
	tabuleiro = NULL;
	guys.clear();
	width = 24;
	height = 24;
	initTime = 0;
	nGoodGuys = 0;
	acabouJogo=false;
	info="";
}

GameBase::~GameBase(void) {
	if (tabuleiro) delete tabuleiro;
	for (unsigned int i = 0; i < guys.size(); i++) {
		delete guys[i];
	}
	guys.clear();

}

void GameBase::draw(HDC &hdc, PAINTSTRUCT &PtStc) {
	
	drawInfo(hdc);

	if (tabuleiro) tabuleiro->draw(hdc, PtStc);
	for (unsigned int i = 0; i < guys.size(); i++) {
		guys[i]->draw(hdc, PtStc);
	}
}


int GameBase::getInitTime()const {
	return initTime;
}

void GameBase::setTabuleiro(TabuleiroBase *tabuleiro) {
	this->tabuleiro = tabuleiro;
}

void GameBase::addBadGuy(Guy *guy) {
	guys.push_back(guy);
}

void GameBase::addGoodGuy(Guy *guy) {
	guys.push_back(guy);
	nGoodGuys++;
}

void GameBase::GuyMorreu(Guy *guy) {
	if (guy->isGoodGuy()) {
		nGoodGuys--;
	}
}

Application * GameBase::getApp() {
	return application;
}

int GameBase::getCoordX(int x) const {
	return x * width + 30;
}

int GameBase::getCoordY(int y) const {
	return y * height + 30;
}

int GameBase::getHeight() const {
	return height;
}

int GameBase::getWidth() const {
	return width;
}

void GameBase::doKeyDown(int key) {
	for (unsigned int i = 0; i < guys.size(); i++) {
		guys[i]->doKeyDown(key);
	}
}

TabuleiroBase *GameBase::getTabuleiro() {
	return tabuleiro;
}

void GameBase::terminou() {
	acabouJogo=true;
}

int GameBase::getTabXFromScr(int x)const {
	return ((x - 30) / width);
}

int GameBase::getTabYFromScr(int y)const {
	return ((y - 30) / width);
}

bool GameBase::canGoLeft(Guy *guy, int x, int y, bool ataca)const {
	return (tabuleiro->canGoLeft(x, y)) && (!isGuyIn(guy, x - 1, y, ataca));
}

bool GameBase::canGoRight(Guy *guy, int x, int y, bool ataca)const {
	return (tabuleiro->canGoRight(x, y)) && (!isGuyIn(guy, x + 1, y, ataca));
}

bool GameBase::canGoUp(Guy *guy, int x, int y, bool ataca)const {
	return (tabuleiro->canGoUp(x, y) && !isGuyIn(guy, x, y - 1, ataca));
}

bool GameBase::canGoDown(Guy *guy, int x, int y, bool ataca)const {
	return (tabuleiro->canGoDown(x, y)) && (!isGuyIn(guy, x, y + 1, ataca));
}

bool GameBase::isGuyIn(Guy *guy, int x, int y,bool ataca)const {
	bool ret;
	for (unsigned int i = 0; i < guys.size(); i++) {
		if (guy==NULL) {
			ret = guys[i]->isIn(x, y);
			if (ret) {
				//guys[i].atacado(guy);
				return true;
			}
		} else
			if (guy != guys[i]) {
				ret = guys[i]->isIn(x, y);
				if (ret) {
					guys[i]->foiAtacadoPor(guy);
					return true;
				}
			}
	}
	return false;
}

int GameBase::getNGuys() const {
	return guys.size();
}

Guy * GameBase::getGuy(int index) {
	if (index < 0 || index > (int)guys.size()) return NULL;
	return guys[index];
}

void GameBase::avanca()
{
	  for (unsigned int i = 0; i < guys.size(); i++) {
		  guys[i]->avanca();
	  }

	  if ((getTabuleiro()->getPontosFalta()==0) || (nGoodGuys==0))  {
		  terminou();
	  }
}

void GameBase::desistir()
{
	terminou();
	//application->terminouJogo(2);
}



void GameBase::doMouseMove( WORD keys, int x, int y )
{

}

void GameBase::defkeys(int left, int right, int up, int down) {

}


bool GameBase::getAcabouJogo()
{
	return acabouJogo;
}

void GameBase::start()
{
	initTime = GetTickCount();
}

int GameBase::getGameTime() const
{
	return GetTickCount() - getInitTime();
}

int GameBase::getPontosTotal() const
{
	if (tabuleiro==NULL) return 342;
	return tabuleiro->getPontosTotal();
}

void GameBase::setInfo( std::string info )
{
	this->info=info;
}

void GameBase::drawInfo( HDC & hdc )
{
	SetTextColor(hdc,RGB(255,255,255));
	SetBkColor(hdc,RGB(0,0,0));
	TextOut(hdc,35,10,"                                                                                                                      ",120);
	TextOut(hdc,35,10,info.c_str(),info.length());
}

int GameBase::getPontosMax() const
{
	if (tabuleiro==NULL) return 0;
	return tabuleiro->getPontosMax();
}

int GameBase::getPontosFalta() const
{
	if (tabuleiro==NULL) return 0;
	return tabuleiro->getPontosFalta();
}

bool GameBase::isGuyLastIn( int x, int y ) {
	bool ret;
	for (unsigned int i = 0; i < guys.size(); i++) {

		ret = guys[i]->isLastIn(x, y);
		if (ret) {
			return true;


		}
		
	}
	return false;
}