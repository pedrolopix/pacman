#include "BadGuyBase.h"



BadGuyBase::BadGuyBase(GameBase *game, GuyCor cor, int x, int y) : Guy(game, x, y) {
  setAuto(true);
  this->cor=cor;

}

BadGuyBase::~BadGuyBase(void) {
}

GuyCor BadGuyBase::getCor() const
{
	return cor;
}

bool BadGuyBase::isGoodGuy() const
{
	return false;
}
