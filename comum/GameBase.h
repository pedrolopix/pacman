#ifndef GameBaseH
#define GameBaseH

#include <vector>
#include "..\comum\GraphicObject.h"
#include "..\comum\TabuleiroBase.h"


class Application;
class Guy;
class GoodGuyBase;
class BadGuyBase;

class GameBase {
private:
  int height;
  int width;
  int nGoodGuys; // n. de good guys vivos
  bool acabouJogo;
  TabuleiroBase *tabuleiro;
  std::vector<Guy *> guys;
  int initTime;
  std::string info;
  Application *application;
  GameBase(const GameBase &game);
  GameBase & operator =(GameBase & game);
protected:
  void setTabuleiro(TabuleiroBase *tabuleiro);
public:
  GameBase(Application *application);
  virtual ~GameBase(void);
  void addBadGuy(Guy *guy);
  void addGoodGuy(Guy *guy);
  virtual void start();
  virtual int getCoordX(int x) const;
  virtual int getCoordY(int y) const;
  virtual int getWidth() const;
  virtual int getHeight() const;
  TabuleiroBase *getTabuleiro();
  Application * getApp();
  virtual void draw(HDC &hdc, PAINTSTRUCT &PtStc);

  virtual void drawInfo( HDC & hdc );

  virtual void doKeyDown(int key);
  virtual bool isGuyIn(Guy *guy, int x, int y,bool ataca)const;
  virtual bool canGoLeft(Guy *guy, int x, int y,bool ataca)const;
  virtual bool canGoRight(Guy *guy, int x, int y,bool ataca)const;
  virtual bool canGoUp(Guy *guy, int x, int y,bool ataca)const;
  virtual bool canGoDown(Guy *guy, int x, int y,bool ataca)const;
  virtual int getInitTime()const;
  virtual int getNGuys()const;
  virtual Guy *getGuy(int index);
  virtual void terminou();
  virtual int getTabXFromScr(int x)const;
  virtual int getTabYFromScr(int y)const;
  virtual void avanca();
  virtual bool getAcabouJogo();
  virtual void desistir();
  virtual void defkeys(int left, int right, int up, int down);
  virtual void doMouseMove( WORD keys, int x, int y );
  virtual void GuyMorreu(Guy *guy);
  virtual void setInfo(std::string info);
  virtual int getGameTime() const;
  virtual int getPontosTotal() const;
  virtual int getPontosMax() const;
  virtual int getPontosFalta() const;
  bool isGuyLastIn( int x, int y );
};

#endif 