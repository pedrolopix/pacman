#ifndef ApplicationH
#define ApplicationH
#include <windows.h>

class Application {
private:
	Application(const Application &a);
	Application & operator = (const Application &a);
public:
	Application();
	virtual HINSTANCE getHInstance()=0;
	virtual ~Application();
};


#endif