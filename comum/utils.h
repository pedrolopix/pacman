#ifndef utilsH
#define utilsH
#include <string>
#include <Windows.h>

 void init_rand();
 int random_l_h(int min, int max);
 void ErrorExit(std::string info);
 std::string MillisecondsToStr(int miliseconds);
 std::string getDLL(HMODULE h);
 std::string GetPathFrom (const std::string str);
 std::string getAppPath();
 void ShowError(std::string info);
#endif

