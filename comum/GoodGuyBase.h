#ifndef GoodGuyBaseH
#define GoodGuyBaseH
#include <string>
#include "..\comum\guy.h"

class GameBase;

class GoodGuyBase :public Guy
{
private:
	int pontos;
	int vida;
	bool inicioAuto;
	std::string nome;
	GoodGuyBase(const GoodGuyBase &a);
	GoodGuyBase & operator = (const GoodGuyBase &a);
public:
	GoodGuyBase(GameBase *game,std::string nome, int vida, bool inicioAuto=false,int x=1, int y=1);
	virtual void come();
	virtual void foiAtacadoPor(Guy *guy);
	virtual int getPontos()const;
	virtual void setPontos(int pontos);
	virtual std::string getNome();
	virtual ~GoodGuyBase(void);
	virtual bool getInicioAuto();
	virtual void avanca();
	virtual int getVida()const;
	virtual void setVida(int vida);
};

#endif