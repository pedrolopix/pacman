#ifndef BadGuyBaseH
#define BadGuyBaseH
#include "guy.h"
#include "..\comum\Comunication.h"

class BadGuyBase :public Guy
{
private:
	BadGuyBase(const BadGuyBase &a);
	BadGuyBase & operator = (const BadGuyBase &a);
	GuyCor cor;
public:
	BadGuyBase(GameBase *game, GuyCor cor, int x=1, int y=1);
	virtual ~BadGuyBase(void);
	virtual bool isGoodGuy()const;
	GuyCor getCor() const;
};

#endif