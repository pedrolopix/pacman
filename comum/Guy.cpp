#include <stdio.h>
#include "..\comum\Guy.h"
//#include "resource.h"
#include "..\comum\Application.h"
#include "..\comum\utils.h"


Guy::Guy(GameBase *game, int x, int y):GraphicObject(game){
	this->x=x;
   	this->y=y;
	lastX=x;
	lastY=y;
	
	direccao=d_dto;
	morreu=false;
	keyDown= VK_DOWN;
	keyUp = VK_UP;
	keyLeft = VK_LEFT;
	keyRight = VK_RIGHT;
}

Guy::~Guy(void)
{
	for(int i=0;i<ngbt;i++ ) DeleteObject(hbit[i]);
}

void Guy::draw( HDC &hdc,PAINTSTRUCT &PtStc )
{
	if (estaMorto()) return;
	if ((x<PtStc.rcPaint.left) && (x>PtStc.rcPaint.right) &&
		(y<PtStc.rcPaint.top) && (y>PtStc.rcPaint.bottom) )
	{
		return;

	}
	draw(hdc);

}

void Guy::draw( HDC & hdc )
{
	if (estaMorto()) return;
	HDC auxmemdc;
	auxmemdc=CreateCompatibleDC(hdc);
	SelectObject(auxmemdc, hbit[direccao]);
	BitBlt(hdc, translateCoordX(x),translateCoordX(y), getWidth(), getHeight(), auxmemdc, 0,0, SRCCOPY);
	DeleteDC(auxmemdc);
}

void Guy::doKeyDown( int key )
{
	if (estaMorto()) return;
	if (automatico) {
		return;
	}
	if (key==keyLeft && (getGame()->canGoLeft(this,x,y,false))) {
		direccao = d_esq;
	}
	if (key==keyRight && (getGame()->canGoRight(this,x,y,false))) {
		direccao = d_dto;
	}
	if (key==keyUp && (getGame()->canGoUp(this,x,y,false))){
		direccao = d_cima;
	}
	if (key==keyDown && (getGame()->canGoDown(this,x,y,false))){
		direccao = d_baixo;
	}

}

void Guy::come()
{

}


void Guy::loadBitmap( Direccao tipo, int r )
{
	hbit[tipo] = LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(r));
}

int Guy::getY()const
{
	return y;
}

int Guy::getX()const
{
	return x;
}

void Guy::setAuto(bool a)
{
   this->automatico=a;
}

int Guy::getKeyDown() const
{
	return keyDown;
}

void Guy::setKeyDown( int val )
{
	keyDown = val;
}

int Guy::getKeyUp() const
{
	return keyUp;
}

void Guy::setKeyUp( int val )
{
	keyUp = val;
}

int Guy::getKeyLeft() const
{
	return keyLeft;
}

void Guy::setKeyLeft( int val )
{
	keyLeft = val;
}

int Guy::getKeyRight() const
{
	return keyRight;
}

void Guy::setKeyRight( int val )
{
	keyRight = val;
}

bool Guy::isIn( int x, int y )
{
	if (estaMorto()) return false;
	return (this->x==x && this->y==y);
}

void Guy::avanca()
{
	if (estaMorto()) return;
	bool andou=false;

	if (automatico) {
		AutoMudaDireccao();
	}

	switch (direccao) {
	case d_baixo:
		if (getGame()->canGoDown(this,x,y,true))  {
			andou= true;
			lastY=y;
			y++;
		}
		break;
	case d_cima:
		if (getGame()->canGoUp(this,x,y,true)) {
			andou= true;
			lastY=y;
			y--;
		}
		break;
	case d_dto:
		if  (getGame()->canGoRight(this,x,y,true)) {
			andou= true;
			lastX=x;
			x++;
		}
		break;
	case d_esq :
		if (getGame()->canGoLeft(this,x,y,true)) {
			andou= true;
			lastX=x;
			x--;
		}

	} 


	if (andou) come();
}

void Guy::AutoMudaDireccao()
{
	//muda de direcao
	Direccao nova;
	bool c= getGame()->canGoUp(this,x,y,false);
	bool e= getGame()->canGoLeft(this,x,y,false);
	bool d= getGame()->canGoRight(this,x,y,false);
	bool b= getGame()->canGoDown(this,x,y,false);
	bool muda=false;

	if (c && !e && !d && !b) {
		direccao = d_cima;
		return;
	}
	if (!c && e && !d && !b) {
		direccao = d_esq;
		return;
	}
	if (!c && !e && d && !b) {
		direccao = d_dto;
		return;
	}
	if (!c && !e && !d && b) {
		direccao = d_baixo;
		return;
	}



	int dd= random_l_h(0,3);
	nova = (Direccao)dd;

	 muda=true;
	switch (nova) {
	case d_baixo:
		if ( direccao == d_cima ) muda=false;
		if (!b) muda=false;
		break;
	case d_cima:
		if ( direccao == d_baixo ) muda=false;
		if (!c)  muda=false;
		break;
	case d_dto:
		if ( direccao == d_esq ) muda=false;
		if (!d)  muda=false;
		break;
	case d_esq :
		if ( direccao == d_dto ) muda=false;
		if (!e)  muda=false;
	} 

	if  (muda)  {
		direccao = nova;
	}	

}

void Guy::setXY( int x, int y )
{
   if (x<0 || y<0 || x>TAB_MAX_X || y >TAB_MAX_Y) return;
   lastX=this->x;
   lastY=this->y; 	
   this->x=x;
   this->y=y;
}

void Guy::foiAtacadoPor( Guy *guy )
{
	
}   

void Guy::morre()
{
	morreu=true;
	getGame()->GuyMorreu(this);
}

bool Guy::estaMorto() const
{
	return morreu;
}

void Guy::setDireccao( Direccao direccao )
{
	this->direccao=direccao;
}

Direccao Guy::getDireccao()const
{
	return direccao;
}

int Guy::getLastX() const
{
	return lastX;
}

int Guy::getlastY() const
{
	return lastY;
}

bool Guy::getAuto()const
{
	return automatico;
}

bool Guy::isGoodGuy() const
{
	return true;
}

bool Guy::isLastIn( int lastx, int lasty )
{
	if (estaMorto()) return false;
	return (this->lastX==lastx && this->lastY==lasty);
}


