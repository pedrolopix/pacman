#include <windows.h>
#include <stdio.h>
#include "BaseThread.h"


BaseThread::BaseThread() {
  hTread = NULL;
  terminated = false;
}

BaseThread::~BaseThread(void) {
  terminated = true;
  wait(INFINITE);
}

void BaseThread::execute() {

}

void BaseThread::start() {
  if (hTread == NULL) {
    hTread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) ThreadCall, (LPVOID) this, 0, &id);
  } else {
    ResumeThread(hTread);
  }
}

void BaseThread::stop() {
  if (hTread != NULL) SuspendThread(hTread);
}

DWORD WINAPI BaseThread::ThreadCall(LPVOID param) {
  BaseThread *bt = (BaseThread*) param;
  return bt->internalExecute();
}

DWORD BaseThread::internalExecute() {
  execute();
  ExitThread(hTread);
  terminated = true;
  hTread = 0;
  return 0;
}

bool BaseThread::getTerminated() const {
  return terminated;
}

void BaseThread::setTerminated() {
  terminated = true;
}

void BaseThread::wait( DWORD miliseconds )
{
	if (hTread != NULL) WaitForSingleObject(hTread, miliseconds);
}

