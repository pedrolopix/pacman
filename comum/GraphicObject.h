#ifndef GraphicObjectH
#define GraphicObjectH
#include <windows.h>

class Application;
class GameBase;

class GraphicObject
{
private:
	GameBase *game;
	GraphicObject(const GraphicObject &go);
	GraphicObject & operator = (GraphicObject & go);
protected:
	int translateCoordX(int x);
	int translateCoordY(int y);
	int getWidth();
	int getHeight();
public:
	GraphicObject(GameBase *game);
	virtual void draw(HDC &hdc,PAINTSTRUCT &PtStc);
	virtual void doKeyDown( WPARAM wParam, LPARAM lParam );
	GameBase *getGame();
	virtual ~GraphicObject(void);
};

#endif
