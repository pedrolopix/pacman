#ifndef serviceH
#define serviceH

#define NOME_SERVICO "PACMAN-PL"
#define NOME_SERVICO_DESC "Pacman by Pedro Lopes"

//#define DEBUG_CONSOLA

bool instalaServico();
bool desinstalaServico();
bool startServico();
bool stopServico();


#endif