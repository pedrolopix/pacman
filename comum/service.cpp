#include <windows.h>
#include <cpl.h>
#include <stdio.h>
#include <string.h>
#include "..\comum\service.h"
#include "..\comum\Utils.h"
#include "..\comum\Config.h"

bool instalaServico() {


	SC_HANDLE schSCManager,schService;
	schSCManager = OpenSCManager( 
		NULL,                    // local machine 
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (schSCManager == NULL)  {
		//ErrorExit("Erro: OpenSCManager");
		//MessageBox(hwnd, (LPCTSTR)"Erro: OpenSCManager", (LPCTSTR)"PACMAN", MB_OK);
		return false;
	}

	Config cfg;
	cfg.load();
	char lpszBinaryPathName[MAX_PATH];
	strcpy_s((char *)lpszBinaryPathName,MAX_PATH,cfg.getServerName().c_str());


	//MessageBox(hwnd, lpszBinaryPathName, (LPCTSTR)"PACMAN", MB_OK);

	schService = CreateService( 
		schSCManager,              // SCManager database 
		NOME_SERVICO,               // name of service 
		NOME_SERVICO_DESC,           // service name to display 
		SERVICE_ALL_ACCESS,        // desired access 
		SERVICE_WIN32_OWN_PROCESS, // service type 
		SERVICE_AUTO_START,
		//SERVICE_DEMAND_START,      // start type 
		SERVICE_ERROR_NORMAL,      // error control type 
		lpszBinaryPathName,        // service's binary 
		NULL,                      // no load ordering group 
		NULL,                      // no tag identifier 
		NULL,                      // no dependencies 
		NULL,                      // LocalSystem account 
		NULL);                     // no password 

	if (schService == NULL) 
		return false;
	

	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager); 
	return true;
}

bool desinstalaServico() {
	SC_HANDLE schSCManager,schService;
	schSCManager = OpenSCManager( 
		NULL,                    // local machine 
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (schSCManager == NULL) 
		return false;

	schService = OpenService( 
		schSCManager,       // SCManager database 
		NOME_SERVICO,       // name of service 
		DELETE);            // only need DELETE access 

	if (schService == NULL) 
		return false;


	if (! DeleteService(schService) )         
		return false;

	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager); 
	return true;
}

bool startServico() {
	SC_HANDLE schSCManager,schService;
	schSCManager = OpenSCManager( 
		NULL,                    // local machine 
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (schSCManager == NULL) 
		return false;

	schService = OpenService( 
		schSCManager,       // SCManager database 
		NOME_SERVICO,       // name of service 
		SERVICE_START);     // starts

	if (schService == NULL) 
		return false;


	if (! StartService(schService,0,NULL) )         
		return false;



	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager); 
	return true;
}


bool stopServico() {
	SERVICE_STATUS status;
	SC_HANDLE schSCManager,schService;
	schSCManager = OpenSCManager( 
		NULL,                    // local machine 
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (schSCManager == NULL) 
		return false;

	schService = OpenService( 
		schSCManager,       // SCManager database 
		NOME_SERVICO,       // name of service 
		SERVICE_STOP);     // starts

	if (schService == NULL) 
		return false;

	if (! ControlService(schService,SERVICE_CONTROL_STOP,&status))         
		return false;



	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager); 
	return true;
}
