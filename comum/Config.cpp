#include "Config.h"
#include <winreg.h>
#include "..\comum\utils.h"

using namespace std;

#define key "SOFTWARE\\plpacman\\"



Config::Config()
{
	host=".";
	username="admin";
	password="admin";
	saveLogin=true;
	load();
}

Config::~Config()
{
}

std::string Config::getHost() const
{
	return host;
}

void Config::setHost( std::string val )
{
	host = val;
}

bool Config::getSaveLogin() const
{
	return saveLogin;
}

void Config::setSaveLogin( bool val )
{
	saveLogin = val;
}

std::string Config::getUsername() const
{
	return username;
}

void Config::setUsername( std::string val )
{
	username = val;
}

std::string Config::getPassword() const
{
	return password;
}

void Config::setPassword( std::string val )
{
	password = val;
}



LONG Config::getValue(HKEY hKey, const std::string &strValueName, std::string &strValue, const std::string &strDefaultValue)
{

	LONG Ret;
	CHAR value[512];
	DWORD dwType = REG_SZ;
	DWORD size= sizeof(value);

	Ret = ::RegQueryValueEx(hKey,strValueName.c_str(),NULL,&dwType,(BYTE*)(LPCTSTR)value,&size);   

	if( Ret== ERROR_SUCCESS)
	{
		Ret = TRUE;
		strValue = (LPCTSTR)value;
	}
	else
	{
		strValue=strDefaultValue;
	}

	return Ret;
}

LONG Config::getValue( HKEY hKey, const std::string &strValueName, DWORD &value, const DWORD &DefaultValue )
{
	LONG Ret;
	DWORD dwType = REG_DWORD;
	DWORD size= sizeof(value);
	Ret = ::RegQueryValueEx(hKey,strValueName.c_str(),NULL,&dwType,(LPBYTE)&value,&size);   
	if( Ret== ERROR_SUCCESS)
	{
		Ret = TRUE;
	}
	else
	{
		value=DefaultValue;
	}

	return Ret;
}



LONG Config::setValue(HKEY hKey, const std::string &strValueName,const std::string &strValue)
{
	LONG reg;
	reg=::RegSetValueEx(hKey, (LPCSTR)strValueName.c_str(), 0, REG_SZ, (LPBYTE)(LPCTSTR)strValue.c_str(), strValue.length());

	if( reg== ERROR_SUCCESS)
	{
		::RegFlushKey(hKey);
		return reg;
	}
	return reg;

}

LONG Config::setValue(HKEY hKey, const std::string &strValueName,const DWORD &value)
{
	LONG reg;
	reg=::RegSetValueEx(hKey, (LPCSTR)strValueName.c_str(), 0, REG_DWORD,(LPBYTE) &value, sizeof(value));

	if( reg== ERROR_SUCCESS)
	{
		::RegFlushKey(hKey);
		return reg;
	}
	return reg;

}


void Config::load()
{
	HKEY hKey;
	LONG lRes = RegOpenKeyEx(HKEY_CURRENT_USER, key, 0, KEY_READ | KEY_QUERY_VALUE, &hKey);
	if (lRes != ERROR_SUCCESS) return;

	string str;
	getValue(hKey, "root",root,root);
	getValue(hKey, "host", host, host);
	getValue(hKey, "username", username, username);
	getValue(hKey, "password", password, password);
	getValue(hKey, "saveLogin",str,str);
	saveLogin=(str!="0");
}

void Config::save()
{
    string str;
	HKEY hKey;
	LONG lRes = RegOpenKeyEx(HKEY_CURRENT_USER, key, 0, KEY_ALL_ACCESS, &hKey);
	if (lRes==2) {
		lRes=RegCreateKeyEx(HKEY_CURRENT_USER,key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
	}
	
	if (lRes != ERROR_SUCCESS) {
		ErrorExit("A gravar registo");
		return;
	}
	setValue(hKey, "root",root);
	setValue(hKey,"host", host);
	setValue(hKey, "username", username);
	setValue(hKey, "password", password);
	str="0";
	if (saveLogin) str="1";
	setValue(hKey, "saveLogin",str);

}

std::string Config::getRoot()
{
	return root;
}

void Config::setRoot( std::string root )
{
	this->root=root;
}

std::string Config::getServerName()
{
	return root+"\\pacmansrv.exe";
}
