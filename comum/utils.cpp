#include "utils.h"
#include <time.h>
#include <windows.h>
#include <Strsafe.h>
#include <sstream>


int random_l_h( int min, int max )
{
	return min + rand() % (max - min + 1);
}

void init_rand()
{
	srand((unsigned)time(NULL));
}


void ShowError(std::string info) {
	// Retrieve the system error message for the last-error code

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) & lpMsgBuf,
		0, NULL);

	// Display the error message and exit the process

	lpDisplayBuf = (LPVOID) LocalAlloc(LMEM_ZEROINIT,(lstrlen((LPCTSTR) lpMsgBuf) + lstrlen((LPCTSTR) info.c_str()) + 40) * sizeof (TCHAR));
	StringCchPrintf((LPTSTR) lpDisplayBuf,LocalSize(lpDisplayBuf) / sizeof (TCHAR),TEXT("%s failed with error %d: %s"),info.c_str(), dw, lpMsgBuf);

	MessageBox(NULL, (LPCTSTR) lpDisplayBuf, TEXT("Error"), MB_OK);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}

void ErrorExit(std::string info) {
	// Retrieve the system error message for the last-error code

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) & lpMsgBuf,
		0, NULL);

	// Display the error message and exit the process

	lpDisplayBuf = (LPVOID) LocalAlloc(LMEM_ZEROINIT,(lstrlen((LPCTSTR) lpMsgBuf) + lstrlen((LPCTSTR) info.c_str()) + 40) * sizeof (TCHAR));
	StringCchPrintf((LPTSTR) lpDisplayBuf,LocalSize(lpDisplayBuf) / sizeof (TCHAR),TEXT("%s failed with error %d: %s"),info.c_str(), dw, lpMsgBuf);

	MessageBox(NULL, (LPCTSTR) lpDisplayBuf, TEXT("Error"), MB_OK);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
	ExitProcess(dw);
}

std::string MillisecondsToStr( int miliseconds )
{
	char str[80];
	int t = miliseconds;
	int h = (t / (1000 * 60 * 60));
	int m = (t % (1000 * 60 * 60)) / (1000 * 60);
	int s = ((t % (1000 * 60 * 60)) % (1000 * 60)) / 1000;
	sprintf_s(str, "%02d:%02d:%02d", h, m, s);
	return str;
}

std::string getAppPath() {
   return GetPathFrom(getDLL(0));
}

std::string getDLL(HMODULE h) {
   
	char    szAppPath[MAX_PATH] = "";
	std::string strAppName;

	::GetModuleFileName(h, szAppPath, MAX_PATH);
   strAppName = szAppPath;
	
	return strAppName;
}

std::string GetPathFrom (const std::string str)
{
  size_t found;
  //cout << "Splitting: " << str << endl;
  found=str.find_last_of("/\\");
  return str.substr(0,found);
  //cout << " folder: " << str.substr(0,found) << endl;
  //cout << " file: " << str.substr(found+1) << endl;
}