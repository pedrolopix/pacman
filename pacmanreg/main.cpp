#include "..\comum\service.h"
#include <stdio.h>
#include <string.h>
#include "..\comum\utils.h"
#include "..\comum\config.h"

int main( int argc, const char* argv[] ) {
	printf("pacmanreg\n\n");
	if (argc<=1) {
		printf("usar: \n");
		printf("\tpacmanreg reg\n");
		printf("\tpacmanreg unreg\n");
		return 0;
	}
	if (strcmp(argv[1],"reg")==0) {
		printf("a instalar servico....\n");
		Config cfg;
		cfg.load();
		cfg.setRoot(getAppPath());
		cfg.save();
		if (instalaServico()) {printf("servico instalado....\n");} else {printf("erro a instalar servico....\n");}
		if (startServico()) {printf("servico iniciado....\n");} else {printf("erro a iniciar servico....\n");}

	} else
		if (strcmp(argv[1],"unreg")==0) {
			printf("a desinstalar servico....\n");
			if (stopServico()) {printf("servi�o parado....\n");} else {printf("erro a parar servi�o....\n");}
			if (desinstalaServico()) {printf("servico desinstalado....\n");} else {printf("erro a desinstalar servi�o....\n");}
		} else
		{
			printf("commando desconhecido\n");
		}


}