#include <windows.h>
#include <cpl.h>
#include "resource.h"
#include <stdio.h>
#include <string.h>
#include "..\comum\service.h"
#include "..\comum\Utils.h"

HINSTANCE id_dll;
HANDLE hWTimer;


BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved){
    if(fdwReason == DLL_PROCESS_ATTACH){	//Processo se carregou a dll, so da primeira vez q esta � fisicamente carregada em memoria
        id_dll = hinstDLL;
    }
    return 1;
}



void queryServico(HWND hwnd) {
	SERVICE_STATUS status;
	SC_HANDLE schSCManager,schService;

	EnableWindow( GetDlgItem( hwnd, IDC_BUTTON_START ), FALSE );
	EnableWindow( GetDlgItem( hwnd, IDC_BUTTON_STOP ), FALSE );
	EnableWindow( GetDlgItem( hwnd, IDC_BUTTON_INSTALA ), FALSE );
	EnableWindow( GetDlgItem( hwnd, IDC_BUTTON_DESINSTALA ), FALSE );

	schSCManager = OpenSCManager( 
		NULL,                    // local machine 
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (schSCManager == NULL) 
		return;

	schService = OpenService( 
		schSCManager,       // SCManager database 
		NOME_SERVICO,       // name of service 
		SERVICE_QUERY_STATUS);     // starts

	if (schService == NULL)  {
		CloseServiceHandle(schSCManager); 
		EnableWindow( GetDlgItem( hwnd, IDC_BUTTON_INSTALA ), TRUE );
		return;
	}


	if (!QueryServiceStatus(schService,&status)) {
		CloseServiceHandle(schService); 
		CloseServiceHandle(schSCManager); 
		return;
	} 
	




	switch (status.dwCurrentState) {
	case SERVICE_PAUSED:
	case SERVICE_STOPPED:
		EnableWindow( GetDlgItem( hwnd, IDC_BUTTON_START ), TRUE );
		EnableWindow( GetDlgItem( hwnd, IDC_BUTTON_DESINSTALA ), TRUE );
		break;
	case SERVICE_RUNNING:
		EnableWindow( GetDlgItem( hwnd, IDC_BUTTON_STOP ), TRUE );
		EnableWindow( GetDlgItem( hwnd, IDC_BUTTON_DESINSTALA ), FALSE );
		break;
	}


	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager); 
}


DWORD WINAPI checkStatus(LPVOID param){
	int periodo=1000; 
	LARGE_INTEGER li;

	HANDLE hWTimerLocal=CreateWaitableTimer(NULL,0,NULL);
	li.QuadPart=-10000000; //1 segundo
	//MessageBox((HWND)param, (LPCTSTR)"init dialog", (LPCTSTR)"Control Panel Application", MB_OK);
	SetWaitableTimer(hWTimerLocal,&li,periodo,NULL,NULL,0);
	while(TRUE){
		//MessageBox((HWND)param, (LPCTSTR)"init dialog", (LPCTSTR)"Control Panel Application", MB_OK);
		//queryServico((HWND)param);
		PostMessage((HWND)param,WM_USER+1,0,0);
		WaitForSingleObject(hWTimerLocal,INFINITE);
	}
	return 0;
}



BOOL CALLBACK dialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
	case WM_INITDIALOG:
		
		LARGE_INTEGER li;
		CreateThread(NULL,0,checkStatus,(LPVOID)hDlg,0,NULL);
		li.QuadPart=-100000000;
		//timer nao periodico
		SetWaitableTimer(hWTimer,&li,0,NULL,NULL,0);
		break;
	case WM_USER+1:
		queryServico(hDlg);
		break;
	case WM_COMMAND:
            if(LOWORD(wParam)== IDFECHA)
            {
                EndDialog(hDlg,1);
                return 1;
            }
			if(LOWORD(wParam)== IDC_BUTTON_INSTALA) {
				if (instalaServico()) {
					MessageBox(hDlg, (LPCTSTR)"Servi�o instalado com sucesso!", (LPCTSTR)"PACMAN", MB_OK);
				} else {
					ShowError("Erro a instalar servi�o");
				}
			}
			if(LOWORD(wParam)== IDC_BUTTON_DESINSTALA) {
				if (desinstalaServico())  {
					MessageBox(hDlg, (LPCTSTR)"Servi�o desinstalado com sucesso!", (LPCTSTR)"PACMAN", MB_OK);
				} else {
					ShowError("Erro a desinstalar servi�o");
				}
			}
			if(LOWORD(wParam)== IDC_BUTTON_STOP) {
				if (stopServico())  {
					MessageBox(hDlg, (LPCTSTR)"Servi�o parado com sucesso!", (LPCTSTR)"PACMAN", MB_OK);
				} else {
					ShowError("Erro a parar servi�o");
				}
			}
			if(LOWORD(wParam)== IDC_BUTTON_START) {
				if (startServico())  {
					MessageBox(hDlg, (LPCTSTR)"Servi�o iniciado com sucesso!", (LPCTSTR)"PACMAN", MB_OK);
				} else {
					ShowError("Erro a iniciar servi�o");
				}
			}
			break;
		case WM_CLOSE:
			EndDialog(hDlg,1);
            return 1;
    }
	//queryServico(hDlg);
    return 0;
}






LONG CALLBACK CPlApplet(HWND hwnd,UINT Msg,LPARAM lParam1,LPARAM lParam2){

    switch(Msg){
		/*case CPL_INIT:
			return TRUE;
		case CPL_GETCOUNT:
			return 1;*/
        case CPL_INQUIRE:
            //Preenchimento da estrutura CPLINFO com o valor 0, forcando assim ao sistema ir buscar a informa��o ao NEWINQUIRE
            //A diferen�a entre o INQUIRE e o NEWINQUIRE � que este primeiro faz cache e o ultimo n�o. Se quisermos alterar os 
			//dados temos que usar o NEWINQUIRE sen�o podemos usar o outro porque � mais r�pido.
            {
                LPCPLINFO lpCPlInfo;
                lpCPlInfo=(LPCPLINFO) lParam2;
				lpCPlInfo->idIcon=IDI_ICON1;
                lpCPlInfo->idName=0;
                lpCPlInfo->idInfo=0;
                lpCPlInfo->lData=0;
				
            }
            //MessageBox(hwnd, (LPCTSTR)"INQUIRE", (LPCTSTR)"Control Panel Application", MB_OK);
            break;

        case CPL_NEWINQUIRE:
            //No NEWINQUIRE a unica coisa que preenchemos � o icon, o nome e a descricao da cpl.
            {

                LPNEWCPLINFO lpCPlInfo;
                lpCPlInfo=(LPNEWCPLINFO) lParam2;
                lpCPlInfo->dwSize=sizeof(NEWCPLINFO);
                lpCPlInfo->dwFlags=0;
                lpCPlInfo->dwHelpContext=0;
                
                lpCPlInfo->hIcon=LoadIcon(id_dll,MAKEINTRESOURCE(IDI_ICON1));    
                strcpy_s(lpCPlInfo->szInfo, 64, NOME_SERVICO_DESC);
                strcpy_s(lpCPlInfo->szName, 32, "Pacman");

                lpCPlInfo->szHelpFile[0]=0;
                lpCPlInfo->lData=0;
				
                //MessageBox(hwnd, (LPCTSTR)GetPathFrom(getDLL(id_dll)).c_str(), (LPCTSTR)"NEWINQUIRE", MB_OK);
            }
            
            break;

        case CPL_EXIT:
            //MessageBox(hwnd, (LPCTSTR)"Exit", (LPCTSTR)"Control Panel Application", MB_OK);
            break;
        case CPL_GETCOUNT:
            //MessageBox(hwnd, (LPCTSTR)"GetCount", (LPCTSTR)"Control Panel Application", MB_OK);
            break;
        case CPL_DBLCLK:
            DialogBox(id_dll,MAKEINTRESOURCE(IDD_DIALOG1),hwnd,dialogProc);
            break;
        case CPL_INIT:
            //MessageBox(hwnd, (LPCTSTR)"Init", (LPCTSTR)"Control Panel Application", MB_OK);
            break;
    }
    return 1;
}




