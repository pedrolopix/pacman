#include "DialogTop10.h"
#include "resource.h"
#include "PacmanApp.h"
#include <sstream>

using namespace std;

DialogTop10::DialogTop10(PacmanApp *app) : Dialog(app, IDD_TOP10) {
}



DialogTop10::~DialogTop10()
{
}

BOOL DialogTop10::dialogProc( HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam )
{
	switch (messg) {
	case WM_COMMAND:
		switch(LOWORD(wParam)) 
		{ 
		case IDOK:

			setModalResult(true);
			close();
			return 0;
		}   

	}	
	return Dialog::dialogProc(hWnd,messg,wParam,lParam);
}

void DialogTop10::doInit()
{
  CbufTop10 top10;
  getApp()->getRemote()->getTop10(top10);
  stringstream str1;
  str1 << "Nome" << "          " << "Pontos"<< "          " << "Vitórias" << "          " << "Desistências" <<"          " << "Derroras";
  SendDlgItemMessage(getHWnd(), IDC_TOP10, LB_ADDSTRING, 0, (LPARAM)(str1.str().c_str()));
  stringstream str2;
  str2 << "==============================================================================";
  SendDlgItemMessage(getHWnd(), IDC_TOP10, LB_ADDSTRING, 0, (LPARAM)(str2.str().c_str()));
  for(int i=0;i<top10.count; i++) {
	  stringstream str;
	  str << top10.jogadores[i].nome << "            ";
	  str << top10.jogadores[i].NPontos<<  "                      ";
	  str << top10.jogadores[i].NVitorias << "                      ";
	  str << top10.jogadores[i].NDesistencias <<"                      ";
	  str << top10.jogadores[i].NDerroras;
	  SendDlgItemMessage(getHWnd(), IDC_TOP10, LB_ADDSTRING, 0, (LPARAM)(str.str().c_str()));
  }



}
