#include "GameDemo.h"
#include "Tabuleiro.h"
#include "..\comum\Guy.h"
#include "GoodGuy.h"
#include "BadGuy.h"
#include "PacmanApp.h"
#include <stdio.h>
#include "Game.h"
#include "GameLocal.h"

GameDemo::GameDemo(PacmanApp *PacmanApp):GameLocal(PacmanApp,500,4,10,false)
{
	contador=0;
	Guy *g1=new GoodGuy(this,"Pedro Lopes",5);
	g1->setAuto(true);
	addGoodGuy(g1);
	Guy *g2=new GoodGuy(this,"Tiago",5,false,31,1);
	g2->setAuto(true);
	addGoodGuy(g2);
	getTheGuy()->setAuto(true);
	setInfo("Demonstração");
}


GameDemo::~GameDemo(void)
{
}

void GameDemo::draw(HDC &hdc,PAINTSTRUCT &PtStc )
{
	GameBase::draw(hdc,PtStc);
	//contador++;
	//char str[80]="\0";
	//sprintf_s(str, 80, "Entradas em WM_PAINT: %d", contador);
	//TextOut(hdc,0,0,str,(int)strlen(str));
}

void GameDemo::terminou()
{
	GameBase::terminou();
	PostMessage(getApp()->getHWnd(),WM_USER+1,0,0);
}
