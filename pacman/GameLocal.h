#ifndef GameLocalH
#define GameLocalH
#include "Game.h"
#include "..\comum\GoodGuyBase.h"
#include "GameThread.h"
class PacmanApp;

class GameLocal:public Game 
{
private:
	GoodGuyBase *theguy;
	GameThread *gamethread;
	GameLocal(const GameLocal &a);
	GameLocal & operator = (const GameLocal &a);
public:
	GameLocal(PacmanApp *PacmanApp, int nObjectos,int nBadGuys, int nAtaques,bool inicioAuto);

	void addBadGuys( int nBadGuys );

	virtual void terminou();
    virtual void defkeys(int left, int right, int up, int down);
	virtual void desistir();
	virtual void start();
	virtual GoodGuyBase *getTheGuy();
	virtual ~GameLocal();

};


#endif

