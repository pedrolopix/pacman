#ifndef BadGuyH
#define BadGuyH
#include "..\comum\BadGuyBase.h"

class Game;

class BadGuy: public BadGuyBase
{
private:
	BadGuy(const BadGuy &a);
	BadGuy & operator = (const BadGuy &a);
public:
	BadGuy(Game *game, GuyCor cor, int x=1, int y=1);
	virtual ~BadGuy(void);
};

#endif