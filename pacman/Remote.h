#pragma once
#include <windows.h>
#include <string>
#include "ComunicationThread.h"
#include <windef.h>

class PacmanApp;

class Remote
{
private:
	HANDLE hPipe;
	bool isLoggedin;
	std::string host;
	std::string username;
	PacmanApp *app;
	ComunicationThread *comht;
	bool writePipe(const Commands command,const LPVOID buf,const int len);
	bool readPipe(LPVOID buf, int len);
	bool readCmd(CommandBuf &cmd);
	Remote(const Remote &r);
	Remote & operator = (const Remote & r);
public:
	Remote(PacmanApp *app);
	bool connect(const std::string &host);
	bool login(const std::string username,const  std::string password, CBufUserProp &user);
	void loggoff();
	bool getUsers( CBufUserList&ul );
	void addUser(const std::string username, const std::string password,const bool isAdmin );
	bool userDelete( const std::string username );
	bool getUser( const std::string username, CBufUser& user );
	void cmdError( std::string msg, bool flushpipe );
	bool iniciarJogo(int nObjectos,int nBadGuys, int nAtaques, bool inicioAuto, int xini,int yini);
	bool ligarJogo(bool inicioAuto, int xini,int yini);
	~Remote(void);	
	void sendkeyDown(int key);
	void desisteJogo();
	bool getIsLoggedin();
	void defkeys( int left, int right, int up, int down );
	std::string getUserName()const;
	void getTop10( CbufTop10 &top10 );
};

