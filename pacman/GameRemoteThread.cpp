#include <string>
#include "GameRemoteThread.h"
#include "PacmanApp.h"
#include "..\comum\Comunication.h"
#include "GameRemote.h"
#include "..\comum\utils.h"

using namespace std;

GameRemoteThread::GameRemoteThread(GameRemote *game, std::string host)
{
	this->game=game;
	this->host=host;
	hPipe=0;
}


GameRemoteThread::~GameRemoteThread()
{
	if (hPipe!=0 ) CloseHandle(hPipe);
}


bool GameRemoteThread::connectGame()
{
	string pipename="\\\\"+host+PIPE_NAME_GAME;
	if (!WaitNamedPipe(pipename.c_str(), TIMEOUT)) {
		//app->showMessage("Erro a ligar ao servidor...");
		ErrorExit("pipe");
		return false;
	}
	hPipe= CreateFile(pipename.c_str(), GENERIC_WRITE|GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hPipe==NULL) {
		//app->showMessage("Erro a ligar ao servidor...");
		ErrorExit("pipe");
		return false;
	}
	return true;
}

void GameRemoteThread::execute()
{
    CbufGame buf;
	int len=sizeof(CbufGame);
	int n=0;
	//read pipe
	//connect pipe server
	if (!connectGame()) return;
	while (1) {
		int ret, n;
		ret = ReadFile(hPipe, &buf, len,(LPDWORD) &n, NULL);
		if (!ret || !n)  {ErroPipe(); return;}

		if (len!=n) {ErroPipe(); return;}

		processa(buf);

		if (getTerminated()) return;
	}
	if (hPipe!=0) CloseHandle(hPipe);
	hPipe=0;
}

void GameRemoteThread::ErroPipe()
{
	
}

void GameRemoteThread::processa(CbufGame &buf )
{
	game->processa(buf);
}
