#include "GoodGuy.h"
#include "Game.h"
#include "resource.h"

GoodGuy::GoodGuy( Game *game,std::string nome, int vida,bool inicioAuto, int x/*=1*/, int y/*=1*/ ):GoodGuyBase(game,nome,vida,inicioAuto,x,y)
{
	loadBitmap(d_baixo,IDB_PACMAN_BAIXO);
	loadBitmap(d_cima,IDB_PACMAN_CIMA);
	loadBitmap(d_esq,IDB_PACMAN_ESQ);
	loadBitmap(d_dto,IDB_PACMAN_DTO);
}


GoodGuy::~GoodGuy(void)
{
}
