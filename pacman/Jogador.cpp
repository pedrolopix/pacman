#include "Jogador.h"
#include "PacmanApp.h"

Jogador::Jogador(PacmanApp *PacmanApp)
{
	keyLeft= VK_LEFT;
	keyRight= VK_RIGHT;
	keyUp= VK_UP;
	keyDown= VK_DOWN;
	prefNObjectos=100;
	prefNBadGuys=4;
	prefNAtaques=10;
	posXini=1;
	posYini=1;
	isAdmin=false;
	nome="demo";
}


Jogador::~Jogador()
{
}

int Jogador::getPosXini() const
{
	return posXini;
}

void Jogador::setPosXini( int val )
{
	posXini = val;
}

int Jogador::getPosYini() const
{
	return posYini;
}

void Jogador::setPosYini( int val )
{
	posYini = val;
}

int Jogador::getKeyLeft() const
{
	return keyLeft;
}

void Jogador::setKeyLeft( int val )
{
	keyLeft = val;
}

int Jogador::getKeyRight() const
{
	return keyRight;
}

void Jogador::setKeyRight( int val )
{
	keyRight = val;
}

int Jogador::getKeyUp() const
{
	return keyUp;
}

void Jogador::setKeyUp( int val )
{
	keyUp = val;
}

int Jogador::getKeyDown() const
{
	return keyDown;
}

void Jogador::setKeyDown( int val )
{
	keyDown = val;
}

int Jogador::getTotalPontos() const
{
	return totalPontos;
}

void Jogador::setTotalPontos( int val )
{
	totalPontos = val;
}

int Jogador::getTotalJogos() const
{
	return totalJogos;
}

void Jogador::setTotalJogos( int val )
{
	totalJogos = val;
}

int Jogador::getPrefNObjectos() const
{
	return prefNObjectos;
}

void Jogador::setPrefNObjectos( int val )
{
	prefNObjectos = val;
}

int Jogador::getPrefNBadGuys() const
{
	return prefNBadGuys;
}

void Jogador::setPrefNBadGuys( int val )
{
	prefNBadGuys = val;
}

int Jogador::getPrefNAtaques() const
{
	return prefNAtaques;
}

void Jogador::setPrefNAtaques( int val )
{
	prefNAtaques = val;
}

void Jogador::setPosXY( int xx, int yy )
{
	posXini=xx;
	posYini=yy;
}

void Jogador::setNome( std::string nome )
{
	this->nome= nome;
}

void Jogador::setIsAdmin( bool isAdmin )
{
	this->isAdmin= isAdmin;
}

bool Jogador::getIsAdmin() const
{
	return isAdmin;
}
