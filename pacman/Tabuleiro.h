#pragma once
#include "..\comum\TabuleiroBase.h"
class Game;

class Tabuleiro :public TabuleiroBase
{
private:
	HBITMAP hbitBARREIRA_TOPESQUERDO;
	HBITMAP hbitBARREIRA_TOPDIREITO;
	HBITMAP hbitBARREIRA_BOTTOMESQUERDO;
	HBITMAP hbitBARREIRA_BOTTOMDIREITO;
	HBITMAP hbitBARREIRA_TCIMA;
	HBITMAP hbitPONTO;
	HBITMAP hbitBARREIRA_HORIZONTAL;
	HBITMAP hbitBARREIRA_ALTO;
	HBITMAP hbitVAZIO;
	HBITMAP hbitBARREIRA_TESQUERDO;
	HBITMAP hbitBARREIRA_TDIREITO;
	HBITMAP hbitBARREIRA_TBAIXO;
	HBITMAP hbitBARREIRA_CRUZ;
	void drawTabuleiro(HDC &hdc,PAINTSTRUCT &PtStc);
	Tabuleiro(const Tabuleiro &a);
	Tabuleiro & operator = (const Tabuleiro &a);
public:
	Tabuleiro(Game *game);
	virtual void drawTabuleiro(HDC &hdc, int x, int y);
	virtual void draw(HDC &hdc,PAINTSTRUCT &PtStc);
	virtual ~Tabuleiro(void);
};

