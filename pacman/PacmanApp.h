#ifndef PacmanAppH
#define PacmanAppH
#include "..\comum\Application.h"
#include <windows.h>
#include <string>
#include <vector>
#include "Game.h"
#include "DialogInfoJogo.h"
#include "..\comum\Config.h"
#include "Jogador.h"
#include "Remote.h"
#include "DialogLigarJogo.h"

enum Defkeys {dk_left, dk_right, dk_down, dk_up, dk_none};
enum PosicionaBoneco {pbnone, pbNovo, pbLigar, pblocal};

class DialogNovoJogo;


class PacmanApp: public Application
{
private:
	HINSTANCE hInst;
	HINSTANCE hPrevInst;
	LPSTR lpCmdLine;
	int nCmdShow;
	WNDCLASSEX wcApp;
	std::string progName;
	std::string progCaption;
	Config *config;
	HWND hWnd;
	HACCEL hAccel;
	Defkeys defkeys;
	PosicionaBoneco definePosition;
	GameBase *game;
	DialogInfoJogo *dlginfo;
	DialogNovoJogo *dlgNovoJogo;
	DialogLigarJogo *dlgLigarJogo;
	Jogador *jogador;
	Remote *remote;
	static LRESULT CALLBACK StaticWindowProc( HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	PacmanApp(const PacmanApp &app);
	PacmanApp & operator = (const PacmanApp & app);
protected:
	bool createPacmanApp(); 
	bool createWindow(); 
	void startDemo();
	void mnuNewGame();
	void mnuJoinGame();
	void mnuCloseGame();
	void mnuLogin();
	void mnuHelp();
	void mnuOpenJogoInfo();
	void mnuUtilzadores();
	void mnuTop10();
	void mnuNewGameLocal();
	void doPaint();
	void doKeyDown( WPARAM wParam, LPARAM lParam );
	void doMouseMove( WORD keys, int x, int y);
    void doUserLoggedin();
	void doNovoJogoLocal(int nObjectos,int nBadGuys, int nAtaques,bool inicioAuto);
	void doNovoJogo(int nObjectos,int nBadGuys, int nAtaques,bool inicioAuto);
public:
	PacmanApp(HINSTANCE hInst, HINSTANCE hPrevInst,LPSTR lpCmdLine, int nCmdShow);
	LRESULT WindowProc( HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	HINSTANCE getHInstance();
	HWND getHWnd();
	int run();
	~PacmanApp(void);
	bool initialization();
	std::string getProgCaption();
	GameBase * getGame();
	Remote *getRemote();
	Config *getConfig();
	void invalidate();
	void refreshInfo(GameBase *game);	
	void mnuDesistirJogo();
	Jogador *getJogador();
	void terminouJogo(int op);
	void startDefinePosition(PosicionaBoneco pb);
	void showMessage( std::string msg );
	void showMessageError( std::string msg );
	void disconneted();	
	void doLigarJogo( bool AutoInicial );
};

#endif