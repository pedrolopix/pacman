#ifndef DialogLigarJogoH
#define DialogLigarJogoH
#include "dialog.h"

class PacmanApp;

class DialogLigarJogo : public Dialog
{
private:
	bool inicioAuto;
protected:
	virtual BOOL dialogProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
public:
	DialogLigarJogo(PacmanApp *app);
	bool getAutoInical()const;
	~DialogLigarJogo(void);
};

#endif