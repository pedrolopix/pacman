#include "DialogNovoJogo.h"
#include "PacmanApp.h"
#include "resource.h"
#include "Dialog.h"
#include <winuser.h>
#include <string>
#include <sstream>

using namespace std;

DialogNovoJogo::DialogNovoJogo(PacmanApp *app) : Dialog(app, IDD_NOVOJOGO) 
{
	inicioAuto=false;
}


DialogNovoJogo::~DialogNovoJogo()
{
}

BOOL DialogNovoJogo::dialogProc( HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam )
{
	//	char str[20];

	switch (messg) {
	case WM_COMMAND:
		switch(LOWORD(wParam)) 
		{ 
		case IDC_NJ_POSICIONAR:
			if (actualizaJogador()) {
				setModalResult(false);
				close();
				getApp()->startDefinePosition(posicionaBoneco); 
			}
			return 0;
		case IDC_NJ_COMECAR:
			if (actualizaJogador()) {
				setModalResult(true);
				close();
			}
			return 0;
		case IDC_NJ_CANCELAR:
			setModalResult(false);
			close();
			return 0;
		}   

	}
	return Dialog::dialogProc(hWnd, messg, wParam, lParam);;
}

int DialogNovoJogo::getNObjectos()const
{
	return nObjectos;
}

int DialogNovoJogo::getNAtaques()const
{
	return nAtaques;
}

int DialogNovoJogo::getNBadGuys()const
{
	return nBadGuys;
}

bool DialogNovoJogo::getAutoInical()const
{
	return inicioAuto;
}


void DialogNovoJogo::doInit()
{
	setEditValue(IDC_NJ_NOBJECTOS,getApp()->getJogador()->getPrefNObjectos());
	setEditValue(IDC_NJ_NATAQUES,getApp()->getJogador()->getPrefNAtaques());
	setEditValue(IDC_NJ_NBADGUYS,getApp()->getJogador()->getPrefNBadGuys());

}

bool DialogNovoJogo::actualizaJogador()
{
	stringstream str;
	int max= getApp()->getGame()->getPontosTotal();
	int min=10;
	inicioAuto= (SendDlgItemMessage(getHWnd(), IDC_NJ_AUTO, BM_GETCHECK,0, 0)!=0);
	nObjectos = getEditIntValue(IDC_NJ_NOBJECTOS);
	if (nObjectos<min || nObjectos>max) {
		str << "N Objectos tem de estar entre " <<min<<" e " << max;
		getApp()->showMessageError(str.str());
		return false;
	}

	min=1;
	max=10;
	nAtaques= getEditIntValue(IDC_NJ_NATAQUES);
	if (nAtaques<min || nAtaques>max) {
		str << "N Ataques tem de estar entre " <<min<<" e " << max;
		getApp()->showMessageError(str.str());
		return false;
	}

	min=1;
	max=10;
	nBadGuys= getEditIntValue(IDC_NJ_NBADGUYS);
	if (nBadGuys<min || nBadGuys>max) {
		str << "N maus tem de estar entre " <<min<<" e " << max;
		getApp()->showMessageError(str.str());
		return false;
	}
	
	getApp()->getJogador()->setPrefNAtaques(nAtaques);
	getApp()->getJogador()->setPrefNBadGuys(nBadGuys);
    getApp()->getJogador()->setPrefNObjectos(nObjectos);
	return true;
}

void DialogNovoJogo::setPosicionaBoneco( PosicionaBoneco pb )
{
	this->posicionaBoneco=pb;
}

