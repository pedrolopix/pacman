#ifndef GameSetPositionH
#define GameSetPositionH
#include "Game.h"
#include "..\comum\GoodGuyBase.h"

class PacmanApp;

class GameSetPosition:public Game 
{
private:
	GoodGuyBase *theguy;
	GameSetPosition(const GameSetPosition &a);
	GameSetPosition & operator = (const GameSetPosition &a);
public:
	GameSetPosition(PacmanApp *app);
	virtual void defkeys( int keys[4] );
	virtual void doMouseMove( WORD keys, int x, int y );
	virtual ~GameSetPosition(void);
};

#endif