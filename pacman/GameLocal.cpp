#include "GameLocal.h"
#include "Tabuleiro.h"
#include "PacmanApp.h"
#include "BadGuy.h"
#include "GoodGuy.h"
#include "..\comum\utils.h"

GameLocal::GameLocal(PacmanApp *pacmanApp, int nObjectos,int nBadGuys, int nAtaques,bool inicioAuto):Game(pacmanApp)
{	

	gamethread=NULL;
	TabuleiroBase *t=new Tabuleiro(this);
	t->LoadInternal();
	t->SetPontos(nObjectos);
	setTabuleiro(t);

	addBadGuys(nBadGuys);


	this->theguy=new GoodGuy(this,"local", nAtaques, inicioAuto);
	addGoodGuy(this->theguy);

	setInfo("Jogo local");
	gamethread = new GameThread(pacmanApp);
}

void GameLocal::start() {	
	Game::start();
	gamethread->start();
}

GameLocal::~GameLocal(void)
{
	if (gamethread!=NULL) {
		delete gamethread;
		gamethread=NULL;
	}
}

void GameLocal::terminou()
{
	GameBase::terminou();
	PostMessage(getApp()->getHWnd(),WM_USER+1,1,0);
}

void GameLocal::desistir()
{
	GameBase::desistir();
	PostMessage(getApp()->getHWnd(),WM_USER+1,2,0);
}

void GameLocal::defkeys(int left, int right, int up, int down)
{
	theguy->setKeyDown(down);
	theguy->setKeyLeft(left);
	theguy->setKeyRight(right);
	theguy->setKeyUp(up);
}

GoodGuyBase * GameLocal::getTheGuy()
{
	return theguy;
}

void GameLocal::addBadGuys( int nBadGuys )
{
	int x;
	int y;
	for(int i=0; i<nBadGuys; i++) {
		GuyCor cor=(GuyCor) random_l_h(0,1);
		do {
			x= random_l_h(13,19);
			y= random_l_h(15,16);
		} while (isGuyIn(NULL,x,y,false));
		addBadGuy(new BadGuy(this,cor,x,y));
	}
}
