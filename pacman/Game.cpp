#include "Game.h"
#include "PacmanApp.h"
#include "..\comum\GameBase.h"
#include "..\comum\Guy.h"

Game::Game( PacmanApp *app ):GameBase(app)
{

}

void Game::avanca()
{

	HDC hdc = GetDC(getApp()->getHWnd()); 
	for (int i = 0; i < getNGuys(); i++) {
		getTabuleiro()->drawTabuleiro(hdc,getGuy(i)->getX(),getGuy(i)->getY());
	}

	GameBase::avanca();

	for (int i = 0; i < getNGuys(); i++) {
		getGuy(i)->draw(hdc);
	}

	ReleaseDC(getApp()->getHWnd(),hdc);

}

void Game::desistir()
{
	getApp()->terminouJogo(2);
}


Game::~Game()
{

}

PacmanApp * Game::getApp()
{
	return (PacmanApp *)(GameBase::getApp());
}

Tabuleiro * Game::getTabuleiro()
{
	return (Tabuleiro *)(GameBase::getTabuleiro());
}

