#include "Dialog.h"
#include "PacmanApp.h"
#include <winuser.h>
#include <sstream>


using namespace std;

Dialog::Dialog(PacmanApp *app, int idDialog) {
	this->app = app;
	this->idDialog = idDialog;
	hWnd = 0;
	modalResult=false;
}

Dialog::~Dialog(void) {
	close();
}

BOOL CALLBACK Dialog::staticDialogProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam) {
	int i;
	if (messg==WM_INITDIALOG) {
		i= lParam;
		SetWindowLong(hWnd, GWL_USERDATA, (long) lParam);
	} else {
		i = GetWindowLong(hWnd, GWL_USERDATA);
	}


	if (i != 0) {
		Dialog *dlg = (Dialog *) i;
		return dlg->dialogProc(hWnd, messg, wParam, lParam);
	}
	return 0;
}

BOOL Dialog::dialogProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam) {
	
	switch (messg) {
	case WM_INITDIALOG:
		this->hWnd=hWnd;
		doInit();
		return 1;
	case WM_COMMAND:
		break;
		/*switch(LOWORD(wParam)) 
		{ 
		case IDCCLOSE:
		DestroyWindow(hWnd);
		return 1;
		}   */
	case WM_CLOSE:
		close();
		return 1;
	}
	return 0;
}

void Dialog::doInit() {

}

void Dialog::show() {
	if (hWnd == 0) {
		hWnd = CreateDialogParam(app->getHInstance(), (LPCTSTR) idDialog, app->getHWnd(), (DLGPROC) staticDialogProc,(LPARAM)this);
		SetWindowLong(hWnd, GWL_USERDATA, (long) this);
	}
	ShowWindow(hWnd, SW_SHOW); 
}

bool Dialog::showModal()
{
	if (hWnd != 0) {
		DestroyWindow(hWnd);
		hWnd=0;
	}
	DialogBoxParam(app->getHInstance(), (LPCSTR) idDialog, app->getHWnd(), (DLGPROC) staticDialogProc,(LPARAM)this); 
	return modalResult;
}


void Dialog::close() {
	if (hWnd != 0) {
		EndDialog(hWnd, 0);
		DestroyWindow(hWnd);
		hWnd = 0;
	}
}

PacmanApp * Dialog::getApp() {
	return app;
}

HWND Dialog::getHWnd() const {
	return hWnd;
}

void Dialog::setModalResult( bool value )
{
	modalResult= value;
}

int Dialog::getEditIntValue(int id)
{
	char str[256];
	GetDlgItemText(getHWnd(),id,str,256);
	return  atoi(str);
}

std::string Dialog::getEditStrValue(int id)
{
	char str[256];
	GetDlgItemText(getHWnd(),id,str,256);
	return str;
}


void Dialog::setEditValue( int id, int value )
{
	stringstream ss;
	ss << value;
	SetDlgItemText(hWnd,id,ss.str().c_str());
}

void Dialog::setEditValue( int id, std::string value )
{
	SetDlgItemText(hWnd,id,value.c_str());
}

void Dialog::hide()
{
	ShowWindow(hWnd, SW_HIDE);
}
