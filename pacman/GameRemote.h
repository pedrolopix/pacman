#ifndef GameRemoteH
#define GameRemoteH
#include <Windows.h>
#include "game.h"
#include "GameRemoteThread.h"


class GameRemote :public Game
{
	bool firstCall;
	int pontosTotal;
	int pontosMax;
	int pontosFalta;
	int time;
	GameRemoteThread *gamethread;
	GameRemote(const GameRemote &a);
	GameRemote & operator = (const GameRemote &a);
protected:
	
	void updateGame( CbufGame & buf );	
public:
	GameRemote(PacmanApp *pacmanApp);
	virtual void doKeyDown(int key);
	virtual void start();
	virtual void avanca();
	virtual void desistir();
	virtual int getGameTime() const;
	virtual int getPontosTotal() const;
	virtual int getPontosMax() const;
	virtual int getPontosFalta() const;
	void processa( CbufGame & buf );
	~GameRemote(void);
};

#endif