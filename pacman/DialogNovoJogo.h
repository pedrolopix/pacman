#ifndef DialogNovoJogoH
#define DialogNovoJogoH
#include "Dialog.h"
#include "PacmanApp.h"

class DialogNovoJogo: public Dialog
{
private:
	int nObjectos;
	int nAtaques;
	int nBadGuys;
	bool inicioAuto;
	int keyLeft;
	int keyRitgh;
	int KeyUp;
	int KeyDown;
	PosicionaBoneco posicionaBoneco;
	DialogNovoJogo(const DialogNovoJogo &a);
	DialogNovoJogo & operator = (const DialogNovoJogo &a);
protected:
	virtual BOOL dialogProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	virtual void doInit();
	bool actualizaJogador();
public:
	DialogNovoJogo(PacmanApp *app);
	int getNObjectos()const;
	int getNAtaques()const;
	int getNBadGuys()const;
	bool getAutoInical()const;

	virtual  ~DialogNovoJogo(void);
	void setPosicionaBoneco( PosicionaBoneco pb );

};

#endif