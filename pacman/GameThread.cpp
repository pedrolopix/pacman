#include "PacmanApp.h"
#include "GameThread.h"


GameThread::GameThread(PacmanApp *app):BaseThread()
{
	this->app= app;
}


GameThread::~GameThread(void)
{

}

void GameThread::execute()
{
	do {
      app->refreshInfo(app->getGame());
	  app->getGame()->avanca();	
	  if (app->getGame()->getAcabouJogo()) return;
	  Sleep(150);

	} while (!getTerminated());
}

PacmanApp * GameThread::getApp() {
	return app;
}