//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by pacman.rc
//
#define IDR_MENU1                       101
#define IDR_ACCELERATOR1                102
#define IDB_PACMAN_FECHADO              103
#define IDD_STATUS                      103
#define IDD_NOVOJOGO                    104
#define IDB_BARREIRA_ALTO               106
#define IDB_BARREIRA_VERTICAL           106
#define IDB_BARREIRA_HORIZONTAL         107
#define IDB_PONTO                       108
#define IDB_BARREIRA_TOPESQUERDO        109
#define IDB_BARREIRA_TOPDIREITO         110
#define IDB_BARREIRA_BOTTOMESQUERDO     111
#define IDB_BARREIRA_BSAIXOESQUERDO     111
#define IDB_BARREIRA_BOTTOMDIREITO      112
#define IDB_BARREIRA_BAIXODIREITO       112
#define IDB_BARREIRA_TCIMA              113
#define IDB_BITMAP1                     114
#define IDB_VAZIO                       114
#define IDB_BARREIRA_TESQUERDO          116
#define IDB_BARREIRA_CRUZ               117
#define IDB_BITMAP2                     120
#define IDB_BARREIRA_TDIREITO           120
#define IDB_BARREIRA_TBAIXO             121
#define IDB_BITMAP3                     123
#define IDB_PACMAN_ABERTOESQ            123
#define IDB_PACMAN_BAIXO                124
#define IDB_PACMAN_CIMA                 125
#define IDB_PACMAN_ESQ                  126
#define IDB_PACMAN_DTO                  127
#define IDB_BADGUY_BAIXO                128
#define IDB_BADGUY_CIMA                 129
#define IDB_BADGUY_DTO                  130
#define IDB_BADGUY_ESQ                  131
#define IDI_PACMAN                      132
#define IDD_LIGARAJOGO                  133
#define IDD_LOGIN                       134
#define IDD_UTILIZADORES                135
#define IDD_TOP10                       136
#define IDB_BADGUY_BAIXO_AZUL           137
#define IDB_BADGUY_CIMA_AZUL            139
#define IDB_BADGUY_DTO_AZUL             140
#define IDB_BADGUY_ESQ_AZUL             141
#define IDC_STATUS_JOGNOME1             1007
#define IDC_JOGNOME2                    1008
#define IDC_STATUS_JOGNOME2             1008
#define IDC_JOGNOME3                    1009
#define IDC_STATUS_JOGNOME3             1009
#define IDC_STATUS_JOGPONTOS1           1010
#define IDC_STATUS_JOGPONTOS2           1011
#define IDC_STATUS_JOGPONTOS3           1012
#define IDC_STATUS_TEMPOJOGO            1013
#define IDC_STATUS_TOTAIS               1014
#define IDC_PONTOSFALTA                 1015
#define IDC_STATUS_PONTOSFALTA          1015
#define IDC_STATUS_VIDA1                1016
#define IDC_NJ_NOBJECTOS                1017
#define IDC_STATUS_VIDA3                1017
#define IDC_NJ_NBADGUYS                 1018
#define IDC_STATUS_VIDA2                1018
#define IDC_NJ_AUTO                     1019
#define IDC_NJ_COMECAR                  1020
#define IDC_NJ_CANCELAR                 1021
#define IDC_SERVIDOR                    1023
#define IDC_UTILIZADOR                  1024
#define IDC_LISTUSERS                   1024
#define IDC_USERNAME                    1025
#define IDC_NJ_NATAQUES                 1026
#define IDC_PASSWORD                    1026
#define IDC_ISADMIN                     1028
#define IDC_GRAVAR                      1029
#define IDC_TOP10                       1030
#define IDC_NJ_POSICIONAR               1032
#define IDC_APAGAR                      1033
#define IDC_CANCEL                      1034
#define IDC_AUTO                        1035
#define ID_JOGO_CRIARNOVOJOGO           40001
#define ID_JOGO_LIGARAUMJOGO            40002
#define ID_JOGO_SAIR                    40003
#define ID_AJUDA                        40006
#define ID_CONFIGURA40008               40008
#define ID_JOGO_INFORMA40009            40009
#define ID_JOGO_INFO                    40010
#define ID_JOGO_LOGIN                   40013
#define ID_INFORMA40018                 40018
#define ID_JOGO_TOP10                   40019
#define ID_JOGO_UTILIZADORES            40020
#define ID_JOGO_DESISTIRDOJOGO          40021
#define ID_TECLAS_DEFINIRTECLASESQUERDA 40022
#define ID_TECLAS_DEFINIRTECLAESQUERDA  40023
#define ID_TECLAS_DEFINIRTECLAESQUERDA40024 40024
#define ID_TECLAS_DEFINIRTECLAESQUERDA40025 40025
#define ID_TECLAS_DIREITA               40026
#define ID_TECLAS_ESQUERDA              40027
#define ID_TECLAS_CIMA                  40028
#define ID_TECLAS_BAIXO                 40029
#define ID_JOGO_JOGOLOCAL               40030
#define ID_JOGO_LOCAL                   40031
#define ID_ACCELERATOR40034             40034

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         40036
#define _APS_NEXT_CONTROL_VALUE         1037
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
