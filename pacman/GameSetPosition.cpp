#include "GameSetPosition.h"
#include "Tabuleiro.h"
#include "BadGuy.h"
#include "GoodGuy.h"
#include "..\comum\utils.h"
#include "PacmanApp.h"

GameSetPosition::GameSetPosition(PacmanApp *app):Game(app)
{	
	TabuleiroBase *t=new Tabuleiro(this);
	t->LoadInternal();
	//t->SetPontos(100);
	setTabuleiro(t);
	this->theguy=new GoodGuy(this,"position",1);
	addGoodGuy(this->theguy);
}


GameSetPosition::~GameSetPosition(void)
{
}


void GameSetPosition::defkeys( int keys[4] )
{
	theguy->setKeyDown(keys[(int)dk_down]);
	theguy->setKeyLeft(keys[(int)dk_left]);
	theguy->setKeyRight(keys[(int)dk_right]);
	theguy->setKeyUp(keys[(int)dk_up]);
}

void GameSetPosition::doMouseMove( WORD keys, int x, int y )
{
	int xx=getTabXFromScr(x);
	int yy=getTabXFromScr(y);
	if (getTabuleiro()->canGoXY(xx,yy)) {
		getApp()->getJogador()->setPosXY(xx,yy);


		HDC hdc = GetDC(getApp()->getHWnd()); 
		for (int i = 0; i < getNGuys(); i++) {
			getTabuleiro()->drawTabuleiro(hdc,getGuy(i)->getX(),getGuy(i)->getY());
		}
	    theguy->setXY(xx,yy);
		for (int i = 0; i < getNGuys(); i++) {
			getGuy(i)->draw(hdc);
		}

		ReleaseDC(getApp()->getHWnd(),hdc);
	
	} 
}
