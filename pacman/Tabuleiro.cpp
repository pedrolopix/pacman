#include "Tabuleiro.h"
#include "resource.h"
#include "Game.h"
#include <stdio.h>
#include <istream>
#include <fstream>
#include <string>
#include "..\comum\TabuleiroBase.h"
#include "..\comum\GraphicObject.h"
#include "..\comum\GameBase.h"
#include "..\comum\Application.h"
#include "..\comum\utils.h"


Tabuleiro::Tabuleiro(Game *game):TabuleiroBase(game)
{
	hbitBARREIRA_TOPESQUERDO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_TOPESQUERDO));
	hbitBARREIRA_TOPDIREITO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_TOPDIREITO));
	hbitBARREIRA_BOTTOMESQUERDO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_BOTTOMESQUERDO));
	hbitBARREIRA_BOTTOMDIREITO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_BOTTOMDIREITO));
	hbitPONTO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_PONTO));
	hbitBARREIRA_HORIZONTAL=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_HORIZONTAL));
	hbitBARREIRA_ALTO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_ALTO));
	hbitVAZIO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_VAZIO));
	hbitBARREIRA_TCIMA=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_TCIMA));
	hbitBARREIRA_TESQUERDO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_TESQUERDO));
	hbitBARREIRA_TDIREITO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_TDIREITO));	
	hbitBARREIRA_TBAIXO=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_TBAIXO));
	hbitBARREIRA_CRUZ=LoadBitmap( getGame()->getApp()->getHInstance(), MAKEINTRESOURCE(IDB_BARREIRA_CRUZ));
}

void Tabuleiro::draw( HDC &hdc,PAINTSTRUCT &PtStc )
{
	GraphicObject::draw(hdc,PtStc);
	//char str[80]="\0";
	//sprintf_s(str, 80, "Tabuleiro");
	//TextOut(hdc,200,0,str,(int)strlen(str));
	drawTabuleiro(hdc,PtStc);
}


void Tabuleiro::drawTabuleiro( HDC &hdc,PAINTSTRUCT &PtStc )
{
	int xi=0;
	int xf=TAB_MAX_X;
	int yi=0;
	int yf=TAB_MAX_Y;


	if (PtStc.rcPaint.left!= 0 || PtStc.rcPaint.top!=0) {
		xi=PtStc.rcPaint.left-2;
		xf=PtStc.rcPaint.right+2;
		yi=PtStc.rcPaint.top-2;
		yf=PtStc.rcPaint.bottom+2;
		if (xi<0) xi=0;
		if (xf>TAB_MAX_X) xf=TAB_MAX_X;
		if (yi<0) yi=0;
		if (yf>TAB_MAX_Y) yf=TAB_MAX_Y;
	}



	for(int x=xi;x<xf;x++) {
		for(int y=yi; y<yf;y++) {
			drawTabuleiro(hdc, x, y);
		}
	}
}

void Tabuleiro::drawTabuleiro(HDC & hdc, int x, int y)
{
	if (x<0 || y<0 || x>TAB_MAX_X || y >TAB_MAX_Y) return;

	HBITMAP hbit=hbitVAZIO;
	HDC auxmemdc;
	switch (getTab(x,y)) {
	case '+':
		//top esquerdo??
		if (isTopoEsquerdo(x,y)) { 
			hbit =hbitBARREIRA_TOPESQUERDO;
		} 
		if (isTopoDireito(x,y)) { 
			hbit=hbitBARREIRA_TOPDIREITO;
		}
		if (isBbottomEsquerdo(x,y)) { 
			hbit=hbitBARREIRA_BOTTOMESQUERDO;
		}
		if (isBottomDireito(x,y)) { 
			hbit=hbitBARREIRA_BOTTOMDIREITO;
		}
		if (isTCima(x,y)) { 
			hbit=hbitBARREIRA_TCIMA;
		}
		if (isTBaixo(x,y)) { 
			hbit=hbitBARREIRA_TBAIXO;
		}
		if (isTEsquerdo(x,y)) { 
			hbit=hbitBARREIRA_TESQUERDO;
		}			  
		if (isTDireito(x,y)) { 
			hbit=hbitBARREIRA_TDIREITO;
		}
		if (isCruz(x,y)) { 
			hbit=hbitBARREIRA_CRUZ;
		}	
		break;
	case '.':
		hbit=hbitPONTO;
		break;		  
	case '-':
		hbit=hbitBARREIRA_HORIZONTAL;
		break;
	case '|':
		hbit=hbitBARREIRA_ALTO;
		break;
	}
	if (hbit!=0) { //&& (!getGame()->isGuyIn(NULL,x,y,false)) || (getGame()->isGuyLastIn(x,y))
		auxmemdc=CreateCompatibleDC(hdc);
		SelectObject(auxmemdc, hbit);
		BitBlt(hdc, translateCoordX(x),translateCoordX(y), getWidth(), getHeight(), auxmemdc, 0,0, SRCCOPY);
		DeleteDC(auxmemdc);
	}
}


Tabuleiro::~Tabuleiro(void)
{
	DeleteObject(hbitBARREIRA_TOPESQUERDO);
	DeleteObject(hbitBARREIRA_TOPESQUERDO);
	DeleteObject(hbitBARREIRA_TOPDIREITO);
	DeleteObject(hbitBARREIRA_BOTTOMESQUERDO);
	DeleteObject(hbitBARREIRA_BOTTOMDIREITO);
	DeleteObject(hbitBARREIRA_TCIMA);
	DeleteObject(hbitPONTO);
	DeleteObject(hbitBARREIRA_HORIZONTAL);
	DeleteObject(hbitBARREIRA_ALTO);
	DeleteObject(hbitVAZIO);
	DeleteObject(hbitBARREIRA_TESQUERDO);
	DeleteObject(hbitBARREIRA_TDIREITO);
	DeleteObject(hbitBARREIRA_TBAIXO);
	DeleteObject(hbitBARREIRA_CRUZ);
}
