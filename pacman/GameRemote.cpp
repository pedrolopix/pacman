#include "GameRemote.h"
#include "PacmanApp.h"
#include "GoodGuy.h"
#include "BadGuy.h"
#include "..\comum\utils.h"

using namespace std;

GameRemote::GameRemote( PacmanApp *pacmanApp):Game(pacmanApp)
{
	TabuleiroBase *t=new Tabuleiro(this);
	t->LoadInternal();
	setTabuleiro(t);
	gamethread = new GameRemoteThread(this,pacmanApp->getConfig()->getHost());
	firstCall=true;
	pontosTotal=0;
	pontosMax=0;
	pontosFalta=0;
}

GameRemote::~GameRemote(void)
{
	if (gamethread!=NULL) {
		delete gamethread;
		gamethread=NULL;
	}
}

void GameRemote::doKeyDown(int key)
{
	getApp()->getRemote()->sendkeyDown(key);
}

void GameRemote::start()
{
	gamethread->start(); 

}

void GameRemote::processa( CbufGame & buf )
{

	switch (buf.cmd) {
	case gcmd_serverend:
		setInfo("O Servidor foi desligado");
		PostMessage(getApp()->getHWnd(),WM_USER+1,3,0);
		return;
	case gcmd_end:
		setInfo("jogo acabou!");
		PostMessage(getApp()->getHWnd(),WM_USER+1,1,0);
		break;
	case gcmd_esperaJogadores:
		time=0;
		setInfo("Espera que outros jogadores de liguem... jogo come�a em " + MillisecondsToStr(buf.time));
		break;
	default:
		time=buf.time;
		break;
	}
	updateGame(buf);
	getApp()->refreshInfo(this);
}


void GameRemote::updateGame( CbufGame & buf )
{
	int vivos=0;
	int pontos=0;
	string vencedor="";
	pontosTotal=buf.pontosTotal;
	pontosMax=buf.pontosMax;
	pontosFalta=buf.pontosFalta;

	getTabuleiro()->loadTabuleiro(buf.tabuleiro);

	for(int i=0; i<buf.nguys; i++) {
		if (i>=getNGuys()) {
			if (buf.guys[i].isGoodGuy) {
				GoodGuy *g=new GoodGuy(this,buf.guys[i].nome, buf.guys[i].vida,false, buf.guys[i].x, buf.guys[i].y);
				addGoodGuy(g);
			} else {
				BadGuy *b=new BadGuy(this, buf.guys[i].cor, buf.guys[i].x, buf.guys[i].y);
				addBadGuy(b);
			}
		}

		if (buf.guys[i].pontos>pontos) {
			pontos=buf.guys[i].pontos;
			vencedor = buf.guys[i].nome;
		}

		getGuy(i)->setXY(buf.guys[i].x, buf.guys[i].y);
		if (getGuy(i)->isGoodGuy()) {
			GoodGuyBase *g= (GoodGuyBase *)(getGuy(i));
			g->setPontos(buf.guys[i].pontos);
			g->setVida(buf.guys[i].vida);
		}
		if (buf.guys[i].morto) {
			if (buf.guys[i].nome==getApp()->getRemote()->getUserName() ) {
				if (vivos==0) {
					setInfo(vencedor+ "... ganhou!"); 
				}
				else {
					setInfo(getApp()->getRemote()->getUserName()+ "... morreu!!") ;
				}
			}
			getGuy(i)->morre();
		} else {
			vivos++;
			if (buf.cmd!=gcmd_esperaJogadores) {
				if (buf.guys[i].automatic) {
					setInfo(getApp()->getRemote()->getUserName()+ " em autom�tico...");
				} else {
					setInfo(getApp()->getRemote()->getUserName() );
				}
			}

		}
		getGuy(i)->setDireccao(buf.guys[i].direccao);
	}


	avanca();
}

void GameRemote::avanca()
{
	if (firstCall) {
		firstCall=false;
		getApp()->invalidate();
		return;
	}
	HDC hdc = GetDC(getApp()->getHWnd()); 
	for (int i = 0; i < getNGuys(); i++) {
		getTabuleiro()->drawTabuleiro(hdc,getGuy(i)->getLastX(),getGuy(i)->getlastY());
	}

	//GameBase::avanca(); n�o implementa....

	for (int i = 0; i < getNGuys(); i++) {
		getGuy(i)->draw(hdc);
	}
	drawInfo(hdc);
	ReleaseDC(getApp()->getHWnd(),hdc);
}

void GameRemote::desistir()
{
	getApp()->getRemote()->desisteJogo();
}

int GameRemote::getPontosTotal() const
{
	return pontosTotal;
}

int GameRemote::getPontosMax() const
{
	return pontosMax;
}

int GameRemote::getPontosFalta() const
{
	return pontosFalta;
}

int GameRemote::getGameTime() const
{
	return time;
}
