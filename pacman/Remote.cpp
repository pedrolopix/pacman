#include "Remote.h"
#include "PacmanApp.h"
#include "..\comum\utils.h"

using namespace std;

Remote::Remote(PacmanApp *app)
{
	this->app=app;
	this->hPipe=NULL;
	this->host= "";
	isLoggedin=false;
	this->username= "";
}


Remote::~Remote(void)
{
	loggoff();
}

bool Remote::writePipe(const Commands command,const LPVOID buf,const int len) {
	int ret,n;
	CommandBuf cmd;
	cmd.command=command;
	cmd.len=len;
	ret=WriteFile(hPipe,&cmd,sizeof(cmd),(LPDWORD)&n,NULL);
	if (!ret || !n) {
		app->showMessage("Erro a ligar ao servidor...");
		app->disconneted();
		return false;
	}
	if (len==0) return true;
	ret=WriteFile(hPipe,buf,len,(LPDWORD)&n,NULL);
	if (!ret || !n) {
		app->showMessage("Erro a ligar ao servidor...");
		app->disconneted();
		return false;
	}
	return true;
}

bool Remote::readPipe(LPVOID buf, int len) {
	int ret, n;
	ret = ReadFile(hPipe, buf, len,(LPDWORD) &n, NULL);
	if (!ret || !n) {
		app->disconneted();
		return false;
	}
	if (len!=n) {
		cmdError("buffer error", true);
		return false;
	}

	return true;
}
bool Remote::readCmd(CommandBuf &cmd) {
	
    return readPipe(&cmd,sizeof(cmd));
}

bool Remote::connect( const std::string &host)
{
	loggoff();
	string pipename="\\\\"+host+PIPE_NAME;
	if (!WaitNamedPipe(pipename.c_str(), TIMEOUT)) {
		app->showMessage("Erro a ligar ao servidor...");
		//ErrorExit("pipe");
		return false;
	}

	hPipe = CreateFile(pipename.c_str(), GENERIC_WRITE|GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hPipe==NULL) {
		app->showMessage("Erro a ligar ao servidor...");
		return false;
	}
	this->host=host;
	isLoggedin=false;
	return true;
}




void Remote::loggoff()
{
	if (hPipe!=NULL) {
		CloseHandle(hPipe);
		hPipe=NULL;
	}
	isLoggedin=false;
	this->username= "";
}

bool Remote::login(const std::string username,const std::string password,CBufUserProp &user )
{
	CBufLogin l;
	CommandBuf cmd;
	this->username= "";
	strcpy_s(l.username,MAX_CHAR,username.c_str());
	strcpy_s(l.password,MAX_CHAR,password.c_str());
	writePipe(cmd_c_Login,&l,sizeof(l));
	//espera resposta;
	if (!readCmd(cmd) ) {
		app->showMessage("Erro desconhecido, tente novamente");
		return false;
	}
	isLoggedin=false;
	switch (cmd.command) {
	case cmd_s_Loggedin:
		if (cmd.len==0 || cmd.len!=sizeof(user)) {
			app->showMessage("Erro desconhecido, tente novamente");
			break;
		}
		if (!readPipe(&user,cmd.len)) {
			app->showMessage("Erro desconhecido, tente novamente");
		    break;
		}
		isLoggedin=true;
		this->username=username;
		app->showMessage("Utilizador autenticado!");
		return true;
	case cmd_s_UserAlreadyLoggedin:
		app->showMessage("utlizador j� est� autenticado noutra sess�o!");
		break;
	case cmd_s_LoginError:
		app->showMessage("utlizador ou palavra passe inv�lida");
		break;
	default:
		cmdError("Erro desconhecido, tente novamente", true);
		break;
	}
	return false;
}

bool Remote::getUsers( CBufUserList&ul )
{
	CommandBuf cmd;
	if (!writePipe(cmd_c_UserList,NULL,0)) return false;
	if (!readCmd(cmd) ) {
		return false;
	}

	if (cmd.command!=cmd_s_UserList) return false;
	if (!readPipe(&ul,cmd.len)) return false;
	return ul.eof;
}

void Remote::addUser(const std::string username, const std::string password,const bool isAdmin )
{
	CBufUserAdd u;
	u.isAdmin= isAdmin;
	strcpy_s(u.user.username,MAX_CHAR,username.c_str());
	strcpy_s(u.user.password,MAX_CHAR,password.c_str());
	if (!writePipe(cmd_c_UserAdd,&u,sizeof(u))) return;
	CommandBuf cmd;
	if (!readCmd(cmd) ) {
		return;
	}
	switch (cmd.command) {
	case cmd_s_UserDuplicated:
		app->showMessage("O utilizador j� existe!");
		break;
	case cmd_s_UserUpdated:
		app->showMessage("O utilizador actualizado!");
		break;
	case cmd_s_UserAdded:
		app->showMessage("O utilizador adicionado!");
		break;
	default:
		cmdError("Erro desconhecido, tente novamente",true);
		break;
	}
}

bool Remote::userDelete( const std::string username )
{
	CBufUserName u;
	strcpy_s(u.username,MAX_CHAR,username.c_str());
	if (!writePipe(cmd_c_UserDelete,&u,sizeof(u))) return false;
	CommandBuf cmd;
	if (!readCmd(cmd) ) {
		return false;
	}
	switch (cmd.command) {
	case cmd_s_UserNotExist:
		app->showMessage("O utilizador n�o existe!");
		break;
	case cmd_s_UserDeleted:
		app->showMessage("O utilizador foi apagado!");
		return true;
	default:
		cmdError("Erro desconhecido, tente novamente", true);
		break;
	}
	return false;
}

bool Remote::getUser( const std::string username, CBufUser& user )
{
	CBufUserName u;
	strcpy_s(u.username,MAX_CHAR,username.c_str());
	if (!writePipe(cmd_c_UserGet,&u,sizeof(u))) return false;
	CommandBuf cmd;
	if (!readCmd(cmd) ) {
		return false;
	}
	switch (cmd.command) {
	case cmd_s_UserNotExist:
		app->showMessage("O utilizador n�o existe!");
		break;
	case cmd_s_GetUser:
		if (!readPipe(&user,cmd.len)) return false;
		return true;
	default:
		cmdError("Erro desconhecido, tente novamente", true);
		break;
	}
	return false;
}

void Remote::cmdError( std::string msg, bool flushpipe )
{
	if (flushpipe) FlushFileBuffers(hPipe);
	app->showMessageError(msg);
}

bool Remote::iniciarJogo(int nObjectos,int nBadGuys, int nAtaques, bool inicioAuto, int xini,int yini)
{
	CBufInicarJogo ij;
	ij.nObjectos=nObjectos;
	ij.nBadGuys=nBadGuys;
	ij.nAtaques=nAtaques;
	ij.inicioAuto=inicioAuto;
	ij.xini= xini;
	ij.yini= yini;
	
	if (!writePipe(cmd_c_NovoJogo,&ij,sizeof(ij))) return false;
	CommandBuf cmd;
	if (!readCmd(cmd) ) {
		return false;
	}
	switch (cmd.command) {
	case cmd_s_JaExisteJogo:
		app->showMessage("J� existe um jogo, n�o pode come�ar outro!");
		break;
	case cmd_s_novoJogoOk:
		//app->showMessage("Jogo inciado com sucesso!");
		return true;
	default:
		cmdError("Erro desconhecido, tente novamente", true);
		break;
	}
	return false;
}

bool Remote::ligarJogo(bool inicioAuto, int xini,int yini)
{
	CBufLigarJogo ij;
	ij.inicioAuto=inicioAuto;
	ij.xini= xini;
	ij.yini= yini;

	if (!writePipe(cmd_c_LigarJogo,&ij,sizeof(CBufLigarJogo))) return false;
	CommandBuf cmd;
	if (!readCmd(cmd) ) {
		return false;
	}
	switch (cmd.command) {
	case cmd_s_NaoExisteJogo:
		app->showMessage("N�o existe nenhum jogo, Tente come�ar um novo!");
		break;
	case cmd_s_JogoADecorrer:
		app->showMessage("Jogo j� est� a decorrer!");
		break;
	case cmd_s_MaxJogadores:
		app->showMessage("Atingido o n� max de jogadores por jogo!");
		break;
	case cmd_s_LigarJogoOk:

		//app->showMessage("ligado a jogo!");
		return true;		
	default:
		cmdError("Erro desconhecido, tente novamente", true);
		break;
	}
	return false;
}

void Remote::sendkeyDown( int key )
{
	CBufSendKeys k;
	k.key=key;
	if (!writePipe(cmd_c_Sendkeys,&k,sizeof(k))) return;
	

}

void Remote::desisteJogo()
{
	writePipe(cmd_c_DesisteJogo,NULL,0);
}

bool Remote::getIsLoggedin()
{
	return isLoggedin;
}

void Remote::defkeys( int left, int right, int up, int down )
{
	CBufSetKeys sk;
	sk.left=left;
	sk.right=right;
	sk.up=up;
	sk.down=down;

	if (!writePipe(cmd_c_UserSetKeys,&sk,sizeof(CBufSetKeys))) return;
	
}

std::string Remote::getUserName() const
{
	return username;
}

void Remote::getTop10( CbufTop10 &top10 )
{
	top10.count=0;
	if (!writePipe(cmd_c_top10,NULL,0)) return;
	CommandBuf cmd;
	if (!readCmd(cmd) ) {
		return;
	}
	switch (cmd.command) {
	case cmd_s_top10:
		if (!readPipe(&top10,cmd.len)) return;
		break;
	default:
		cmdError("Erro desconhecido, tente novamente", true);
		break;
	}
	return;	
}


