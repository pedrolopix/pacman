#ifndef GameThreadH
#define GameThreadH
#include "..\comum\basethread.h"
class PacmanApp;

class GameThread : public BaseThread
{
private:	
	PacmanApp *app;
	GameThread(const GameThread &a);
	GameThread & operator = (const GameThread &a);
protected:
    virtual void execute();
public:
	GameThread(PacmanApp *app);
	virtual ~GameThread(void);
	PacmanApp *getApp();
};

#endif