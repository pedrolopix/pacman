#ifndef DialogTop10H
#define DialogTop10H

#include "dialog.h"

class PacmanApp;

class DialogTop10 : public Dialog
{
private:
	DialogTop10(const DialogTop10 &a);
	DialogTop10 & operator = (const DialogTop10 &a);
protected:
	virtual BOOL dialogProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	virtual void doInit();
public:
	DialogTop10(PacmanApp *app);
	virtual ~DialogTop10();
};


#endif
