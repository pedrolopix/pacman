#ifndef GoodGuyH
#define GoodGuyH

#include "..\comum\GoodGuyBase.h"
#include <string>

class Game;

class GoodGuy :public GoodGuyBase
{
	GoodGuy(const GoodGuy &a);
	GoodGuy & operator = (const GoodGuy &a);
public:
	GoodGuy(Game *game,std::string nome, int vida,bool inicioAuto=false, int x=1, int y=1);
	virtual ~GoodGuy(void);
};

#endif