#ifndef GameRemoteThreadH
#define GameRemoteThreadH
#include <Windows.h>
#include <string>
#include "..\comum\basethread.h"
#include "..\comum\Comunication.h"

class GameRemote;

class GameRemoteThread :public BaseThread
{
private:
	GameRemote *game;
	std::string host;
	HANDLE hPipe;
protected:
	virtual void execute();
	void processa( CbufGame &buf );
	bool connectGame();
public:
	GameRemoteThread(GameRemote *game, std::string host);
	~GameRemoteThread();
	void ErroPipe();


};

#endif