#ifndef DialogUsersH
#define DialogUsersH

#include "dialog.h"

class PacmanApp;

class DialogUsers : public Dialog
{
private:
	DialogUsers(const DialogUsers &a);
	DialogUsers & operator = (const DialogUsers &a);
protected:
	virtual BOOL dialogProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	virtual void doInit();
public:
	DialogUsers(PacmanApp *app);
	virtual ~DialogUsers();
	void fillUsers();
	void doGravar();
	void doApagar();
	void doFillUser();
};


#endif
