#include <windows.h>
#include "PacmanApp.h"
#include "..\comum\utils.h"

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst,LPSTR lpCmdLine, int nCmdShow) {
	
	init_rand();

	PacmanApp app(hInst,hPrevInst,lpCmdLine,nCmdShow);
	if (app.initialization()) {
  	  return app.run(); 
    }
	return 0;
}
