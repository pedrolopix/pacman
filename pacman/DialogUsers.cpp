#include "DialogUsers.h"
#include "resource.h"
#include "PacmanApp.h"
#include <string>
#include <sstream>

using namespace std;

DialogUsers::DialogUsers(PacmanApp *app) : Dialog(app, IDD_UTILIZADORES) {
}



DialogUsers::~DialogUsers()
{
}

BOOL DialogUsers::dialogProc( HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam )
{
	switch (messg) {
	case WM_COMMAND:
		switch(LOWORD(wParam)) 
		{ 
		case IDC_LISTUSERS:
			if (HIWORD(wParam)==LBN_DBLCLK)	//Clique Duplo
				doFillUser();
			return 0;
		case IDC_GRAVAR:
			doGravar();	
			return 0;
		case IDC_APAGAR:
			doApagar();	
			return 0;
		case IDC_CANCEL:
			setModalResult(false);
			close();
			return 0;
		}   

	}
	return Dialog::dialogProc(hWnd,messg,wParam,lParam);
}



void DialogUsers::doInit()
{
	fillUsers();
}

void DialogUsers::fillUsers()
{
	CBufUserList ul;
	SendDlgItemMessage(getHWnd(), IDC_LISTUSERS,LB_RESETCONTENT,0,0);
	if (!getApp()->getRemote()->getUsers(ul)) return;
	do {
		for(int i=0; i<ul.nUsers; i++) {
		   SendDlgItemMessage(getHWnd(), IDC_LISTUSERS, LB_ADDSTRING, 0, (LPARAM)(ul.users[i]));
		}
		if (!ul.eof) {
		   if (!getApp()->getRemote()->getUsers(ul) )return;
		}
	} while (!ul.eof);
}

void DialogUsers::doGravar()
{
	stringstream str;
	string username=getEditStrValue(IDC_USERNAME);
	string password=getEditStrValue(IDC_PASSWORD);
	bool isAdmin= (SendDlgItemMessage(getHWnd(), IDC_ISADMIN, BM_GETCHECK,0, 0)!=0);
	if (username.length()==0) {
		str << "O username � obrigat�rio!";
		getApp()->showMessageError(str.str());
		return ;
	}
	if (password.length()==0) {
		str << "N password � obrigat�ria!";
		getApp()->showMessageError(str.str());
		return ;
	}

	getApp()->getRemote()->addUser(username,password,isAdmin);
	setEditValue(IDC_USERNAME,"");
	setEditValue(IDC_PASSWORD,"");
	fillUsers();
}

void DialogUsers::doApagar()
{
	string username=getEditStrValue(IDC_USERNAME);
	if (getApp()->getRemote()->userDelete(username)) {
		fillUsers();
	}
}

void DialogUsers::doFillUser()
{
	char str[MAX_CHAR];
	CBufUser user;
	int i = (int) SendDlgItemMessage(getHWnd(), IDC_LISTUSERS, LB_GETCURSEL, 0, 0);
	SendDlgItemMessage(getHWnd(), IDC_LISTUSERS, LB_GETTEXT, i, (LPARAM) str);
	if (getApp()->getRemote()->getUser(str, user)) {
		setEditValue(IDC_USERNAME,user.user.username);
		setEditValue(IDC_PASSWORD,user.user.password);		
		SendDlgItemMessage(getHWnd(), IDC_ISADMIN, BM_SETCHECK, user.Prop.isAdmin, 0);
		return;
	}

}
