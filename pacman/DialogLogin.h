#ifndef DialogLoginH
#define DialogLoginH

#include "dialog.h"

class PacmanApp;

class DialogLogin : public Dialog
{
private:
	DialogLogin(const DialogLogin &a);
	DialogLogin & operator = (const DialogLogin &a);
protected:
	virtual BOOL dialogProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	virtual void doInit();
	void okClick();
public:
	DialogLogin(PacmanApp *app);
	virtual ~DialogLogin(void);

};


#endif
