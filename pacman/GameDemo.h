#ifndef GameDemoH
#define GameDemoH

#include "Game.h"
#include "PacmanApp.h"
#include "GameLocal.h"


class GameDemo:public GameLocal
{
private:
	int contador;
	GameDemo(const GameDemo &a);
	GameDemo & operator = (const GameDemo &a);
public:
	GameDemo(PacmanApp *PacmanApp);
	virtual void draw(HDC &hdc,PAINTSTRUCT &PtStc );
	virtual void terminou();
	virtual ~GameDemo(void);
};

#endif