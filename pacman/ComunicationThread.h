#pragma once
#include <Windows.h>
#include "..\comum\BaseThread.h"
#include "..\comum\Comunication.h"

class Remote;

class ComunicationThread:public BaseThread
{
private:
	HANDLE hPipe;
	Remote *remote;
	void interpretaCmd( const CommandBuf &cmd );
	void log(const std::string &str);
protected:
	virtual void execute();
public:
	ComunicationThread(Remote *remote, HANDLE pipe);
	~ComunicationThread();


};

