#include "PacmanApp.h"
#include <sstream>
#include "resource.h"
#include "GameDemo.h"
#include "DialogNovoJogo.h"
#include "GameSetPosition.h"
#include "DialogLogin.h"
#include "..\comum\Utils.h"
#include "GameLocal.h"
#include "DialogTop10.h"
#include "DialogUsers.h"
#include "GameRemote.h"

using namespace std;


PacmanApp::PacmanApp(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow) {
	this->hInst = hInst;
	this->hPrevInst = hPrevInst;
	this->lpCmdLine = lpCmdLine;
	this->nCmdShow = nCmdShow;
	progName = "pacman";
	progCaption = "Pacman by Pedro Lopes";
	dlginfo = NULL;
	game = NULL;
	dlgNovoJogo = NULL;
	dlgLigarJogo = NULL;
	defkeys = dk_none;
	definePosition = pbnone;
	config = new Config();
	jogador = new Jogador(this);
	remote= new Remote(this);
}

PacmanApp::~PacmanApp(void) {
	if (dlgNovoJogo!=NULL) delete dlgNovoJogo;
	if (dlgLigarJogo!=NULL) delete dlgLigarJogo;
	if (dlginfo != NULL) delete dlginfo;
	if (game != NULL) delete game;
	config;
	delete jogador;
	delete remote;
}

int PacmanApp::run() {
	MSG lpMsg;
	while (GetMessage(&lpMsg, NULL, 0, 0)) {
		if (!TranslateAccelerator(hWnd, hAccel, &lpMsg)) {
			TranslateMessage(&lpMsg);
			DispatchMessage(&lpMsg);
		}
	}
	return ((int) lpMsg.wParam);
}

bool PacmanApp::createPacmanApp() {

	wcApp.cbSize = sizeof (WNDCLASSEX);
	wcApp.hInstance = hInst;
	wcApp.lpszClassName = progName.c_str();
	wcApp.lpfnWndProc = StaticWindowProc;
	wcApp.style = NULL; //CS_HREDRAW | CS_VREDRAW;
	wcApp.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(IDI_PACMAN));
	wcApp.hIconSm = NULL;
	wcApp.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcApp.lpszMenuName = (LPCTSTR) IDR_MENU1;
	wcApp.cbClsExtra = 0;
	wcApp.cbWndExtra = 0;
	//wcApp.hbrBackground =  (HBRUSH) WHITE_BRUSH;
	wcApp.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	if (!RegisterClassEx(&wcApp)) {
		ErrorExit("createPacmanApp");
	}





	hAccel = LoadAccelerators(hInst, (LPCSTR) IDR_ACCELERATOR1);

	return (true);
}

bool PacmanApp::createWindow() {
	hWnd = CreateWindow(
		progName.c_str(), // Nome da janela (programa) definido acima
		progCaption.c_str(), // Texto que figura na barra da janela
		WS_OVERLAPPEDWINDOW, // Estilo da janela (WS_OVERLAPPED= normal)
		// Outros valores: WS_HSCROLL, WS_VSCROLL
		// (Fazer o OR "|" do que se pretender)
		CW_USEDEFAULT, // Posi��o x pixels (default=� direita da �ltima)
		CW_USEDEFAULT, // Posi��o y pixels (default=abaixo da �ltima)
		TAB_MAX_X * 24 + 60, // Largura da janela (em pixels)
		TAB_MAX_Y * 24 + 60, // Altura da janela (em pixels)
		(HWND) HWND_DESKTOP, // handle da janela pai (se se criar uma a partir 
		// de outra) ou HWND_DESKTOP se a janela for
		// a primeira, criada a partir do "desktop"
		(HMENU) NULL, // handle do menu da janela (se tiver menu)
		hInst, // handle da inst�ncia do programa actual
		// ("hInst" � declarado num dos par�metros
		// de WinMain(), valor atribu�do pelo Windows)
		0); // N�o h� par�metros adicionais para a janela

	if (!hWnd) ErrorExit("CreateWindow");

	//guardar o endere�o deste objecto
	SetWindowLong(hWnd, GWL_USERDATA, (long) this);

	// ============================================================================
	// 4. Mostrar a janela
	// ============================================================================
	ShowWindow(hWnd, nCmdShow); // "hWnd"= handler da janela, devolvido 
	// por "CreateWindow"; "nCmdShow"= modo de
	// exibi��o (p.e. normal, modal); � passado
	// como par�metro de WinMain()
	UpdateWindow(hWnd); // Refrescar a janela (Windows envia � janela
	// uma mensagem para pintar, mostrar dados,
	// (refrescar), etc)

	startDemo();
	invalidate();

	return true;
}

bool PacmanApp::initialization() {
	if (createPacmanApp()) {
		return createWindow();
	} else {
		return false;
	}

}

LRESULT PacmanApp::WindowProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam) {

	switch (messg) {
	case WM_COMMAND:
		switch (LOWORD(wParam)) {
		case ID_JOGO_LOCAL:
			mnuNewGameLocal();
			break;		
		case ID_JOGO_LOGIN:
			mnuLogin();
			break;
		case ID_JOGO_CRIARNOVOJOGO:
			mnuNewGame();
			break;
		case ID_JOGO_LIGARAUMJOGO:
			mnuJoinGame();
			break;
		case ID_JOGO_SAIR:
			mnuCloseGame();
			break;
		case ID_JOGO_INFO:
			mnuOpenJogoInfo();
			break;
		case ID_JOGO_DESISTIRDOJOGO:
			mnuDesistirJogo();
			break;
		case ID_JOGO_UTILIZADORES:
			mnuUtilzadores();
			break;
		case ID_JOGO_TOP10:
			mnuTop10();
			break;
		case ID_TECLAS_BAIXO:
			MessageBox(hWnd,"Precione uma tecla a seguir a esta mensagem para definir a tecla para a BAIXO!",progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
			defkeys=dk_down;
			break;
		case ID_TECLAS_CIMA:
			MessageBox(hWnd,"Precione uma tecla a seguir a esta mensagem para definir a tecla para a CIMA!",progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
			defkeys=dk_up;			
			break;
		case ID_TECLAS_ESQUERDA:
			MessageBox(hWnd,"Precione uma tecla a seguir a esta mensagem para definir a tecla para a ESQUERDA!",progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
			defkeys=dk_left;			
			break;
		case ID_TECLAS_DIREITA:
			MessageBox(hWnd,"Precione uma tecla a seguir a esta mensagem para definir a tecla para a Direita!",progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
			defkeys=dk_right;			
			break;
		case ID_AJUDA:
			mnuHelp();
			break;
		}
		break;
	case WM_SIZING:
	case WM_MOVE:						
		InvalidateRect(hWnd, NULL, 0);	
		break;
	case WM_KEYDOWN:
		doKeyDown(wParam, lParam);
		break;
	case WM_PAINT:
		doPaint();
		break;
	case WM_KILLFOCUS:
		break;
	case WM_MOUSEMOVE:
		doMouseMove(wParam, LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_LBUTTONDOWN:
		if (definePosition==pblocal) {
			definePosition=pbnone;
			startDemo();
			MessageBox(hWnd,"Posicao inicial definida!",progCaption.c_str(),MB_OK|MB_ICONINFORMATION);
			mnuNewGameLocal();	
		}	
		if (definePosition==pbNovo) {
			definePosition=pbnone;
			startDemo();
			MessageBox(hWnd,"Posicao inicial definida!",progCaption.c_str(),MB_OK|MB_ICONINFORMATION);
			mnuNewGame();	
		}
		if (definePosition==pbLigar) {
			definePosition=pbnone;
			startDemo();
			MessageBox(hWnd,"Posicao inicial definida!",progCaption.c_str(),MB_OK|MB_ICONINFORMATION);
			mnuJoinGame();	
		}
		break;
	case WM_CLOSE:
		mnuCloseGame();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_USER+1:
		// terminou o jogo
		terminouJogo(wParam);
		break;
	default:
		return (DefWindowProc(hWnd, messg, wParam, lParam));
		break;
	}
	return 0;
}

void PacmanApp::doPaint() {
	HDC hdc; // Declara��o de um handler para um Device Context
	PAINTSTRUCT PtStc; // Estrutura com dados para o refrescamento da janela
	hdc = BeginPaint(hWnd, &PtStc);
	if (game) {
		game->draw(hdc, PtStc);
	}
	EndPaint(hWnd, &PtStc);
}

LRESULT CALLBACK PacmanApp::StaticWindowProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam) {
	PacmanApp *app = (PacmanApp *) GetWindowLong(hWnd, GWL_USERDATA);
	return app->WindowProc(hWnd, messg, wParam, lParam);
}

void PacmanApp::mnuJoinGame() {
	definePosition=pbnone;
	if (dlgLigarJogo==NULL) dlgLigarJogo=new DialogLigarJogo(this);
	if (dlgLigarJogo->showModal()) {
	    doLigarJogo(dlgLigarJogo->getAutoInical());
	}
	
}

void PacmanApp::mnuNewGame() {
	definePosition=pbnone;
	if (dlgNovoJogo==NULL) dlgNovoJogo=new DialogNovoJogo(this);
	dlgNovoJogo->setPosicionaBoneco(pbNovo);
	if (dlgNovoJogo->showModal()) {
		doNovoJogo(jogador->getPrefNObjectos(),jogador->getPrefNBadGuys(),jogador->getPrefNAtaques(),dlgNovoJogo->getAutoInical());
		
	}
}

void PacmanApp::mnuNewGameLocal() {
	definePosition=pbnone;
	if (dlgNovoJogo==NULL) dlgNovoJogo=new DialogNovoJogo(this);
	dlgNovoJogo->setPosicionaBoneco(pblocal);
	if (dlgNovoJogo->showModal()) {
		doNovoJogoLocal(jogador->getPrefNObjectos(),jogador->getPrefNBadGuys(),jogador->getPrefNAtaques(),dlgNovoJogo->getAutoInical());
	}
}


void PacmanApp::mnuCloseGame() {
	int resposta;
	resposta = MessageBox(hWnd, "Terminar o jogo?", progName.c_str(), MB_YESNO | MB_ICONQUESTION);
	if (resposta == IDYES)
		PostQuitMessage(0); // Se YES, terminar jogo
}

void PacmanApp::mnuHelp() {
	MessageBox(hWnd, "Trabalho efectuado para o Sistemas Operativos II\n ISEC 2012\nPedro Lopes 212200565", progName.c_str(), MB_OK);
}

HINSTANCE PacmanApp::getHInstance() {
	return hInst;
}

void PacmanApp::doKeyDown(WPARAM wParam, LPARAM lParam) {
	stringstream ss;
	switch (defkeys) {
	case dk_down:
		ss << "Tecla para a baixo definida!";
		if (remote->getIsLoggedin()) remote->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
		jogador->setKeyDown(wParam);
		game->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
		defkeys=dk_none;
		MessageBox(hWnd,ss.str().c_str(), progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
		break;  
	case dk_up:
		ss << "Tecla para a cima definida!";
		jogador->setKeyUp(wParam);
		if (remote->getIsLoggedin()) remote->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
		game->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
		defkeys=dk_none;
		MessageBox(hWnd,ss.str().c_str(), progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
		break;  
	case dk_right:
		ss << "Tecla para a direita definida!";
		jogador->setKeyRight(wParam);
		if (remote->getIsLoggedin()) remote->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
		game->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
		defkeys=dk_none;
		MessageBox(hWnd,ss.str().c_str(), progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
		break;
	case dk_left:
		ss << "Tecla para a esquerda definida!";
		jogador->setKeyLeft(wParam);
		if (remote->getIsLoggedin()) remote->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
		game->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
		defkeys=dk_none;
		MessageBox(hWnd,ss.str().c_str(), progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
		break;
	default:
		game->doKeyDown(wParam);
		break;
	}
}

HWND PacmanApp::getHWnd() {
	return hWnd;
}

std::string PacmanApp::getProgCaption() {
	return progCaption;
}

void PacmanApp::doMouseMove(WORD keys, int x, int y) {

	//HDC hdc;
	//hdc = GetDC(hWnd); 

	//char str[80]="\0";
	//int xx = game->getTabXFromScr(x);
	//int yy = game->getTabYFromScr(y);

	//sprintf_s(str, 80, "mouse (%d,%d)->(%d,%d)       ",x,y,xx,yy);
	//TextOut(hdc,200,0,str,(int)strlen(str));

	//ReleaseDC(hWnd,hdc);

	if (definePosition!=pbnone && game!=NULL) {

		game->doMouseMove(keys, x, y);
	}

}

void PacmanApp::mnuOpenJogoInfo() {
	if (dlginfo == NULL) {
		dlginfo = new DialogInfoJogo(this);
	}
	dlginfo->show();

}


GameBase * PacmanApp::getGame() {
	return game;
}

void PacmanApp::refreshInfo(GameBase *game) {
	if (dlginfo != NULL) {
		dlginfo->refresh(game);
	}
}

void PacmanApp::invalidate()
{
	InvalidateRect(hWnd, NULL, 1);	
}

void PacmanApp::mnuLogin()
{
	definePosition=pbnone;
	DialogLogin *dlg=new DialogLogin(this);
	if (dlg->showModal()) {
		doUserLoggedin();
	}
	delete dlg;
}

Config *PacmanApp::getConfig() {
	return config;
}

void PacmanApp::doUserLoggedin()
{
	HMENU menu=GetMenu(hWnd);
	EnableMenuItem(menu,ID_JOGO_CRIARNOVOJOGO,MF_ENABLED);
	EnableMenuItem(menu,ID_JOGO_LIGARAUMJOGO,MF_ENABLED);
	EnableMenuItem(menu,ID_JOGO_TOP10,MF_ENABLED);
	EnableMenuItem(menu,ID_TECLAS_BAIXO,MF_ENABLED);
	EnableMenuItem(menu,ID_TECLAS_CIMA,MF_ENABLED);
	EnableMenuItem(menu,ID_TECLAS_ESQUERDA,MF_ENABLED);
	EnableMenuItem(menu,ID_TECLAS_DIREITA,MF_ENABLED);

	if (jogador->getIsAdmin()) {
		EnableMenuItem(menu,ID_JOGO_UTILIZADORES,MF_ENABLED);
	}
	startDemo();
}

void PacmanApp::doNovoJogoLocal(int nObjectos,int nBadGuys, int nAtaques,bool inicioAuto)
{
	delete game;
	game = new GameLocal(this, nObjectos, nBadGuys, nAtaques, inicioAuto);
	game->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
	((GameLocal *)game)->getTheGuy()->setXY(jogador->getPosXini(),jogador->getPosYini());
	HMENU menu=GetMenu(hWnd);	
	EnableMenuItem(menu,ID_JOGO_INFO,MF_ENABLED);
	EnableMenuItem(menu,ID_JOGO_DESISTIRDOJOGO,MF_ENABLED);
	game->start();
	invalidate();
}

void PacmanApp::doLigarJogo( bool AutoInicial )
{
	if (!remote->ligarJogo(AutoInicial, jogador->getPosXini(),jogador->getPosYini())) return;


	HMENU menu=GetMenu(hWnd);
	EnableMenuItem(menu,ID_JOGO_CRIARNOVOJOGO,MF_DISABLED);
	EnableMenuItem(menu,ID_JOGO_LIGARAUMJOGO,MF_DISABLED);
	EnableMenuItem(menu,ID_JOGO_INFO,MF_ENABLED);
	EnableMenuItem(menu,ID_JOGO_DESISTIRDOJOGO,MF_ENABLED);
	delete game;
	game = new GameRemote(this);
	game->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
	game->start();
}


void PacmanApp::doNovoJogo(int nObjectos,int nBadGuys, int nAtaques,bool inicioAuto)
{
	if (!remote->iniciarJogo(nObjectos,nBadGuys,nAtaques, inicioAuto, jogador->getPosXini(),jogador->getPosYini())) return;
	

	HMENU menu=GetMenu(hWnd);
	EnableMenuItem(menu,ID_JOGO_CRIARNOVOJOGO,MF_DISABLED);
	EnableMenuItem(menu,ID_JOGO_LIGARAUMJOGO,MF_DISABLED);
	EnableMenuItem(menu,ID_JOGO_INFO,MF_ENABLED);
	EnableMenuItem(menu,ID_JOGO_DESISTIRDOJOGO,MF_ENABLED);
	delete game;
	game = new GameRemote(this);
	game->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
	game->start();
}

void PacmanApp::startDemo()
{

	if (game!=NULL) {
	   if (typeid(*game)==typeid(GameDemo)) return;	
		
	}
	delete game;
	game = new GameDemo(this);
	game->start();
	invalidate();
}


void PacmanApp::mnuDesistirJogo()
{
	int resposta;
	resposta = MessageBox(hWnd, "Desistir do jogo?", progName.c_str(), MB_YESNO | MB_ICONQUESTION);
	if (resposta == IDYES) {
		game->desistir();
		HMENU menu=GetMenu(hWnd);
		EnableMenuItem(menu,ID_JOGO_CRIARNOVOJOGO,MF_ENABLED);
		EnableMenuItem(menu,ID_JOGO_LIGARAUMJOGO,MF_ENABLED);
		//startDemo();
	}
}

void PacmanApp::mnuUtilzadores()
{
	DialogUsers *dlg= new DialogUsers(this);
	dlg->showModal();
	delete dlg;
}

void PacmanApp::mnuTop10()
{
	DialogTop10 *dlg= new DialogTop10(this);
	dlg->showModal();
	delete dlg;
}

void PacmanApp::terminouJogo(int op)
{
	switch (op){
	case 0:
		//acabou o jogo demo
		break;
	case 1:
		MessageBox(hWnd,"Terminou o jogo!",progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
		break;
	case 2:
		MessageBox(hWnd,"Desistiu do jogo!",progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
		break;
	case 3:
		MessageBox(hWnd,"O Servidor foi desligado",progCaption.c_str(),MB_OK | MB_ICONERROR);
	}
	if (dlginfo != NULL) {
		delete dlginfo;
		dlginfo=NULL;
	}
	HMENU menu=GetMenu(hWnd);
	EnableMenuItem(menu,ID_JOGO_INFO,MF_DISABLED);
	EnableMenuItem(menu,ID_JOGO_DESISTIRDOJOGO,MF_DISABLED);
	startDemo();
}



void PacmanApp::startDefinePosition(PosicionaBoneco pb)
{
	delete game;
	game = new GameSetPosition(this);
	game->defkeys(jogador->getKeyLeft(),jogador->getKeyRight(),jogador->getKeyUp(),jogador->getKeyDown());
	invalidate();
	definePosition = pb;
}

Jogador * PacmanApp::getJogador()
{
	return jogador;
}

void PacmanApp::showMessage( std::string msg )
{
	MessageBox(hWnd,msg.c_str(),progCaption.c_str(),MB_OK | MB_ICONINFORMATION);
}

void PacmanApp::showMessageError( std::string msg )
{
	MessageBox(hWnd,msg.c_str(),progCaption.c_str(),MB_OK | MB_ICONERROR );
}

Remote *PacmanApp::getRemote()
{
	return remote;
}


void PacmanApp::disconneted()
{
	HMENU menu=GetMenu(hWnd);
	EnableMenuItem(menu,ID_JOGO_CRIARNOVOJOGO,MF_DISABLED);
	EnableMenuItem(menu,ID_JOGO_LIGARAUMJOGO,MF_DISABLED);
	EnableMenuItem(menu,ID_JOGO_TOP10,MF_DISABLED);
	EnableMenuItem(menu,ID_TECLAS_BAIXO,MF_DISABLED);
	EnableMenuItem(menu,ID_TECLAS_CIMA,MF_DISABLED);
	EnableMenuItem(menu,ID_TECLAS_ESQUERDA,MF_DISABLED);
	EnableMenuItem(menu,ID_TECLAS_DIREITA,MF_DISABLED);
	EnableMenuItem(menu,ID_JOGO_UTILIZADORES,MF_DISABLED);
}

