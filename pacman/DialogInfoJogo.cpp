#include "DialogInfoJogo.h"
#include "Resource.h"
#include "..\comum\Guy.h"
#include "Dialog.h"
#include "PacmanApp.h"
#include "..\comum\GoodGuyBase.h"
#include "..\comum\utils.h"
#include "..\comum\GameBase.h"
#include "GoodGuy.h"


using namespace std;

DialogInfoJogo::DialogInfoJogo(PacmanApp *app) : Dialog(app, IDD_STATUS) {

}

DialogInfoJogo::~DialogInfoJogo(void) {
}


BOOL DialogInfoJogo::dialogProc( HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam )
{
	//switch (messg) {
	//case WM_COMMAND:
	//	switch(LOWORD(wParam)) 
	//	{ 
	//	case IDC_DESISTIR:
	//		getApp()->mnuDesistirJogo();
	//		return 0;
	//	}   

	//}
	return Dialog::dialogProc(hWnd, messg, wParam, lParam);
}
void DialogInfoJogo::refresh(GameBase *game) {
  refreshPontos(game);
  refreshJogadores(game);
  refreshTimer(game);
  return;
  
}

void DialogInfoJogo::refreshJogadores(GameBase *game) {
  int n = 0;


  if (getHWnd() == 0) return;
  Guy *guy;
  GoodGuyBase *goodGuy;

  for (int i = 1; i < 4; i++) {
	  setInfo(i, "",  0, 0);
  }

  for (int i = 0; i < game->getNGuys(); i++) {
    
	guy = game->getGuy(i);
    if (!guy->isGoodGuy()) continue;
    goodGuy = (GoodGuy *) guy;


    if (goodGuy != NULL) {
      n++;
	  setInfo(n, goodGuy->getNome(), goodGuy->getPontos(), goodGuy->getVida());

    }

  }


}

void DialogInfoJogo::refreshTimer(GameBase *game) {
//  SYSTEMTIME st;
  string str = MillisecondsToStr(game->getGameTime());
  SetDlgItemText(getHWnd(), IDC_STATUS_TEMPOJOGO, str.c_str());
}

void DialogInfoJogo::refreshPontos(GameBase *game) {
  char str[80] = "\0";
  sprintf_s(str, 80, "%d", game->getPontosMax());
  SetDlgItemText(getHWnd(), IDC_STATUS_TOTAIS, str);
  sprintf_s(str, 80, "%d", game->getPontosFalta());
  SetDlgItemText(getHWnd(), IDC_STATUS_PONTOSFALTA, str);
}

void DialogInfoJogo::setInfo( int n, string nome,  int pontos, int vida )
{
	  char str[80] = "\0";
	switch (n) {
	case 1:
		SetDlgItemText(getHWnd(), IDC_STATUS_JOGNOME1,nome.c_str());
		sprintf_s(str, 80, "%d", pontos);
		SetDlgItemText(getHWnd(), IDC_STATUS_JOGPONTOS1, str);
		sprintf_s(str, 80, "%d", vida);
		SetDlgItemText(getHWnd(), IDC_STATUS_VIDA1, str);
		break;
	case 2:
		SetDlgItemText(getHWnd(), IDC_STATUS_JOGNOME2, nome.c_str());
		sprintf_s(str, 80, "%d", pontos);
		SetDlgItemText(getHWnd(), IDC_STATUS_JOGPONTOS2, str);
		sprintf_s(str, 80, "%d", vida);
		SetDlgItemText(getHWnd(), IDC_STATUS_VIDA2, str);
		break;
	case 3:
		SetDlgItemText(getHWnd(), IDC_STATUS_JOGNOME3, nome.c_str());
		sprintf_s(str, 80, "%d", pontos);
		SetDlgItemText(getHWnd(), IDC_STATUS_JOGPONTOS3, str);
		sprintf_s(str, 80, "%d", vida);
		SetDlgItemText(getHWnd(), IDC_STATUS_VIDA3, str);
		break;
	}
}
