#ifndef DialogInfoJogoH
#define DialogInfoJogoH

#include "dialog.h"
#include "..\comum\GameBase.h"

class DialogInfoJogo : public Dialog
{
private:
	DialogInfoJogo(const DialogInfoJogo &a);
	DialogInfoJogo & operator = (const DialogInfoJogo &a);
	void setInfo( int n, std::string nome, int pontos, int vida );
public:
	DialogInfoJogo(PacmanApp *app);
	void refresh(GameBase *game);
	void refreshJogadores(GameBase *game);
	void refreshPontos(GameBase *game);
	void refreshTimer(GameBase *game);
	~DialogInfoJogo(void);
	BOOL dialogProc( HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam );
};

#endif