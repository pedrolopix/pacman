#include "DialogLigarJogo.h"
#include "PacmanApp.h"
#include "resource.h"

DialogLigarJogo::DialogLigarJogo(PacmanApp *app) : Dialog(app, IDD_LIGARAJOGO) 
{
	inicioAuto=false;
}


DialogLigarJogo::~DialogLigarJogo(void)
{
}


BOOL DialogLigarJogo::dialogProc( HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam )
{
	switch (messg) {
	case WM_COMMAND:
		switch(LOWORD(wParam)) 
		{ 
		case IDC_NJ_POSICIONAR:

			setModalResult(false);
			close();
			getApp()->startDefinePosition(pbLigar); 

			return 0;
		case IDOK:
			inicioAuto= (SendDlgItemMessage(getHWnd(), IDC_NJ_AUTO, BM_GETCHECK,0, 0)!=0);
			setModalResult(true);
			close();

			return 0;
		case IDCANCEL:
			setModalResult(false);
			close();
			return 0;
		}   

	}
	return Dialog::dialogProc(hWnd, messg, wParam, lParam);;
}


bool DialogLigarJogo::getAutoInical()const
{
	return inicioAuto;
}

