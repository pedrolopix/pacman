#ifndef GameH
#define GameH
#include "..\comum\GameBase.h"
#include "Tabuleiro.h"

class PacmanApp;

class Game:public GameBase
{
public:
	Game(PacmanApp *app);
	PacmanApp *getApp();
	Tabuleiro *getTabuleiro();
	virtual void desistir();
	virtual void avanca();
	virtual ~Game();
};


#endif
