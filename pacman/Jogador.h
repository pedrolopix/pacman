#ifndef JogadorH
#define JogadorH
#include <string>

class PacmanApp;

class Jogador 
{
private:
	std::string nome;
	int posXini;
	int posYini;
	int keyLeft;
	int keyRight;
	int keyUp;
	int keyDown;
	int totalPontos;
	int totalJogos;
	int prefNObjectos;
	int prefNBadGuys;
	int prefNAtaques;
	bool isAdmin;
	Jogador(const Jogador &a);
	Jogador & operator = (const Jogador &a);
public:
	Jogador(PacmanApp *PacmanApp);
	void setNome(std::string nome);
	int getPosXini() const;
	void setPosXini(int val);
	int getPosYini() const;
	void setPosYini(int val);
	int getKeyLeft() const;
	void setKeyLeft(int val);
	int getKeyRight() const;
	void setKeyRight(int val);
	int getKeyUp() const;
	void setKeyUp(int val);
	int getKeyDown() const;
	void setKeyDown(int val);
	int getTotalPontos() const;
	void setTotalPontos(int val);
	int getTotalJogos() const;
	void setTotalJogos(int val);
	int getPrefNObjectos() const;
	void setPrefNObjectos(int val);
	int getPrefNBadGuys() const;
	void setPrefNBadGuys(int val);
	int getPrefNAtaques() const;
	void setPrefNAtaques(int val);
	virtual ~Jogador();
	void setPosXY( int xx, int yy );
	bool getIsAdmin() const;
	void setIsAdmin( bool isAdmin );
};


#endif

