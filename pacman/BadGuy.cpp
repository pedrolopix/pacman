#include "BadGuy.h"
#include "Game.h"
#include "resource.h"
#include "..\comum\BadGuyBase.h"


BadGuy::BadGuy( Game *game, GuyCor cor, int x/*=1*/, int y/*=1*/ ):BadGuyBase(game,cor,x,y)
{
	switch (cor) {
	case vermelho:
		loadBitmap(d_baixo, IDB_BADGUY_BAIXO);
		loadBitmap(d_cima, IDB_BADGUY_CIMA);
		loadBitmap(d_esq, IDB_BADGUY_ESQ);
		loadBitmap(d_dto, IDB_BADGUY_DTO);
		break;
	case azul:
		loadBitmap(d_baixo, IDB_BADGUY_BAIXO_AZUL);
		loadBitmap(d_cima, IDB_BADGUY_CIMA_AZUL);
		loadBitmap(d_esq, IDB_BADGUY_ESQ_AZUL);
		loadBitmap(d_dto, IDB_BADGUY_DTO_AZUL);
		break;
	}

	setKeyDown(83); //s
	setKeyUp(87); //w
	setKeyLeft(65); //a
	setKeyRight(68); //d
}


BadGuy::~BadGuy(void)
{
}
