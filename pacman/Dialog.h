#ifndef DialogH
#define DialogH
#include <windows.h>
#include <string>

class PacmanApp;
class Dialog
{
private:
	int idDialog;
	PacmanApp *app;
	HWND hWnd;
	bool modalResult;
	Dialog(const Dialog &a);
	Dialog & operator = (const Dialog &a);
protected:
	static BOOL CALLBACK staticDialogProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	virtual BOOL dialogProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	virtual void doInit();
	virtual void setModalResult(bool value);
	virtual int getEditIntValue(int id);
	virtual std::string getEditStrValue(int id);
	virtual void setEditValue(int id, int value);
	virtual void setEditValue( int id, std::string value );
public:
	Dialog(PacmanApp *app, int idDialog);
	virtual void show();
	virtual bool showModal();
	virtual ~Dialog(void);
	PacmanApp *getApp();
	virtual void hide();
	HWND getHWnd()const;
	void close();
	
};

#endif