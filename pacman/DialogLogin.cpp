#include "DialogLogin.h"
#include "resource.h"
#include "PacmanApp.h"

using namespace std;

DialogLogin::DialogLogin(PacmanApp *app) : Dialog(app, IDD_LOGIN) {
}


DialogLogin::~DialogLogin(void)
{


}

BOOL DialogLogin::dialogProc( HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam )
{
	switch (messg) {
	case WM_COMMAND:
		switch(LOWORD(wParam)) 
		{ 
		case IDOK:
			okClick();	
			return 0;
		case IDCANCEL:
			setModalResult(false);
			close();
			return 0;
		}   

	}
	return Dialog::dialogProc(hWnd, messg, wParam, lParam);
}

void DialogLogin::doInit()
{
	setEditValue(IDC_SERVIDOR,getApp()->getConfig()->getHost());
	setEditValue(IDC_UTILIZADOR,getApp()->getConfig()->getUsername());
	setEditValue(IDC_PASSWORD,getApp()->getConfig()->getPassword());
}

void DialogLogin::okClick()
{
    CBufUserProp u;
	string user= getEditStrValue(IDC_UTILIZADOR);
	string pass= getEditStrValue(IDC_PASSWORD);
	string host = getEditStrValue(IDC_SERVIDOR);

	if (!getApp()->getRemote()->connect(host)) {
		
		return;
	}

	if (!getApp()->getRemote()->login(user,pass,u)) {		
		return;
	}


	getApp()->getJogador()->setNome(user);
	getApp()->getJogador()->setKeyDown(u.keyDown);
	getApp()->getJogador()->setKeyUp(u.keyUp);
	getApp()->getJogador()->setKeyLeft(u.keyLeft);
	getApp()->getJogador()->setKeyRight(u.keyRight);
	getApp()->getJogador()->setIsAdmin(u.isAdmin);



	getApp()->getConfig()->setHost(host);
	getApp()->getConfig()->setUsername(user);
	getApp()->getConfig()->setPassword(pass);
	getApp()->getConfig()->save();
	setModalResult(true);
	close();
}

