#pragma once
#include "ClienteThread.h"
#include "..\comum\Application.h"
#include "..\comum\PLMutex.h"
#include <vector>
#include <windows.h>
#include "Jogadores.h"


class Server;
class CommandsThread;
class EngineServer;
class EngineServerPipeThread;


class ServerPacMan:public Application
{
private:
	Server *srv;
	Jogadores *jogadores; 
	PLMutex novoJogoMutex;
	CommandsThread *commandsThread; //servi�o comunica��o (pipe) de thread para commandos
	EngineServer *engineServer; 
	EngineServerPipeThread *engineServerThread; // servidor pipe do jogo
	bool terminate; // true quando a aplica��o � terminada
	std::vector<std::string> loggedIn; // lista de jogadores autenticados
	std::vector<std::string> jogadoresGame; //lsiat de jogadores em jogo
	std::vector<HANDLE> listeners; //lista de pipes para receber notifica��es
	std::vector<ClienteThread *> clientes; // lista de threads dos clientes do pipe principal
	std::vector<ClienteThread *> clientesZombies;  // lista de threads de clientes que cairam..
	int vida;
	void cleanup(PSID pEveryoneSID, PSID pAdminSID, PACL pACL, PSECURITY_DESCRIPTOR pSD);
	ServerPacMan(const ServerPacMan &s);
	ServerPacMan & operator = (const ServerPacMan & s);
public:
	ServerPacMan(Server *server);
	virtual void run();

	void getSecurity( SECURITY_ATTRIBUTES &sa );

	~ServerPacMan();
	virtual void log(std::string m);
	virtual void logError(std::string m, DWORD value);
	virtual HINSTANCE getHInstance();
	void addClient( HANDLE hPipe );
	void removeClient( ClienteThread* cliente );
	bool doLogin(const std::string username,const std::string password, Jogador &jogador );
	Jogadores *getJogadores();
	bool isLoggedIn( const std::string username, bool add );
	void LoggedOut( const std::string username);
	void mataZoombies(); ///liberta liga��es de clientes mortos
	bool JogoActivo(); // true se existe jogo
	bool JogoComecou(); // true de Jogo j� come�ou
	int  JogoJogadoresCount() const;  ///devolve o n� de jogadores em jogo
	PLMutex &getNovoJogoMutex(); // controlo de acesso ao vector jogadoresGame
	void addListener(HANDLE hGame);
	int listenersCount() const;
	HANDLE getListeners(int idx);
	void setTerminate(bool value);
	bool getTerminated() const;
	void JoinGame( std::string username, bool inicioAuto, int xini,int yini);
	void GameStart( std::string username,int nObjectos,int nBadGuys, int nAtaques,bool inicioAuto, int xini, int yini);
	void gameEnd(); //jogo termina;
	void saiJogo( const std::string username );
	void setListeners( int i, HANDLE h );
	void sendKeys( std::string username, int key );
	void desisteJogo(std::string username);
	void updateKeys( std::string username, int left, int right, int up, int down );
	int getPontosGame( std::string username );
	HANDLE getEventCommand();
	HANDLE getEventGame();
};

