#include <sstream>
#include <iostream>
#include "ClienteThread.h"
#include "ServerPacMan.h"
#include "..\comum\Comunication.h"
#include <iosfwd>

using namespace std;

ClienteThread::ClienteThread(ServerPacMan *srv, HANDLE pipe)
{
	this->hPipe=pipe;
	this->srv=srv;
	this->username="";
}


ClienteThread::~ClienteThread()
{

}


void ClienteThread::writePipe(const Commands command,const LPVOID buf,const int len) {
	int ret,n;
	CommandBuf cmd;
	cmd.command=command;
	cmd.len=len;
	ret=WriteFile(hPipe,&cmd,sizeof(cmd),(LPDWORD)&n,NULL);
	if (!ret || !n) {
		//liga��o caiu...
		return ;
	}
	if (len==0) return;
	ret=WriteFile(hPipe,buf,len,(LPDWORD)&n,NULL);
	if (!ret || !n) {
		//liga��o caiu...
		return ;
	}
}

bool ClienteThread::readpipe(LPVOID buf, int len) {
	int ret, n;
	ret = ReadFile(hPipe, buf, len,(LPDWORD) &n, NULL);
	if (!ret || !n) return false;

	if (len!=n) return false;

	return true;
}

void ClienteThread::execute()
{
	stringstream ss;
	int id=GetCurrentThreadId();
	CommandBuf cmd;
	ss << "[Thread"<<id<<"] Um cliente ligou-se...\n";
	log(ss.str());
	while (1) {
		if (readpipe(&cmd,sizeof(cmd))) {
			cmdSwitch(cmd);
		} else {
			break;
		}
		if (srv->getTerminated()) return;
	}

	ss.clear();
	ss << "[Thread"<<id<<"] Um cliente desligou-se...\n";
	log(ss.str());
	if (hPipe!=0) {
		if(! DisconnectNamedPipe(hPipe)){
			//perror("Erro ao desligar o pipe!");
			//return ;
		}
		CloseHandle(hPipe);
	}
	hPipe=0;
	srv->removeClient(this);
}

void ClienteThread::cmdSwitch( const CommandBuf &cmd )
{
	switch(cmd.command) {
	case cmd_c_Login:
		cmdLogin(cmd);
		break;
	case cmd_c_UserAdd:
		cmdUserAdd(cmd);
		break;
	case cmd_c_UserList:
		cmdUserList(cmd);
		break;
	case cmd_c_UserDelete:
		cmdUserDelete(cmd);
		break;
	case cmd_c_UserGet:
		cmdUserGet(cmd);
		break;
	case cmd_c_NovoJogo:
		cmdNovoJogo(cmd);
		break;
	case cmd_c_LigarJogo:
		cmdLigarJogo(cmd);
		break;
	case cmd_c_Sendkeys:
		cmdSendkeys(cmd);
		break;	
	case cmd_c_DesisteJogo:
		cmdDesisteJogo(cmd);
		break;	
	case cmd_c_UserSetKeys:
		cmdUserSetKeys(cmd);
		break;
	case cmd_c_top10:
		cmdTop10(cmd);
		break;
    default:
        writePipe(cmd_s_cmdUnknown,NULL,0);
	}
}

void ClienteThread::cmdLogin( const CommandBuf & cmd )
{
	CBufLogin login;
	Jogador j;
	if (cmd.len!=sizeof(CBufLogin)) return;
	if (!readpipe(&login,cmd.len)) return;
	srv->getJogadores()->lock();
	if (srv->doLogin(login.username, login.password,j)) {
		if (srv->isLoggedIn(login.username,true)) {
			writePipe(cmd_s_UserAlreadyLoggedin, NULL, 0);
			srv->getJogadores()->unlock();
			return;
		}
		this->username=login.username;
		CBufUserProp u;
		loadJogadorProp(u, j);
		srv->getJogadores()->unlock();
		writePipe(cmd_s_Loggedin, &u, sizeof(u));
		return;
	}
	srv->getJogadores()->unlock();
	writePipe(cmd_s_LoginError,NULL,0);
}

void ClienteThread::log( const std::string &str )
{
	srv->log(str);
}

void ClienteThread::cmdUserAdd( const CommandBuf & cmd )
{
    CBufUserAdd u;
	if (cmd.len!=sizeof(CBufUserAdd)) return;
	if (!readpipe(&u,cmd.len)) return;
	srv->getJogadores()->lock();
	int idx=srv->getJogadores()->findJogador(u.user.username);
	if (idx!=-1) {
		srv->getJogadores()->update(idx,u.user.password,u.isAdmin);
		srv->getJogadores()->unlock();
		writePipe(cmd_s_UserUpdated,NULL,0);
		// ou pode tn ser:
		//writePipe(cmd_s_UserDuplicated,NULL,0);
		return;
	}

    srv->getJogadores()->add(u.user.username,u.user.password,u.isAdmin);
	srv->getJogadores()->unlock();
	writePipe(cmd_s_UserAdded,NULL,0);
}

void ClienteThread::cmdUserList( const CommandBuf & cmd )
{
	CBufUserList ul;
	int n=0;
	srv->getJogadores()->lock();
	int f=srv->getJogadores()->getCount();
	do {
        ul.nUsers=MAXUSERS;
		
		if (ul.nUsers>f) ul.nUsers=f;

		for(int i=n; i<n+ul.nUsers; i++) {
			Jogador *j= srv->getJogadores()->getJogador(i);
    		strcpy_s((ul.users[i]),MAX_CHAR,j->username.c_str());
		}
		n +=ul.nUsers;
		f = srv->getJogadores()->getCount()-n;
		ul.eof = f==0;
		writePipe(cmd_s_UserList,&ul,sizeof(ul));

		
	} while(f!=0);

	srv->getJogadores()->unlock();
	
}

void ClienteThread::cmdUserGet( const CommandBuf & cmd )
{
	CBufUserName u;
	if (cmd.len!=sizeof(CBufUserName)) return;
	if (!readpipe(&u,cmd.len)) return;
	srv->getJogadores()->lock();
	int idx=srv->getJogadores()->findJogador(u.username);
	if (idx==-1) {
		srv->getJogadores()->unlock();
		writePipe(cmd_s_UserNotExist,NULL,0);
		return;
	}
	Jogador *j=srv->getJogadores()->getJogador(idx);
	CBufUser user;
	strcpy_s(user.user.username, MAX_CHAR, j->username.c_str());
	strcpy_s(user.user.password, MAX_CHAR, j->password.c_str());
	loadJogadorProp(user.Prop,*j);
	srv->getJogadores()->unlock();
	writePipe(cmd_s_GetUser,&user,sizeof(user));
}
void ClienteThread::cmdUserDelete( const CommandBuf & cmd )
{
	CBufUserName u;
	if (cmd.len!=sizeof(CBufUserName)) return;
	if (!readpipe(&u,cmd.len)) return;
	srv->getJogadores()->lock();
	int idx=srv->getJogadores()->findJogador(u.username);
	if (idx==-1) {
		srv->getJogadores()->unlock();
		writePipe(cmd_s_UserNotExist,NULL,0);
		return;
	}
	srv->getJogadores()->deleteUser(u.username);
	srv->getJogadores()->unlock();
	writePipe(cmd_s_UserDeleted,NULL,0);
}


void ClienteThread::loadJogadorProp( CBufUserProp &u, Jogador &j )
{
	u.NJogos = j.NJogos;
	u.NPontos = j.NPontos;
	u.NDesistencias= j.NDesistencias;
	u.NQuebras = j.NVitorias;
	u.isAdmin = j.isAdmin;
	u.keyDown = j.keyDown;
	u.keyLeft = j.keyLeft;
	u.keyRight = j.keyRight;
	u.keyUp = j.keyUp;
}

void ClienteThread::cmdLigarJogo( const CommandBuf & cmd )
{
	CBufLigarJogo lj;
	if (cmd.len!=sizeof(CBufLigarJogo)) return;
	if (!readpipe(&lj,cmd.len)) return;

	srv->getNovoJogoMutex().lock();
	if (!srv->JogoActivo()) {
		srv->getNovoJogoMutex().unlock();
		writePipe(cmd_s_NaoExisteJogo,NULL,0);
		return;
	}
	if (srv->JogoComecou()) {
		srv->getNovoJogoMutex().unlock();
		writePipe(cmd_s_JogoADecorrer,NULL,0);
		return;
	}
	if ((srv->JogoJogadoresCount()>=MAXJOGADORES)) {
		srv->getNovoJogoMutex().unlock();
		writePipe(cmd_s_MaxJogadores,NULL,0);
		return;
	}
	srv->JoinGame(username,lj.inicioAuto, lj.xini, lj.yini);
	srv->getNovoJogoMutex().unlock();
	srv->getJogadores()->lock();
	srv->getJogadores()->incJogo(username);
	srv->getJogadores()->unlock();
	writePipe(cmd_s_LigarJogoOk,NULL,0);
}

std::string ClienteThread::getUserName()
{
	return username;
}

void ClienteThread::cmdNovoJogo( const CommandBuf & cmd )
{
	CBufInicarJogo ij;
	if (cmd.len!=sizeof(CBufInicarJogo)) return;
	if (!readpipe(&ij,cmd.len)) return;

	srv->getNovoJogoMutex().lock();
	if (srv->JogoActivo()) {
		srv->getNovoJogoMutex().unlock();
		writePipe(cmd_s_JaExisteJogo,NULL,0);
		return;
	}
	
	srv->getNovoJogoMutex().unlock();
	srv->getJogadores()->lock();
	srv->getJogadores()->incJogo(username);
	srv->getJogadores()->unlock();
	writePipe(cmd_s_novoJogoOk,NULL,0);
	srv->GameStart(username,ij.nObjectos,ij.nBadGuys,ij.nAtaques,ij.inicioAuto,ij.xini,ij.yini);
	
}

void ClienteThread::cmdSendkeys( const CommandBuf & cmd )
{
	CBufSendKeys sk;
	if (cmd.len!=sizeof(CBufSendKeys)) return;
	if (!readpipe(&sk,cmd.len)) return;
	srv->sendKeys(username,sk.key);

}

void ClienteThread::cmdDesisteJogo( const CommandBuf & cmd )
{
	srv->getJogadores()->lock();
	srv->getJogadores()->desisteJogo(username,srv->getPontosGame(username));
	srv->getJogadores()->unlock();
	srv->desisteJogo(username);
}

void ClienteThread::cmdUserSetKeys( const CommandBuf & cmd )
{
	CBufSetKeys sk;
	if (cmd.len!=sizeof(CBufSetKeys)) return;
	if (!readpipe(&sk,cmd.len)) return;
	srv->getJogadores()->lock();
	srv->getJogadores()->updateKeys(username, sk.left, sk.right, sk.up, sk.down);
	srv->getJogadores()->unlock();

	srv->updateKeys(username, sk.left, sk.right, sk.up, sk.down);
}

void ClienteThread::cmdTop10( const CommandBuf & cmd )
{
	srv->getJogadores()->lock();

	CbufTop10 t;
	t.count=0;

	srv->getJogadores()->ordenarPontos();

	for(int i=0;i<srv->getJogadores()->getCount(); i++) {
		strcpy_s((t.jogadores[t.count].nome),MAX_CHAR,srv->getJogadores()->getJogador(i)->username.c_str());
		t.jogadores[t.count].NDesistencias = srv->getJogadores()->getJogador(i)->NDesistencias;
		t.jogadores[t.count].NPontos = srv->getJogadores()->getJogador(i)->NPontos;
		t.jogadores[t.count].NVitorias = srv->getJogadores()->getJogador(i)->NVitorias;
		t.jogadores[t.count].NDerroras = srv->getJogadores()->getJogador(i)->NDerroras;


		t.count++;
	}


	srv->getJogadores()->unlock();
	writePipe(cmd_s_top10,&t,sizeof(CbufTop10));

}
