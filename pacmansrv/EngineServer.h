#pragma once
#include "..\comum\gamebase.h"

class ServerPacMan;
class EngineServerThread;

enum State {esperaJogadores, correr};

class EngineServer :public GameBase
{
private:
	State state;
	ServerPacMan *srv;
	EngineServerThread *gamethread;
	EngineServer(const EngineServer &a);
	EngineServer & operator = (const EngineServer &a);
	void actualizaJogadores();
public:
	EngineServer(ServerPacMan *srv);
	virtual void avanca();
	virtual void start();
	virtual void terminou();
	bool getEsperaJogadores() const;
	~EngineServer();
	void notifyClientes(gameCmd cmd);
	void sendkeys( std::string username, int key );
	void addBadGuys( int nBadGuys );
	void desisteJogo(std::string username);
	void updateKeys( std::string username, int left, int right, int up, int down );
	int getPontosGame( std::string username );
};

