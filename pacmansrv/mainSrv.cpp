#include <windows.h>
#include "Server.h"
#include "..\comum\service.h"



#ifdef DEBUG_CONSOLA

void main() {
	Server *srv= new Server(0,0);
	srv->run();
	delete srv;
}

#else


SERVICE_STATUS          MyServiceStatus; 
SERVICE_STATUS_HANDLE   MyServiceStatusHandle;
HANDLE EventCommand;
HANDLE EventGame;
HANDLE hEventSource;
int status;
 
Server *srv=NULL;
 
VOID  WINAPI MyServiceStart (DWORD argc, LPTSTR *argv); 
VOID  WINAPI MyServiceCtrlHandler (DWORD opcode); 
void MostrarErro(LPSTR str,DWORD val);


void runPacMan(HANDLE EventCommand,HANDLE EventGame){

	srv= new Server(EventCommand,EventGame,hEventSource);
	srv->run();
	delete srv;
}


void main( ) 
{ 
    SERVICE_TABLE_ENTRY   DispatchTable[] = 
    { 
        { NOME_SERVICO, MyServiceStart      }, 
        { NULL,              NULL          } 
    }; 
 
    if (!StartServiceCtrlDispatcher( DispatchTable)) 
    { 
        MostrarErro(" [ServNT] StartServiceCtrlDispatcher error = %d\n", GetLastError()); 
    } 
} 


void WINAPI MyServiceStart(DWORD argc, LPTSTR *argv) 
{

    MyServiceStatus.dwServiceType        = SERVICE_WIN32; 
    MyServiceStatus.dwCurrentState       = SERVICE_START_PENDING; 
    MyServiceStatus.dwControlsAccepted   = 0;
	MyServiceStatus.dwWin32ExitCode      = 0; 
    MyServiceStatus.dwServiceSpecificExitCode = 0; 
    MyServiceStatus.dwCheckPoint         = 0; 
    MyServiceStatus.dwWaitHint           = 0; 
 
    MyServiceStatusHandle = RegisterServiceCtrlHandler( 
        NOME_SERVICO, 
        MyServiceCtrlHandler); 
 
    if (MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0) 
    { 
        MostrarErro(" [ServNT] RegisterServiceCtrlHandler failed %d\n", GetLastError()); 
        return; 
    } 
 
    if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) 
    { 
        status = GetLastError(); 
        MostrarErro(" [ServNT] SetServiceStatus error %ld\n",status);
		return;
    } 

	EventGame=CreateEvent(NULL,FALSE,FALSE,NULL);
	EventCommand=CreateEvent(NULL,FALSE,FALSE,NULL);

	if (EventGame == NULL) {
        status = GetLastError(); 
        MostrarErro(" [ServNT] CreateEvent error %ld\n",status);
		return;
	}
	if (EventCommand == NULL) {
		status = GetLastError(); 
		MostrarErro(" [ServNT] CreateEvent error %ld\n",status);
		return;
	}

    // Initialization complete - report running status. 
    MyServiceStatus.dwCurrentState       = SERVICE_RUNNING; 
    MyServiceStatus.dwCheckPoint         = 0; 
    MyServiceStatus.dwWaitHint           = 0; 
    MyServiceStatus.dwControlsAccepted   = SERVICE_ACCEPT_STOP; // | SERVICE_ACCEPT_PAUSE_CONTINUE; 
 
    if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) 
    { 
        status = GetLastError(); 
        MostrarErro(" [ServNT] SetServiceStatus error %ld\n",status);
		return;
    } 

	MostrarErro(" [ServNT] Vou iniciar o processamento \n",0); 

	runPacMan(EventCommand, EventGame);

	MostrarErro(" [ServNT] terminou",0); 

	CloseHandle(EventGame);
	CloseHandle(EventCommand);
	

    MyServiceStatus.dwCurrentState       = SERVICE_STOPPED; 
    MyServiceStatus.dwCheckPoint         = 0; 
    MyServiceStatus.dwWaitHint           = 0; 
 
    if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) 
    { 
        status = GetLastError(); 
        MostrarErro(" [ServNT] SetServiceStatus error %ld\n",status); 
    } 

 
    return; 
}

VOID WINAPI MyServiceCtrlHandler (DWORD Opcode) 
{ 
    switch(Opcode) 
    { 
        case SERVICE_CONTROL_PAUSE: 
        // Do whatever it takes to pause here. 
            MyServiceStatus.dwCurrentState = SERVICE_PAUSED; 
            break; 
 
        case SERVICE_CONTROL_CONTINUE: 
        // Do whatever it takes to continue here. 
            MyServiceStatus.dwCurrentState = SERVICE_RUNNING; 
            break; 
 
        case SERVICE_CONTROL_STOP: 
        // Do whatever it takes to stop here. 
            MyServiceStatus.dwWin32ExitCode = 0; 
            MyServiceStatus.dwCurrentState  = SERVICE_STOP_PENDING; 
            MyServiceStatus.dwCheckPoint    = 0; 
            MyServiceStatus.dwWaitHint      = 0; 
 
            if (!SetServiceStatus (MyServiceStatusHandle,&MyServiceStatus))
            { 
                status = GetLastError(); 
                MostrarErro(" [ServNT] SetServiceStatus error %ld\n",status); 
            } 
			
			if (srv!=NULL) {
			  srv->setTerminated(true);
			}

			SetEvent(EventCommand);
			SetEvent(EventGame);
			

            MostrarErro(" [ServNT] Leaving MyService \n",0); 
			if (srv!=NULL) {
				//delete srv;
			}
            return; 
 
        case SERVICE_CONTROL_INTERROGATE: 
        // Fall through to send current status. 
            break; 
 
        default: 
            MostrarErro(" [ServNT] Unrecognized opcode %ld\n", 
                Opcode); 
			break;
    } 

	// Send current status. 
    if (!SetServiceStatus (MyServiceStatusHandle,  &MyServiceStatus)) 
    { 
        status = GetLastError(); 
        MostrarErro(" [ServNT] SetServiceStatus error %ld\n",status); 
    } 
    return; 
} 


void MostrarErro(LPSTR str,DWORD val)
{
	char buf[1024];
	LPSTR msg[2]={buf,NULL};
	
	wsprintf(buf,str,val);

    if (!hEventSource) {
        hEventSource = RegisterEventSource(NULL,            // local machine
                                           NOME_SERVICO); // source name
    }

    if (hEventSource) {
        ReportEvent(hEventSource,
                      EVENTLOG_INFORMATION_TYPE,
                      0,
                      0,
                      NULL,   // sid
                      1,
                      0,
                      (const char **)msg,
                      NULL);
    }

}

#endif