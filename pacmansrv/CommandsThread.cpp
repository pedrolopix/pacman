#include "CommandsThread.h"
#include "..\comum\service.h"

#define PIPE_NAMESRV "\\\\."PIPE_NAME

CommandsThread::CommandsThread(ServerPacMan *srv, LPSECURITY_ATTRIBUTES lpSecurityAttributes)
{
	this->srv=srv;
	this->lpSecurityAttributes=lpSecurityAttributes;
}


CommandsThread::~CommandsThread(void)
{
}

void CommandsThread::execute()
{
	int status;
	OVERLAPPED ovl;

	while (true) {
#ifdef DEBUG_CONSOLA				
		HANDLE hPipe= CreateNamedPipe(PIPE_NAMESRV, PIPE_ACCESS_DUPLEX, PIPE_WAIT | PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE, PIPE_UNLIMITED_INSTANCES , MAXBUFFER, MAXBUFFER,TIMEOUT, lpSecurityAttributes);
#else
		HANDLE hPipe= CreateNamedPipe(PIPE_NAMESRV, PIPE_ACCESS_DUPLEX|FILE_FLAG_OVERLAPPED, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE, PIPE_UNLIMITED_INSTANCES , MAXBUFFER, MAXBUFFER,TIMEOUT, lpSecurityAttributes);
#endif
		if(hPipe == INVALID_HANDLE_VALUE){
			srv->logError("Limite Atingido comands pipe!",GetLastError());

			return;
		}

		srv->log("Esperar ligacao de um cliente... (comands pipe)\n");
		//srv->log(PIPE_NAME);
#ifdef DEBUG_CONSOLA				
		if(!ConnectNamedPipe(hPipe, NULL)){
			status = GetLastError();
			if (status != ERROR_IO_PENDING) {
				srv->logError("Erro na liga��o ao cliente!",status);
				return;
			}
		}

#else
		ZeroMemory(&ovl,sizeof(ovl));
		ovl.hEvent=srv->getEventCommand();

		if(!ConnectNamedPipe(hPipe, &ovl)){
			status = GetLastError();
			if (status != ERROR_IO_PENDING) {
				srv->logError("Erro na liga��o ao cliente!",status);
				return;
			}
		}

		srv->log("wait comands pipe");
		WaitForSingleObject(srv->getEventCommand(),INFINITE);
#endif
		if (srv->getTerminated()){
			DisconnectNamedPipe(hPipe);
			CloseHandle(hPipe);		
			break;
		}

		srv->mataZoombies();
		srv->addClient(hPipe);
	}
}
