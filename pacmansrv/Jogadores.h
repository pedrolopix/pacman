#pragma once
#include "..\comum\Comunication.h"
#include "..\comum\PLMutex.h"
#include <vector>

struct Jogador {
	std::string username;
	std::string password;
	int NJogos;
	int NPontos;
	int NDesistencias;
	int NVitorias;
	int NDerroras;
	bool isAdmin;
	int keyDown;
	int keyLeft;
	int keyRight;
	int keyUp;
}; 

class Jogadores
{
private:
	std::string filename;
	std::vector<Jogador *> list;
	PLMutex *mutex;
	void addAdmin();
	Jogadores(const Jogadores &a);
	Jogadores & operator = (const Jogadores &a);
public:
	Jogadores();
	void load(const std::string filename);
	void save(const std::string filename);
	void save();
	int findJogador(const std::string &nome);
	void add(const std::string &nome,const std::string &password, const bool isAdmin);
	bool login(const std::string &nome,const std::string &password, Jogador &user);
	int getCount() const;
	Jogador *getJogador(int idx) const;
	~Jogadores();
	void deleteUser( const std::string username );
	void update( const int idx, const std::string password, const bool isAdmin );
	void lock();
	void unlock();
	void updateKeys( std::string username,int left, int right, int up, int down );
	void incJogo( std::string username );
	void desisteJogo( std::string username, int npontos);
	void ganhouJogo( std::string username, int npontos);
	void ordenarPontos();
	void perdeuJogo( std::string username, int nPontos );
};

