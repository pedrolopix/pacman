#include "ServerPacMan.h"
#include <windows.h>
#include <stdio.h>
#include <aclapi.h>
#include <stdio.h>
#include <strsafe.h>
#include "Server.h"
#include "CommandsThread.h"
#include "..\comum\Comunication.h"
#include "..\comum\utils.h"
#include "EngineServerPipeThread.h"
#include "EngineServer.h"
#include "..\comum\GoodGuyBase.h"

using namespace std;

ServerPacMan::ServerPacMan(Server *srv)
{
	terminate=false;
	this->srv = srv;
	jogadores= new Jogadores();
	jogadores->load(getAppPath()+ "\\jogadores.dat");
	commandsThread=NULL;
	engineServer=NULL;
	engineServerThread=NULL;
	vida=1;
}


ServerPacMan::~ServerPacMan()
{
	terminate=true;
	novoJogoMutex.lock();
	gameEnd();
	novoJogoMutex.unlock();

	if (commandsThread!=NULL) delete commandsThread;
	if (engineServerThread!=NULL) delete engineServerThread;
	if (engineServer!=NULL)  delete engineServer;


	for(unsigned int i=0; i< clientes.size();i++) {
		delete clientes[i];
	}
	jogadores->save(getAppPath()+ "\\jogadores.dat");
	delete jogadores;
	mataZoombies();
}


void ServerPacMan::cleanup(PSID pEveryoneSID, PSID pAdminSID, PACL pACL, PSECURITY_DESCRIPTOR pSD)
{
	if(pEveryoneSID)
		FreeSid(pEveryoneSID);
	if(pAdminSID)
		FreeSid(pAdminSID);
	if(pACL)
		LocalFree(pACL);
	if(pSD)
		LocalFree(pSD);
}

void ServerPacMan::run()
{
	log("start");
	SECURITY_ATTRIBUTES sa1;
	getSecurity(sa1);
	SECURITY_ATTRIBUTES sa2;
	getSecurity(sa2);
	
	 
	commandsThread= new CommandsThread(this,&sa1);
	commandsThread->start();

	engineServerThread= new EngineServerPipeThread(this,&sa2);
	engineServerThread->start();

	commandsThread->wait(INFINITE);
	engineServerThread->wait(INFINITE);

}

void ServerPacMan::addClient( HANDLE hPipe )
{
	ClienteThread *cli= new ClienteThread(this, hPipe);
	clientes.push_back(cli);
	cli->start();
}

void ServerPacMan::removeClient( ClienteThread* cliente )
{
	for(int i=0; clientes.size(); i++) {
		if (clientes[i]==cliente) {
			LoggedOut(cliente->getUserName());
			clientes.erase(clientes.cbegin()+i);
			break;
		}
	}
	
	clientesZombies.push_back(cliente);
}

bool ServerPacMan::doLogin(const string username,const string password, Jogador &jogador )
{
	return jogadores->login(username, password,jogador);;
}

Jogadores * ServerPacMan::getJogadores()
{
	return jogadores;
}

void ServerPacMan::log( std::string m )
{
	srv->log(m);
}

void ServerPacMan::logError(std::string m, DWORD value) {
	srv->logError(m,value);
}


bool ServerPacMan::isLoggedIn( const std::string username, bool add )
{
	for(unsigned int i=0; i< loggedIn.size(); i++) {
		if (loggedIn[i]==username) return true;
	}
	if (add) loggedIn.push_back(username);
	return false;
}

void ServerPacMan::LoggedOut( const std::string username)
{
	for(unsigned int i=0; i< loggedIn.size(); i++) {
		if (loggedIn[i]==username) {
			//remove do jogo
			novoJogoMutex.lock();
			saiJogo(username);
			novoJogoMutex.unlock();
			loggedIn.erase(loggedIn.cbegin()+i);
			return ;
		}
	}
	
}

void ServerPacMan::mataZoombies()
{
	for(unsigned int i=0; i< clientesZombies.size(); i++) {
		delete clientesZombies[i];
	}
	clientesZombies.clear();
}

HINSTANCE ServerPacMan::getHInstance()
{
	return srv->getHInstance();
}

void ServerPacMan::setTerminate( bool value )
{
   terminate=value;
   srv->setTerminated(value);
}

bool ServerPacMan::getTerminated() const
{
	return terminate || srv->getTerminated();
}


PLMutex &ServerPacMan::getNovoJogoMutex()
{
	return novoJogoMutex;
}

bool ServerPacMan::JogoComecou()
{
	if (engineServer==NULL) return false;
	return !engineServer->getEsperaJogadores();
}

int ServerPacMan::JogoJogadoresCount() const
{
	return jogadoresGame.size();
}

void ServerPacMan::JoinGame(std::string username, bool inicioAuto, int xini,int yini)
{
	jogadoresGame.push_back(username);
	GoodGuyBase *g=new GoodGuyBase(engineServer,username,vida,inicioAuto,xini,yini);
	int idx=jogadores->findJogador(username);
	if (idx==-1) srv->logError("jogador devia de existir!",0);
	Jogador *j=jogadores->getJogador(idx);
	g->setKeyDown(j->keyDown);
	g->setKeyLeft(j->keyLeft);
	g->setKeyRight(j->keyRight);
	g->setKeyUp(j->keyUp);
	engineServer->addGoodGuy(g);
}

void ServerPacMan::GameStart( std::string username, int nObjectos,int nBadGuys, int vida,bool inicioAuto, int xini,int yini)
{
	this->vida= vida;
	if (engineServer!=NULL) delete engineServer;
	gameEnd();
	
	engineServer= new EngineServer(this);  
	engineServer->addBadGuys(nBadGuys);
	engineServer->getTabuleiro()->SetPontos(nObjectos);
	JoinGame(username,inicioAuto,xini,yini);
	engineServer->start();

}

void ServerPacMan::gameEnd()
{
   jogadoresGame.clear();
   for(unsigned int i=0; i<listeners.size();i++) {
		CloseHandle(listeners[i]);
   }
   listeners.clear();
}

bool ServerPacMan::JogoActivo()
{
	return (jogadoresGame.size()>0);
}

void ServerPacMan::saiJogo( const std::string username )
{
	for (unsigned int i=0; i<jogadoresGame.size();i++) {
		if (jogadoresGame[i]==username) {
		   jogadoresGame.erase(jogadoresGame.cbegin()+i);
		   return;
		}
	}
}

void ServerPacMan::addListener( HANDLE hGame )
{
	listeners.push_back(hGame);
}

int ServerPacMan::listenersCount() const
{
	return listeners.size();
}

HANDLE ServerPacMan::getListeners( int idx )
{
	return listeners[idx];
}

void ServerPacMan::setListeners( int i, HANDLE h )
{
	listeners[i]=h;
}

void ServerPacMan::sendKeys( std::string username, int key )
{
	if (engineServer!=NULL)
     	engineServer->sendkeys(username,key);
}

void ServerPacMan::desisteJogo(std::string username)
{
	if (engineServer!=NULL)
		engineServer->desisteJogo(username);
}

void ServerPacMan::updateKeys( std::string username, int left, int right, int up, int down )
{
	if (engineServer!=NULL)
		engineServer->updateKeys(username,  left,  right,  up,  down );
}

int ServerPacMan::getPontosGame( std::string username )
{
	if (engineServer==NULL) return 0;
	return engineServer->getPontosGame(username);
}


HANDLE ServerPacMan::getEventCommand() {
	return srv->getEventCommand();
}

HANDLE ServerPacMan::getEventGame()
{
	return srv->getEventGame();
}

void ServerPacMan::getSecurity( SECURITY_ATTRIBUTES &sa )
{
	PSECURITY_DESCRIPTOR pSD;
	PACL pAcl;
	EXPLICIT_ACCESS ea;
	PSID pEveryoneSID = NULL, pAdminSID = NULL;
	SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;



	pSD = (PSECURITY_DESCRIPTOR) LocalAlloc(LPTR,SECURITY_DESCRIPTOR_MIN_LENGTH);
	if (pSD == NULL) {
		logError("Erro LocalAlloc!!!",GetLastError());
		return;
	}
	if (!InitializeSecurityDescriptor(pSD,SECURITY_DESCRIPTOR_REVISION)) {
		logError("Erro IniSec!!!",GetLastError());
		return;
	}

	// Create a well-known SID for the Everyone group.
	if(!AllocateAndInitializeSid(&SIDAuthWorld, 1, SECURITY_WORLD_RID,0, 0, 0, 0, 0, 0, 0, &pEveryoneSID))
	{
		logError("AllocateAndInitializeSid() ",GetLastError());
		cleanup(pEveryoneSID, pAdminSID, NULL, pSD);
	}
	else
		log("AllocateAndInitializeSid() for the Everyone group is OK");

	ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS));

	ea.grfAccessPermissions = GENERIC_WRITE| GENERIC_READ;
	ea.grfAccessMode = SET_ACCESS;
	ea.grfInheritance= SUB_CONTAINERS_AND_OBJECTS_INHERIT;
	ea.Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea.Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ea.Trustee.ptstrName = (LPTSTR) pEveryoneSID;

	if (SetEntriesInAcl(1,&ea,NULL,&pAcl)!=ERROR_SUCCESS) {
		logError("Erro SetAcl!!!",GetLastError());
		return;
	}

	if (!SetSecurityDescriptorDacl(pSD,TRUE,pAcl,FALSE)) {
		logError("Erro IniSec!!!",GetLastError());
		return;
	}

	sa.nLength = sizeof(sa);
	sa.lpSecurityDescriptor = pSD;
	sa.bInheritHandle = TRUE;
}
