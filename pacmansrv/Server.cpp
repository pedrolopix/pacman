#include "Server.h"
#include "ServerPacMan.h"
#include "..\comum\service.h"

Server::Server(HANDLE EventCommand,HANDLE EventGame, HANDLE hEventSource)
{
	this->eventCommand=EventCommand;
	this->eventGame=EventGame;
	this->hEventSource= hEventSource;
	serverPacMan=NULL;
	terminated=false;
}


void Server::setTerminated(bool terminated) {
	this->terminated=terminated;
}

bool Server::getTerminated()const {
	return terminated;
}

Server::~Server(void)
{
	if (serverPacMan!=NULL) delete serverPacMan;
}

void Server::log( std::string msg) {
	logError(msg,0);
}


void Server::logError( std::string msg, DWORD val)
{
#ifdef DEBUG_CONSOLA
	printf(msg.c_str());
	printf("\n");
#else

	char buf[1024];
	LPSTR msgl[2]={buf,NULL};
	
	wsprintf(buf,msg.c_str(),val);

    if (hEventSource) {
        ReportEvent(hEventSource,
                      EVENTLOG_INFORMATION_TYPE,
                      0,
                      0,
                      NULL,   // sid
                      1,
                      0,
                      (const char **)msgl,
                      NULL);
    }
#endif
}




void Server::run()
{
	serverPacMan=new ServerPacMan(this);
	serverPacMan->run();
}

HINSTANCE Server::getHInstance()
{
	return 0;
}


HANDLE  Server::getEventCommand() {
	return eventCommand;
}

HANDLE  Server::getEventGame(){
	return eventGame;
}
