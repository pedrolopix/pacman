#ifndef EngineServerThreadH
#define EngineServerThreadH
#include <Windows.h>
#include "..\comum\BaseThread.h"
#include "ServerPacMan.h"


class EngineServerPipeThread:public BaseThread
{
private:
	ServerPacMan *srv; 
	LPSECURITY_ATTRIBUTES lpSecurityAttributes;
protected:
	virtual void execute();
public:
	EngineServerPipeThread(ServerPacMan *srv, LPSECURITY_ATTRIBUTES lpSecurityAttributes);
	~EngineServerPipeThread(void);
};

#endif;