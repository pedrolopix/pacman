#ifndef ServerH
#define ServerH

#include <Windows.h>
#include <string>
#include <stdio.h>
#include "..\comum\Application.h"
#include "..\comum\service.h"
#include "Jogadores.h"

class ServerPacMan;

class Server:public Application
{
private:
	ServerPacMan *serverPacMan;
	bool terminated;
	HANDLE hEventSource;
	HANDLE eventCommand;
	HANDLE eventGame;
	int status;
	Server(const Server &s);
	Server & operator = (const Server & s);
public:
	Server(HANDLE EventCommand, HANDLE EventGame ,HANDLE hEventSource);
	virtual void run();
	virtual HINSTANCE getHInstance();
	void setTerminated(bool terminated);
	bool getTerminated()const;
	void log(std::string msg);
	void logError( std::string msg, DWORD val);
	HANDLE getEventCommand();
	HANDLE getEventGame();
	~Server();
};


#endif;