#ifndef CommandsThreadH
#define  CommandsThreadH
#include <Windows.h>
#include "..\comum\BaseThread.h"
#include "ServerPacMan.h"

class CommandsThread:public BaseThread
{
private:
	ServerPacMan *srv;
	LPSECURITY_ATTRIBUTES lpSecurityAttributes;
	CommandsThread(const CommandsThread &a);
	CommandsThread & operator = (const CommandsThread &a);
protected:
	virtual void execute();
public:
	CommandsThread(ServerPacMan *srv, LPSECURITY_ATTRIBUTES lpSecurityAttributes);
	~CommandsThread();
};

#endif