#include <Windows.h>
#include <iostream>
#include <fstream>
#include "Jogadores.h"
#include "..\comum\PLMutex.h"


using namespace std;

Jogadores::Jogadores(void)
{
	mutex = new PLMutex(false);
}


Jogadores::~Jogadores(void)
{
	//for(unsigned int i=0; i<list.size(); i++) {
	//	delete list[i];
	//}
	delete mutex;
}

void Jogadores::load( const std::string filename )
{
	for(unsigned int i=0; i<list.size(); i++) {
		delete list[i];
	}
	list.clear();
	this->filename=filename;
	ifstream file(filename.c_str(), ios::binary);
	if (file.is_open())
	{
		int size;
		file.read((char*)&size, 4);
		for(int i=0; i<size; i++) {
			Jogador *j= new Jogador();
			file.read((char*)j,  sizeof(Jogador));
			list.push_back(j);
		}
		file.close();
	} else
	{
	   addAdmin();
	}
}

void Jogadores::save( const std::string filename )
{
	this->filename=filename;
	ofstream file(filename.c_str(), ios::binary);
	if (file.is_open())
	{
		int size1=list.size();
		file.write((const char*)&size1, 4);
		for(int i=0; i<size1; i++)
		   file.write((const char*)list[i], sizeof(Jogador));
		file.close();
	}

}

void Jogadores::save()
{
	save(filename);
}

void Jogadores::add( const std::string &nome,const std::string &password, const bool isAdmin )
{
	Jogador *j= new Jogador();
	j->NDesistencias=0;
	j->NJogos=0;
	j->NVitorias=0;
	j->NDerroras=0;
	j->NPontos=0;
	j->username=nome;
	j->password=password;
	j->isAdmin= isAdmin;
	j->keyLeft = VK_LEFT;
	j->keyRight = VK_RIGHT;
	j->keyDown = VK_DOWN;
	j->keyUp =VK_UP;
	list.push_back(j);
	save();
}

bool Jogadores::login( const std::string &nome,const std::string &password, Jogador &user )
{	
	int idx = findJogador(nome);
	if (idx==-1) return false;
	user=*list[idx];
	return true;
}

void Jogadores::addAdmin()
{
	add("admin","admin", true);
}

int Jogadores::findJogador( const std::string &nome )
{
	for(unsigned int i=0; i<list.size(); i++) {
		if(list[i]->username.compare(nome)==0) {
			return i;
		}
	}
	return -1;
}

int Jogadores::getCount() const
{
	return list.size();
}

Jogador *Jogadores::getJogador( int idx ) const
{
	return list[idx];
}

void Jogadores::deleteUser( const std::string username )
{
	Jogador *j=NULL;
	for(unsigned int i=0; i<list.size(); i++) {
		if(list[i]->username.compare(username)==0) {
			j= list[i];
			list.erase(list.cbegin()+i);
			break;
		}
	}
	if (j!=NULL) {
		save();
		//delete j; //isto d� AV n�o devia dar...
	}
	
}

void Jogadores::update( const int idx, const std::string password, const bool isAdmin )
{
	Jogador *j=getJogador(idx);
	if (j==NULL) return;
	j->password=password;
	j->isAdmin=isAdmin;
	save();
}

void Jogadores::lock()
{
	mutex->lock();
}

void Jogadores::unlock()
{
	mutex->unlock();
}

void Jogadores::updateKeys( std::string username,int left, int right, int up, int down )
{
	int idx=findJogador(username);
	if (idx==-1) return;
	Jogador *j=getJogador(idx);
	if (j==NULL) return;
	j->keyLeft=left;
	j->keyRight=right;
	j->keyUp=up;
	j->keyDown=down;
	save();
}

void Jogadores::incJogo( std::string username )
{
	int idx=findJogador(username);
	if (idx==-1) return;
	Jogador *j=getJogador(idx);
	if (j==NULL) return;
	j->NJogos +=1;
	save();
}

void Jogadores::desisteJogo( std::string username, int npontos )
{
	int idx=findJogador(username);
	if (idx==-1) return;
	Jogador *j=getJogador(idx);
	if (j==NULL) return;
	j->NDesistencias +=1;
	j->NPontos=j->NPontos+npontos;
	save();
}

void Jogadores::ganhouJogo( std::string username, int npontos )
{
	int idx=findJogador(username);
	if (idx==-1) return;
	Jogador *j=getJogador(idx);
	if (j==NULL) return;
	j->NVitorias +=1;
	j->NPontos+=npontos;
	save();
}

void Jogadores::ordenarPontos()
{
	Jogador aux;
	for(unsigned int j=0; j<list.size(); j++)
		for(unsigned int i=0; i<list.size()-1; i++)
			if(list[i]->NPontos<list[i+1]->NPontos){
				aux=*list[i];
				*list[i]=*list[i+1];
				*list[i+1]=aux;
			}

}

void Jogadores::perdeuJogo( std::string username, int nPontos )
{
	int idx=findJogador(username);
	if (idx==-1) return;
	Jogador *j=getJogador(idx);
	if (j==NULL) return;
	j->NDerroras+=1;
	j->NPontos+=nPontos;
	save();
}