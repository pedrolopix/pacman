#pragma once
#include "..\comum\basethread.h"

class EngineServer;

class EngineServerThread : public BaseThread
{
private:
	EngineServer *engineServer;
protected:
	virtual void execute();
public:
	EngineServerThread(EngineServer *engineServer);
	virtual ~EngineServerThread(void);
};

