#pragma once
#include <Windows.h>
#include "..\comum\BaseThread.h"
#include "..\comum\Comunication.h"
#include "Jogadores.h"

class ServerPacMan;

class ClienteThread:public BaseThread
{
private:
	HANDLE hPipe;
	ServerPacMan *srv;
	std::string username;
	void cmdSwitch( const CommandBuf &cmd );
	void cmdLogin( const CommandBuf & cmd );

	void loadJogadorProp( CBufUserProp &u, Jogador &j );

	void cmdUserAdd( const CommandBuf & cmd );
	void log(const std::string &str);
	ClienteThread(const ClienteThread &a);
	ClienteThread & operator = (const ClienteThread &a);
protected:
	virtual void execute();
	bool readpipe(LPVOID buf, int len);
	void writePipe(const Commands command,const LPVOID buf,const int len);
public:
	ClienteThread(ServerPacMan *srv, HANDLE pipe);
	~ClienteThread();
	void cmdUserList( const CommandBuf & cmd );
	void cmdUserGet( const CommandBuf & cmd );
	void cmdUserDelete( const CommandBuf & cmd );
	void cmdLigarJogo( const CommandBuf & cmd );
	std::string getUserName();
	void cmdNovoJogo( const CommandBuf & cmd );
	void cmdSendkeys( const CommandBuf & cmd );
	void cmdDesisteJogo( const CommandBuf & cmd );
	void cmdUserSetKeys( const CommandBuf & cmd );
	void cmdTop10( const CommandBuf & cmd );
};

