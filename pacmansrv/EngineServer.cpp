#include <string>
#include "EngineServer.h"
#include "ServerPacMan.h"
#include "EngineServerThread.h"
#include "..\comum\Guy.h"
#include "..\comum\BadGuyBase.h"
#include "..\comum\GoodGuyBase.h"
#include "..\comum\TabuleiroBase.h"
#include "..\comum\utils.h"
#include "..\comum\Comunication.h"

using namespace std;

EngineServer::EngineServer(ServerPacMan *srv):GameBase(srv)
{
	TabuleiroBase *t=new TabuleiroBase(this);
	t->LoadInternal();
	setTabuleiro(t);
	state=esperaJogadores;
	this->srv=srv;
	this->gamethread = new EngineServerThread(this);
}


EngineServer::~EngineServer()
{
	delete gamethread;
}

void EngineServer::avanca()
{
	if (state==esperaJogadores) {
		if (this->getGameTime()>(WAITTIME*1000)) state=correr;
		notifyClientes(gcmd_esperaJogadores);
		return;
	}

	//srv->log("jogo avanca");
	GameBase::avanca();
	
	if (getAcabouJogo()) {
		//srv->log("jogo acabou");
		actualizaJogadores();

		notifyClientes(gcmd_end);
	} else
	{
		notifyClientes(gcmd_update);
	}
	srv->getNovoJogoMutex().unlock();
}

void EngineServer::addBadGuys( int nBadGuys )
{
	int x;
	int y;
	for(int i=0; i<nBadGuys; i++) {
		GuyCor cor=(GuyCor) random_l_h(0,1);
		do {
			x= random_l_h(13,19);
			y= random_l_h(15,16);
		} while (isGuyIn(NULL,x,y,false));
		addBadGuy(new BadGuyBase(this,cor,x,y));
	}
}



void EngineServer::notifyClientes(gameCmd cmd)
{
	int ret,n,c;
	CbufGame game;
	if (srv->listenersCount()==0) {
		return;
	}

	game.cmd= cmd;
	game.nguys = getNGuys();
	game.pontosTotal = getTabuleiro()->getPontosTotal();
	game.pontosMax= getTabuleiro()->getPontosMax();
	game.pontosFalta= getTabuleiro()->getPontosFalta();


	if (cmd==gcmd_esperaJogadores) {
	   game.time = (WAITTIME*1000)-getGameTime();
	} else
	{
		game.time = getGameTime();
	}

	for(int x=0; x<TAB_MAX_X; x++) {
		for(int y=0; y<TAB_MAX_Y; y++) {
			game.tabuleiro[x][y] = getTabuleiro()->getTab(x,y);
		}
	}

	for (int i=0;i<game.nguys;i++) {
		Guy *g=getGuy(i);
		game.guys[i].direccao= g->getDireccao();
		game.guys[i].x= g->getX();
		game.guys[i].y= g->getY();
		game.guys[i].morto = g->estaMorto();

		if (typeid (*g) == typeid (GoodGuyBase)) {
			GoodGuyBase *gg= (GoodGuyBase *)(g);
			game.guys[i].isGoodGuy = true;
			game.guys[i].vida = gg->getVida();
			game.guys[i].pontos = gg->getPontos();
			game.guys[i].automatic = gg->getAuto();
			strcpy_s(game.guys[i].nome, MAX_CHAR, (gg->getNome().c_str()));
		} else {
			BadGuyBase *bg= (BadGuyBase *)(g);
			game.guys[i].isGoodGuy = false;
			game.guys[i].vida = 0;
			game.guys[i].pontos = 0;
			game.guys[i].automatic= true;
			strcpy_s(game.guys[i].nome, MAX_CHAR, "");
			game.guys[i].cor = bg->getCor();
		}
	}
	
	c=0;

	for(int i=0; i<srv->listenersCount();i++) {
		if (srv->getListeners(i)!=0) {
			//srv->log("notifica...");
			ret=WriteFile(srv->getListeners(i),&game,sizeof(game),(LPDWORD)&n,NULL);
			if (!ret || !n) {					
				CloseHandle(srv->getListeners(i));
				srv->setListeners(i,0);
				//srv->log("handle com erro, fecha handle...");
				c++;
				continue;
			}
			///srv->log("fim notifica");
			
		} else {
			c++;
		}
	}
	if (c==srv->listenersCount()) {
		//srv->log("ja nao ha listeners.... acaba o jogo.");
		if (cmd!=gcmd_end)	terminou();
	}
}

void EngineServer::start()
{
	GameBase::start();
	gamethread->start();
}

void EngineServer::sendkeys( std::string username, int key )
{
	for (int i=0;i<getNGuys();i++) {
		Guy *g=getGuy(i);
		if (typeid (*g) == typeid (GoodGuyBase)) {
		  GoodGuyBase *gg= (GoodGuyBase *)(g);
		  if (gg->getNome()==username) {
			  gg->doKeyDown(key);
			  return;
		  }
		}
	}
}

void EngineServer::desisteJogo(std::string username)
{
	for (int i=0;i<getNGuys();i++) {
		Guy *g=getGuy(i);
		if (typeid (*g) == typeid (GoodGuyBase)) {
			GoodGuyBase *gg= (GoodGuyBase *)(g);
			if (gg->getNome()==username) {
				gg->morre();
				return;
			}
		}
	}
}

void EngineServer::terminou()
{
	GameBase::terminou();
	notifyClientes(gcmd_end);
	srv->gameEnd();
}

bool EngineServer::getEsperaJogadores() const
{
	return (state==esperaJogadores);
}

void EngineServer::updateKeys( std::string username, int left, int right, int up, int down )
{
	for (int i=0;i<getNGuys();i++) {
		Guy *g=getGuy(i);
		if (typeid (*g) == typeid (GoodGuyBase)) {
			GoodGuyBase *gg= (GoodGuyBase *)(g);
			if (gg->getNome()==username) {
				gg->setKeyLeft(left);
				gg->setKeyRight(right);
				gg->setKeyUp(up);
				gg->setKeyDown(down);
				return;
			}
		}
	}
}

int EngineServer::getPontosGame( std::string username )
{
	for (int i=0;i<getNGuys();i++) {
		Guy *g=getGuy(i);
		if (typeid (*g) == typeid (GoodGuyBase)) {
			GoodGuyBase *gg= (GoodGuyBase *)(g);
			if (gg->getNome()==username) {
				
				return gg->getPontos();
			}
		}
	}
	return 0;
}

void EngineServer::actualizaJogadores()
{
	srv->getNovoJogoMutex().lock();
	int pontos=0;
	std::string username;
	for(int i=0; i<getNGuys();i++) {
		if (getGuy(i)->isGoodGuy()) {
			GoodGuyBase *g=(GoodGuyBase*)(getGuy(i));
			if (g->getPontos()>pontos  ) {
				pontos=g->getPontos();
				username=g->getNome();
			}
		}
	}
	srv->getJogadores()->lock();
	for(int i=0; i<getNGuys();i++) {
		if (getGuy(i)->isGoodGuy()) { 
			GoodGuyBase *g=(GoodGuyBase*)(getGuy(i));
			if (g->getNome()==username) {
				srv->getJogadores()->ganhouJogo(g->getNome(),g->getPontos());
			} else {
				srv->getJogadores()->perdeuJogo(g->getNome(),g->getPontos());
			}
		}
	}
	srv->getJogadores()->unlock();
}
