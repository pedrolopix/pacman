#include "EngineServerPipeThread.h"
#include "..\comum\Comunication.h"
#include "ServerPacMan.h"
#include "..\comum\service.h"

#define PIPE_NAME_GAMESRV "\\\\."PIPE_NAME_GAME

EngineServerPipeThread::EngineServerPipeThread(ServerPacMan *srv, LPSECURITY_ATTRIBUTES lpSecurityAttributes)
{
	this->srv=srv;
	this->lpSecurityAttributes=lpSecurityAttributes;
}


EngineServerPipeThread::~EngineServerPipeThread(void)
{
}

void EngineServerPipeThread::execute()
{
	int status;
	OVERLAPPED ovl;
	while (true) {
#ifdef DEBUG_CONSOLA				
		HANDLE hPipe= CreateNamedPipe(PIPE_NAME_GAMESRV, PIPE_ACCESS_DUPLEX, PIPE_WAIT | PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE, MAXJOGADORES, MAXBUFFER, MAXBUFFER,TIMEOUT, lpSecurityAttributes);
#else
		HANDLE hPipe= CreateNamedPipe(PIPE_NAME_GAMESRV, PIPE_ACCESS_DUPLEX|FILE_FLAG_OVERLAPPED, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE, MAXJOGADORES , MAXBUFFER, MAXBUFFER,TIMEOUT, lpSecurityAttributes);
#endif
		if (!hPipe) {
			status=GetLastError();
			if(hPipe == INVALID_HANDLE_VALUE){
				srv->logError("Limite atingido (game pipe)!",status);
				break;
			} else {
				srv->logError("CreateNamedPipe error (game pipe)!",status);
			}
		}

#ifdef DEBUG_CONSOLA		
		if(!ConnectNamedPipe(hPipe, NULL)){
			status = GetLastError();
			if (status != ERROR_IO_PENDING) {
				srv->logError("Erro na liga��o ao cliente (game pipe)!",status);
				break;
			}
		}

#else
		ZeroMemory(&ovl,sizeof(ovl));
		ovl.hEvent=srv->getEventGame();

		srv->log("Esperar ligacao de um cliente para jogo... (game pipe)");
		if(!ConnectNamedPipe(hPipe, &ovl)){
			status = GetLastError();
			if (status != ERROR_IO_PENDING) {
				srv->logError("Erro na liga��o ao cliente (game pipe)!",status);
				break;
			}
		}
		srv->log("wait game pipe");
		DWORD r=(WaitForSingleObject(srv->getEventGame(),INFINITE));
		//if (r==WAIT_TIMEOUT) continue;
#endif

		if (srv->getTerminated()){

			srv->getNovoJogoMutex().lock();
			CbufGame game;
			int n;
			game.cmd=gcmd_serverend;
			for(int i=0; i<srv->listenersCount();i++) {
				if (srv->getListeners(i)!=0) {
					WriteFile(srv->getListeners(i),&game,sizeof(game),(LPDWORD)&n,NULL);
					DisconnectNamedPipe(srv->getListeners(i));
					CloseHandle(srv->getListeners(i));		
					srv->setListeners(i,0);
				}	
			}

			srv->getNovoJogoMutex().unlock();
			DisconnectNamedPipe(hPipe);
			CloseHandle(hPipe);		
			break;
		}
		
		

		//adiciona handler a lista de jogadores em jogo
		srv->getNovoJogoMutex().lock();
		srv->addListener(hPipe);
		srv->getNovoJogoMutex().unlock();

	}
}
