#include "EngineServerThread.h"
#include "EngineServer.h"


EngineServerThread::EngineServerThread(EngineServer *engineServer)
{
	this->engineServer=engineServer;
}


EngineServerThread::~EngineServerThread(void)
{
}

void EngineServerThread::execute()
{
	do {
		engineServer->avanca();	
		if (engineServer->getAcabouJogo()) {		
			break;
		}

		Sleep(150);

	} while (!getTerminated());
	engineServer->terminou();
}
